//
//  OostaJobCustomerRegisterationViewController.m
//  OostaJob
//
//  Created by Apple on 20/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobCustomerUpdateViewController.h"
#import "OostaJobEmailVerificationViewController.h"
#import "UIImage+ImageCompress.h"
#import "SWRevealViewController.h"
#import "OostaJobZipCodeViewController.h"
@interface OostaJobCustomerUpdateViewController ()<SWRevealViewControllerDelegate>
{
    UIImage *choosenImage;
    NSString *strLatitude;
    NSString *strLongitude;
    NSString *strZipcode;
    NSString *strTimeZone;
    NSString * strAddress;
    int val;
    KLCPopup *popupTermsAndConditions;
    MBProgressHUD *HUD;
    UIImagePickerController *pick;
    BOOL isChangePassword;
    NSArray *cityArr;
}
@end

@implementation OostaJobCustomerUpdateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self removePlusIcon];
    [self.imgBlur assignBlur];
    [self setupUI];
    self.navigationController.navigationBarHidden = YES;
    [self registerForKeyboardNotifications];
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneWithNumberPad)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:FONT_BOLD size:18.0], NSFontAttributeName,
                                        [UIColor whiteColor], NSForegroundColorAttributeName,
                                        nil]
                              forState:UIControlStateNormal];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           doneButton,
                           nil];
    [numberToolbar sizeToFit];
    _txtPhone.inputAccessoryView = numberToolbar;
    // Do any additional setup after loading the view from its nib.
    
    if (!(IS_IPHONE5&&(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)))
    {
        _scrollview.contentSize=CGSizeMake(320, 510);
        [self registerForKeyboardNotifications];
    }
    
    SWRevealViewController *revealController = [self revealViewController];
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [self setupValues];
    [self getCityDetails];
}
-(void)removePlusIcon
{
    _imgPlusName.image=[UIImage imageNamed:@""];
    _imgPlusEmail.image=[UIImage imageNamed:@""];
    _imgPlusPassword.image=[UIImage imageNamed:@""];
    _imgPlusAddress.image=[UIImage imageNamed:@""];
    _imgPlusPhone.image=[UIImage imageNamed:@""];
}
-(void)setupValues
{
    NSLog(@"UserDetails:%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"USERDETAILS"]);
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"USERDETAILS"])
    {
        _txtName.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Name"]];
        _txtEmail.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Email"]];
        _txtPassword.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Password"]];
        _txtPhone.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Phone"]];
        _txtAddress.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Address"]];
        strLatitude = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Lat"]];
        strLongitude = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Lng"]];
        strAddress = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Address"]];
        strZipcode = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Zipcode"]];
        [self getTheTimeZoneFromGivenLattitude:strLatitude andLongitude:strLongitude];
        [_activityIndicator startAnimating];
        [self loadFromURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.profilePhoto"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] callback:^(UIImage *image) {
            if (image)
            {
                choosenImage=image;
                [self.btnPhotoCapture setBackgroundImage:choosenImage forState:UIControlStateNormal];
            }
                [_activityIndicator stopAnimating];
        }];
        _imgPlusName.image=[UIImage imageNamed:@"tick"];
        _imgPlusEmail.image=[UIImage imageNamed:@"tick"];
        _imgPlusPassword.image=[UIImage imageNamed:@"tick"];
        _imgPlusAddress.image=[UIImage imageNamed:@"tick"];
        _imgPlusPhone.image=[UIImage imageNamed:@"tick"];
        [self btnCheckBoxTapped:self.btnCheckBox];
    }
}
-(void) loadFromURL: (NSURL*) url callback:(void (^)(UIImage *image))callback {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        NSData * imageData = [NSData dataWithContentsOfURL:url];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *image = [UIImage imageWithData:imageData];
            callback(image);
        });
    });
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
#pragma mark - Setup UI
-(void)setupUI
{
    self.viewName.layer.cornerRadius=5.0;
    self.viewEmail.layer.cornerRadius=5.0;
    self.viewPassword.layer.cornerRadius=5.0;
    self.viewAddress.layer.cornerRadius=5.0;
    self.viewPhone.layer.cornerRadius=5.0;
    self.view.layer.cornerRadius=5.0;
    self.viewPhone.layer.cornerRadius=5.0;
    self.viewPhone.layer.cornerRadius=5.0;
    
    self.viewName.layer.masksToBounds=YES;
    self.viewEmail.layer.masksToBounds=YES;
    self.viewPassword.layer.masksToBounds=YES;
    self.viewAddress.layer.masksToBounds=YES;
    self.viewPhone.layer.masksToBounds=YES;
    self.btnRegister.layer.cornerRadius=5.0;
    self.btnRegister.layer.masksToBounds=YES;
    if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        [self.btnRegister.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        [self.btnTermsConditions.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:23.0f]];
        
        self.txtName.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        self.txtEmail.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        self.txtPassword.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        self.txtAddress.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        self.txtPhone.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        self.lblIAgree.font=[UIFont fontWithName:FONT_BOLD size:23.0f];
        self.lblTermsAndConditions.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblChangePasswordHeader.font = [UIFont fontWithName:FONT_BOLD size:30.0f];
        self.txtConfirmPassword.font = [UIFont fontWithName:FONT_THIN size:30.0f];
        self.txtNewPassword.font = [UIFont fontWithName:FONT_THIN size:30.0f];
        self.txtOldPassword.font = [UIFont fontWithName:FONT_THIN size:30.0f];
        self.btnSubmit.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblUploadYourPhoto.font=[UIFont fontWithName:FONT_BOLD size:23.0f];
    }
    else
    {
        [self.btnRegister.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        [self.btnTermsConditions.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
        self.txtName.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtEmail.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtPassword.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtAddress.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtPhone.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.lblIAgree.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
        self.lblTermsAndConditions.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        
        self.lblChangePasswordHeader.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
        self.txtConfirmPassword.font = [UIFont fontWithName:FONT_THIN size:18.0f];
        self.txtNewPassword.font = [UIFont fontWithName:FONT_THIN size:18.0f];
        self.txtOldPassword.font = [UIFont fontWithName:FONT_THIN size:18.0f];
        self.btnSubmit.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblUploadYourPhoto.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
    }
    self.btnSubmit.layer.cornerRadius = 3.0f;
    
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if (isChangePassword!=YES)
    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+14, 0.0);
        self.scrollview.contentInset = contentInsets;
        self.scrollview.scrollIndicatorInsets = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, _txtEmail.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0, _txtEmail.frame.origin.y-kbSize.height);
            [self.scrollview setContentOffset:scrollPoint animated:YES];
        }
        
    }
    else
    {
        
        if (!IS_IPAD)
        {
            [UIView animateWithDuration:0.8
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 _viewContentChangePasswod.frame = CGRectMake(21, 60, 278, 295);
                             }
                             completion:nil];
            
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    if (isChangePassword!=YES)
    {
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        self.scrollview.contentInset = contentInsets;
        self.scrollview.scrollIndicatorInsets = contentInsets;
    }
    else
    {
        
        if (!IS_IPAD)
        {
            [UIView animateWithDuration:0.8
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 _viewContentChangePasswod.frame = CGRectMake(21, 136, 278, 295);
                             }
                             completion:nil];
        }
    }
    
}

#pragma mark - Textfield Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_txtName)
    {
        [self.txtEmail becomeFirstResponder];
    }
    else if (textField==_txtEmail)
    {
        //        [self.txtPassword becomeFirstResponder];
        [textField resignFirstResponder];
    }
    else if (textField==_txtPassword)
    {
        NSLog(@"Open Map");
        [self btnAddressTapped:self];
        [self.txtPassword resignFirstResponder];
        CGPoint point = CGPointMake(0, 0);
        [self.scrollview setContentOffset:point animated:YES];
    }
    else
    {
        [self.view endEditing:YES];
        CGPoint point = CGPointMake(0, 0);
        [self.scrollview setContentOffset:point animated:YES];
    }
    
    return YES;
}


- (void) scrollVievEditingFinished:(UITextField*)textField
{
    CGPoint point = CGPointMake(0, 0);
    [self.scrollview setContentOffset:point animated:YES];
}
- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==_txtPhone)
    {
        CGPoint scrollPoint = CGPointMake(0.0,_txtPhone.frame.origin.y+5.1*_txtPhone.frame.size.height);
        [self.scrollview setContentOffset:scrollPoint animated:YES];
    }
    return YES;
}
-(void)doneWithNumberPad
{
    [self.txtPhone resignFirstResponder];
    CGPoint point = CGPointMake(0, 0);
    [self.scrollview setContentOffset:point animated:YES];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
    CGPoint point = CGPointMake(0, 0);
    [self.scrollview setContentOffset:point animated:YES];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField==_txtName)
    {
        if (text.length>0)
        {
            _imgPlusName.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            _imgPlusName.image=[UIImage imageNamed:@""];
        }
    }
    
    if (textField==_txtEmail)
    {
        if ([self IsValidEmail:text])
        {
            _imgPlusEmail.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            _imgPlusEmail.image=[UIImage imageNamed:@""];
        }
    }
    
    if (textField==_txtPassword)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (text.length>0&&(![string isEqualToString:@""]) && newLength>=10)
        {
            _imgPlusPassword.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            _imgPlusPassword.image=[UIImage imageNamed:@""];
        }
    }
    
    if (textField==_txtAddress)
    {
        if (text.length>0)
        {
            _imgPlusAddress.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            _imgPlusAddress.image=[UIImage imageNamed:@""];
        }
    }
    
    if (textField==_txtPhone)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength>=10)
        {
            _imgPlusPhone.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            _imgPlusPhone.image=[UIImage imageNamed:@""];
        }
        
        return newLength <= 10;
    }

    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions
- (IBAction)btnBackTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


- (IBAction)btnRegisterTapped:(id)sender
{
    CAKeyframeAnimation * anim = [ CAKeyframeAnimation animationWithKeyPath:@"transform" ] ;
    anim.values = [ NSArray arrayWithObjects:
                   [ NSValue valueWithCATransform3D:CATransform3DMakeTranslation(-5.0f, 0.0f, 0.0f) ],
                   [ NSValue valueWithCATransform3D:CATransform3DMakeTranslation(5.0f, 0.0f, 0.0f) ],
                   nil ] ;
    anim.autoreverses = YES ;
    anim.repeatCount = 2.0f ;
    anim.duration = 0.07f ;
    
    if (_txtName.text.length==0&&_txtEmail.text.length==0&&_txtAddress.text.length==0&&_txtPassword.text.length==0&&_txtPhone.text.length==0)
    {
        [ _viewName.layer addAnimation:anim forKey:nil ] ;
        [ _viewAddress.layer addAnimation:anim forKey:nil ] ;
        [ _viewPhone.layer addAnimation:anim forKey:nil ] ;
        [ _viewEmail.layer addAnimation:anim forKey:nil ] ;
        [ _viewPassword.layer addAnimation:anim forKey:nil ] ;
    }
    else if(_txtName.text.length==0)
    {
        [ _viewName.layer addAnimation:anim forKey:nil ] ;
        
    }
    else if(![self IsValidEmail:_txtEmail.text])
    {
        [ _viewEmail.layer addAnimation:anim forKey:nil ] ;
        
    }
    else if(_txtPassword.text.length==0 || _txtPassword.text.length<10)
    {
        [ _viewPassword.layer addAnimation:anim forKey:nil ] ;
    }
    else if(_txtAddress.text.length==0)
    {
        [ _viewAddress.layer addAnimation:anim forKey:nil ] ;
    }
    else if(_txtPhone.text.length<10)
    {
        [ _viewPhone.layer addAnimation:anim forKey:nil ] ;
    }
    else if (val !=1)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
        hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
        
        hud.labelText = @"Agree to Terms & Conditions ";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
        
        
    }
    else
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            hud.mode = MBProgressHUDModeText;
            hud.color=[UIColor whiteColor];
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            
            hud.labelText = INTERNET_ERR;
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
        else
        {
            NSLog(@"customerupdate");
            if (choosenImage!=nil)
            {
                CGFloat scaleSize = 0.2f;
                UIImage *smallImage = [UIImage imageWithCGImage:choosenImage.CGImage
                                                          scale:scaleSize
                                                    orientation:choosenImage.imageOrientation];
                choosenImage=smallImage;
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.color=[UIColor whiteColor];
                hud.labelText = @"Updating...";
                hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                hud.activityIndicatorColor=[UIColor colorWithRed:0.971 green:0.000 blue:0.452 alpha:1.000];
                hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                hud.margin = 010.f;
                hud.yOffset = 20.f;
                [hud show:YES];
                
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"dd-MM-YYYY"];
                int random = arc4random() % 9999999;
                NSString * randomName;
                randomName=[NSString stringWithFormat:@"%@-%d-Photo.png",[dateFormatter stringFromDate:[NSDate date]],random];
                
                NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"( \" )"];
                randomName = [[randomName componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
                NSLog(@" Name %@",randomName);
                NSString *URLString =[NSString stringWithFormat:@"%@profileimg",BaseURL];
                NSDictionary *parameters = @{@"profile": randomName};
                NSLog(@"parameters:%@",parameters);
                
                
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                manager.securityPolicy.allowInvalidCertificates = YES;
                manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
                manager.requestSerializer = [AFJSONRequestSerializer serializer];    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                                                                      {
                                                                                          
                                                                                          [formData appendPartWithFileData:UIImagePNGRepresentation([UIImage compressImage:choosenImage compressRatio:0.8f]) name:@"profile" fileName:randomName mimeType:@"image/png"];
                                                                                          
                                                                                      }
                                                                                           success:^(AFHTTPRequestOperation *operation, id responseObject)
                                                                                      {
                                                                                          NSLog(@"Success %@", responseObject);
                                                                                          NSString * key1 =@"Name";
                                                                                          NSString * obj1 =_txtName.text;
                                                                                          
                                                                                          NSString * key2 =@"Address";
                                                                                          NSString * obj2 =_txtAddress.text;
                                                                                          
                                                                                          NSString * key3 =@"Phone";
                                                                                          NSString * obj3 =_txtPhone.text;
                                                                                          
                                                                                          NSString * key4 =@"Zipcode";
                                                                                          NSString * obj4 =strZipcode;
                                                                                          
                                                                                          NSString * key5 =@"Lat";
                                                                                          NSString * obj5 =strLatitude;
                                                                                          
                                                                                          NSString * key6 =@"Lng";
                                                                                          NSString * obj6 =strLongitude;
                                                                                          
                                                                                          
                                                                                          NSString * key7 =@"profilePhoto";
                                                                                          NSString * obj7 =randomName;
                                                                                          
                                                                                          
                                                                                          
                                                                                          
                                                                                          NSUserDefaults *userValue=[NSUserDefaults standardUserDefaults];
                                                                                          NSString *strToken=[userValue objectForKey:@"DEVICE_TOKEN"];
                                                                                          
                                                                                          if (strToken.length==0)
                                                                                          {
                                                                                              strToken=@"28d1e4a814db45b0cdabf929106d5f9dd48d966900f1c591dcff70ecdd6b5e6d";
                                                                                          }
                                                                                          NSString * key8 =@"notificationID";
                                                                                          NSString * obj8 =strToken;
                                                                                          
                                                                                          NSString * key9 =@"deviceType";
                                                                                          NSString * obj9 =@"2";
                                                                                          
                                                                                          NSString * key10 =@"Email";
                                                                                          NSString * obj10 =_txtEmail.text;
                                                                                          
                                                                                          NSString * key11 =@"Password";
                                                                                          NSString * obj11 =_txtPassword.text;
                                                                                          
                                                                                          
                                                                                          NSString * key12 =@"timezone";
                                                                                          NSString * obj12 =strTimeZone;
                                                                                          
                                                                                          NSString * key13 =@"userID";
                                                                                          NSString * obj13 =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"]];
                                                                                          NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                                                                                                         initWithObjects:@[obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10,obj11,obj12,obj13]
                                                                                                                         forKeys:@[key1,key2,key3,key4,key5,key6,key7,key8,key9,key10,key11,key12,key13]];
                                                                                          
                                                                                          NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
                                                                                          NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
                                                                                          NSLog(@"DATA %@",jsonString);
                                                                                          NSMutableData *body = [NSMutableData data];
                                                                                          
                                                                                          [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
                                                                                          
                                                                                          NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@customerupdate",BaseURL]]];
                                                                                          NSLog(@"customerupdate URL:%@",request.URL);
                                                                                          [request setHTTPBody:body];
                                                                                          [request setHTTPMethod:@"PUT"];
                                                                                          
                                                                                          [NSURLConnection sendAsynchronousRequest: request
                                                                                                                             queue: [NSOperationQueue mainQueue]
                                                                                                                 completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
                                                                                           {
                                                                                               if (error || !data)
                                                                                               {
                                                                                                   NSLog(@"Server Error : %@", error);
                                                                                                   
                                                                                                   [hud hide:YES];
                                                                                                   MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                                                                                                   
                                                                                                   hud.mode = MBProgressHUDModeText;
                                                                                                   hud.color=[UIColor whiteColor];
                                                                                                   hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                                                                                                   hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                                                                                                   
                                                                                                   hud.labelText = SERVER_ERR;
                                                                                                   hud.margin = 10.f;
                                                                                                   hud.yOffset = 20.f;
                                                                                                   hud.removeFromSuperViewOnHide = YES;
                                                                                                   [hud hide:YES afterDelay:2];
                                                                                               }
                                                                                               else
                                                                                               {
                                                                                                   NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                                                          options:0
                                                                                                                                                            error:NULL];
                                                                                                   NSLog(@"Result %@",result);
                                                                                                   if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                                                                                                   {
                                                                                                       [hud hide:YES];
                                                                                                       MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                                                                                                       [self.navigationController.view addSubview:hud];
                                                                                                       
                                                                                                       hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                                                                                                       hud.mode = MBProgressHUDModeCustomView;
                                                                                                       hud.color=[UIColor whiteColor];
                                                                                                       hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                                                                                                       hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                                                                                                       hud.delegate = self;
                                                                                                       hud.labelText = @"Done";
                                                                                                       [hud show:YES];
                                                                                                       [hud hide:YES afterDelay:2];
                                                                                                       
                                                                                                       NSLog(@"UserDetails:%@",[[result valueForKeyPath:@"CustomerDetails"] objectAtIndex:0]);
                                                                                                       [[NSUserDefaults standardUserDefaults]setObject:[[result valueForKeyPath:@"CustomerDetails"] objectAtIndex:0] forKey:@"USERDETAILS"];
                                                                                                       if(_delegate && [_delegate respondsToSelector:@selector(getUpdate)])
                                                                                                       {
                                                                                                           
                                                                                                           [_delegate getUpdate];
                                                                                                       }
                                                                                                   }
                                                                                                   else if([[result valueForKeyPath:@"CustomerDetails"]isEqualToString:@"Email already exists"])
                                                                                                   {
                                                                                                       
                                                                                                       [hud hide:YES];
                                                                                                       MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                                                                                                       
                                                                                                       hud.mode = MBProgressHUDModeText;
                                                                                                       hud.color=[UIColor whiteColor];
                                                                                                       hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                                                                                                       hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                                                                                                       
                                                                                                       hud.labelText = @"Email already exists";
                                                                                                       hud.margin = 10.f;
                                                                                                       hud.yOffset = 20.f;
                                                                                                       hud.removeFromSuperViewOnHide = YES;
                                                                                                       [hud hide:YES afterDelay:2];
                                                                                                       
                                                                                                   }
                                                                                                   
                                                                                                   else
                                                                                                   {
                                                                                                       [hud hide:YES];
                                                                                                       MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                                                                                                       
                                                                                                       hud.mode = MBProgressHUDModeText;
                                                                                                       hud.color=[UIColor whiteColor];
                                                                                                       hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                                                                                                       hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                                                                                                       
                                                                                                       hud.labelText = SERVER_ERR;
                                                                                                       hud.margin = 10.f;
                                                                                                       hud.yOffset = 20.f;
                                                                                                       hud.removeFromSuperViewOnHide = YES;
                                                                                                       [hud hide:YES afterDelay:2];                                    }
                                                                                               }
                                                                                           }];
                                                                                          
                                                                                      }
                                                                                           failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                                                                      {
                                                                                          NSLog(@"Failure %@, %@", error, operation.responseString);
                                                                                          [hud hide:YES];
                                                                                          MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                                                                                          
                                                                                          hud.mode = MBProgressHUDModeText;
                                                                                          hud.color=[UIColor whiteColor];
                                                                                          hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                                                                                          hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                                                                                          
                                                                                          hud.labelText = SERVER_ERR;
                                                                                          hud.margin = 10.f;
                                                                                          hud.yOffset = 20.f;
                                                                                          hud.removeFromSuperViewOnHide = YES;
                                                                                          [hud hide:YES afterDelay:2];
                                                                                          
                                                                                      }];
                
            }
            else
            {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.color=[UIColor whiteColor];
                hud.labelText = @"Updating...";
                hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                hud.activityIndicatorColor=[UIColor colorWithRed:0.971 green:0.000 blue:0.452 alpha:1.000];
                hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                hud.margin = 010.f;
                hud.yOffset = 20.f;
                [hud show:YES];
                
                NSString * key1 =@"Name";
                NSString * obj1 =_txtName.text;
                
                NSString * key2 =@"Address";
                NSString * obj2 =_txtAddress.text;
                
                NSString * key3 =@"Phone";
                NSString * obj3 =_txtPhone.text;
                
                NSString * key4 =@"Zipcode";
                NSString * obj4 =strZipcode;
                
                NSString * key5 =@"Lat";
                NSString * obj5 =strLatitude;
                
                NSString * key6 =@"Lng";
                NSString * obj6 =strLongitude;
                
                
                NSString * key7 =@"profilePhoto";
                NSString * obj7 =@"";
                
                
                
                
                NSUserDefaults *userValue=[NSUserDefaults standardUserDefaults];
                NSString *strToken=[userValue objectForKey:@"DEVICE_TOKEN"];
                
                if (strToken.length==0)
                {
                    strToken=@"28d1e4a814db45b0cdabf929106d5f9dd48d966900f1c591dcff70ecdd6b5e6d";
                }
                NSString * key8 =@"notificationID";
                NSString * obj8 =strToken;
                
                NSString * key9 =@"deviceType";
                NSString * obj9 =@"2";
                
                NSString * key10 =@"Email";
                NSString * obj10 =_txtEmail.text;
                
                NSString * key11 =@"Password";
                NSString * obj11 =_txtPassword.text;
                
                
                NSString * key12 =@"timezone";
                NSString * obj12 =strTimeZone;
                
                
                NSString * key13 =@"userID";
                NSString * obj13 =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"]];
                NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                               initWithObjects:@[obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10,obj11,obj12,obj13]
                                               forKeys:@[key1,key2,key3,key4,key5,key6,key7,key8,key9,key10,key11,key12,key13]];
                
                
                NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
                NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
                NSLog(@"DATA %@",jsonString);
                NSMutableData *body = [NSMutableData data];
                
                [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
                
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@customerupdate",BaseURL]]];
                NSLog(@"customerupdate URL:%@",request.URL);
                [request setHTTPBody:body];
                [request setHTTPMethod:@"PUT"];
                
                [NSURLConnection sendAsynchronousRequest: request
                                                   queue: [NSOperationQueue mainQueue]
                                       completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
                 {
                     if (error || !data)
                     {
                         NSLog(@"Server Error : %@", error);
                         
                         [hud hide:YES];
                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                         
                         hud.mode = MBProgressHUDModeText;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                         hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                         
                         hud.labelText = SERVER_ERR;
                         hud.margin = 10.f;
                         hud.yOffset = 20.f;
                         hud.removeFromSuperViewOnHide = YES;
                         [hud hide:YES afterDelay:2];
                     }
                     else
                     {
                         NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                                options:0
                                                                                  error:NULL];
                         NSLog(@"Result %@",result);
                         if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                         {
                             [hud hide:YES];
                             MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                             [self.navigationController.view addSubview:hud];
                             
                             hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                             hud.mode = MBProgressHUDModeCustomView;
                             hud.color=[UIColor whiteColor];
                             hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                             hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                             hud.delegate = self;
                             hud.labelText = @"Done";
                             [hud show:YES];
                             [hud hide:YES afterDelay:2];
                             
                             [[NSUserDefaults standardUserDefaults]setObject:[[result valueForKeyPath:@"CustomerDetails"] objectAtIndex:0] forKey:@"USERDETAILS"];
                             
                             if(_delegate && [_delegate respondsToSelector:@selector(getUpdate)])
                             {
                                 
                                 [_delegate getUpdate];
                             }
                             
                         }
                         else if([[result valueForKeyPath:@"CustomerDetails"]isEqualToString:@"Email already exists"])
                         {
                             
                             [hud hide:YES];
                             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                             
                             hud.mode = MBProgressHUDModeText;
                             hud.color=[UIColor whiteColor];
                             hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                             hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                             
                             hud.labelText = @"Email already exists";
                             hud.margin = 10.f;
                             hud.yOffset = 20.f;
                             hud.removeFromSuperViewOnHide = YES;
                             [hud hide:YES afterDelay:2];
                             
                         }
                         
                         else
                         {
                             [hud hide:YES];
                             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                             
                             hud.mode = MBProgressHUDModeText;
                             hud.color=[UIColor whiteColor];
                             hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                             hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                             
                             hud.labelText = SERVER_ERR;
                             hud.margin = 10.f;
                             hud.yOffset = 20.f;
                             hud.removeFromSuperViewOnHide = YES;
                             [hud hide:YES afterDelay:2];                                    }
                     }
                 }];
                
            }
        }
    }
}

-(void)gotoEmailVerification
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController IPad" bundle:nil];
        [self.navigationController pushViewController:emailVerificationObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController" bundle:nil];
        [self.navigationController pushViewController:emailVerificationObj animated:YES];
    }
    else
    {
        OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController" bundle:nil];
        [self.navigationController pushViewController:emailVerificationObj animated:YES];
    }
    
    
}

#pragma mark - Camera Crop
- (IBAction)btnCameraTapped:(id)sender
{
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles: nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Take Photo", nil)];
    }
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Photo Library", nil)];
    if (choosenImage!=nil)
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Remove Photo", nil)];
    }
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    [actionSheet showFromToolbar:self.navigationController.toolbar];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Photo Library", nil)])
    {
        [self openPhotoAlbum];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Take Photo", nil)])
    {
        [self showCamera];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Remove Photo", nil)])
    {
        [self removePhoto];
    }
}
- (void)showCamera
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:controller animated:YES completion:NULL];
    }];
    
}

- (void)openPhotoAlbum
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:controller animated:YES completion:NULL];
    }];
    
    
}

-(void)removePhoto
{
    choosenImage=nil;
    [self.btnPhotoCapture setBackgroundImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    choosenImage = info[UIImagePickerControllerOriginalImage];
    pick = picker;
    [picker dismissViewControllerAnimated:YES completion:^{
        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        controller.image = choosenImage;
        UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:controller];
        [self presentViewController:navigationController animated:YES completion:NULL];
        self.view.clipsToBounds = YES;
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
    }];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    NSLog(@"cropped%@",croppedImage);
    choosenImage=croppedImage;
    [self.btnPhotoCapture setBackgroundImage:croppedImage forState:UIControlStateNormal];
    choosenImage=croppedImage;
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:^{
        [self presentViewController:pick animated:YES completion:NULL];
    }];
}

#pragma mark - Terms and Conditions Checkbox
- (IBAction)btnCheckBoxTapped:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    val = 0;
    if ([UIImagePNGRepresentation(btn.currentBackgroundImage) isEqualToData:UIImagePNGRepresentation([UIImage imageNamed:@"check-40.png"])]){
        val = 0;
        [btn setBackgroundImage:[UIImage imageNamed:@"uncheck-40.png"] forState:UIControlStateNormal];
    }
    else
    {
        [btn setBackgroundImage:[UIImage imageNamed:@"check-40.png"] forState:UIControlStateNormal];
        val = 1;
    }
}

- (IBAction)btnTermsandConditionTapped:(id)sender
{
    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutCenter);
    
    popupTermsAndConditions = [KLCPopup popupWithContentView:_viewTermsAndConditions
                                                    showType:KLCPopupShowTypeFadeIn
                                                 dismissType:KLCPopupDismissTypeFadeOut
                                                    maskType:KLCPopupMaskTypeDimmed
                                    dismissOnBackgroundTouch:NO
                                       dismissOnContentTouch:NO];
    [popupTermsAndConditions showWithLayout:layout];
    [self.scrollview endEditing:YES];
    [self.viewContentChangePasswod endEditing:YES];
    
    //HUD
    HUD = [[MBProgressHUD alloc] initWithView:self.viewTermsAndConditions];
    [self.viewTermsAndConditions addSubview:HUD];
    HUD.delegate = self;
    HUD.labelText = @"Loading...";
    
    //LOAD URL
    NSString* url = [[NSUserDefaults standardUserDefaults] objectForKey:@"CustomerTerms"];
    NSLog(@"url:%@",url);
    NSURL* nsUrl = [NSURL URLWithString:url];
    NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];

    [_webViewTermsAndConditions loadRequest:request];
}
- (IBAction)btnTermsAndConditionClosePressed:(id)sender
{
    [popupTermsAndConditions dismissPresentingPopup];
}

#pragma mark - Webview Delegate
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    //SHOW HUD
    [HUD show:YES];
    
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //KILL HUD
    
    [HUD hide:YES];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    if(!webView.loading)
    {
        //KILL HUD
        [HUD hide:YES];
        
    }
}

#pragma mark - Address
- (IBAction)btnAddressTapped:(id)sender
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        LocationPickerViewController *locObj=[[LocationPickerViewController alloc]initWithNibName:@"LocationPickerViewController IPad" bundle:nil];
        locObj.delegate=self;
        locObj.strLat=strLatitude;
        locObj.strLong=strLongitude;
        locObj.strAddress=strAddress;
        [self.navigationController pushViewController:locObj animated:YES];
    }
    else
    {
        LocationPickerViewController *locObj=[[LocationPickerViewController alloc]initWithNibName:@"LocationPickerViewController" bundle:nil];
        locObj.delegate=self;
        locObj.strLat=strLatitude;
        locObj.strLong=strLongitude;
        locObj.strAddress=strAddress;
        [self.navigationController pushViewController:locObj animated:YES];
    }
    
}


- (void)getAddress:(NSString*)address andtheLat:(NSString *)latitude andtheLong:(NSString *)longitude andtheZipcode:(NSString *)zipcode
{
    NSLog(@"address:%@",address);
    strZipcode=zipcode;
    if ([[cityArr valueForKeyPath:@"zipcode"] containsObject:zipcode])
    {
        
        _imgPlusAddress.image=[UIImage imageNamed:@"tick"];
        _txtAddress.text=address;
        strLongitude=longitude;
        strLatitude=latitude;
        strZipcode=zipcode;
        [self getTheTimeZoneFromGivenLattitude:strLatitude andLongitude:strLongitude];
    }
    else
    {
        _imgPlusAddress.image=[UIImage imageNamed:@""];
        [self GotoAddressNotify];
    }
}


-(void)getTheTimeZoneFromGivenLattitude:(NSString *)Lattitude andLongitude:(NSString *)Longitude
{
    NSTimeInterval seconds = [[NSDate date] timeIntervalSince1970];
    NSString* urlStr1 =[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/timezone/json?location=%@,%@&timestamp=%f&language=es&key=%@",Lattitude,Longitude,seconds,kGoogleAPIKey];
    NSLog(@"GetTheTimezone Url %@",urlStr1);
    NSURLRequest *request1 = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr1]];
    [NSURLConnection sendAsynchronousRequest:request1
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSArray *statusArr =[result valueForKeyPath:@"status"];
             NSLog(@"Status %@",statusArr);
             NSString *resultStr =[NSString stringWithFormat:@"%@",statusArr];
             if ([resultStr isEqualToString:@"OK"])
             {
                 NSLog(@"result:%@",result);
                 strTimeZone=[result valueForKeyPath:@"timeZoneId"];
             }
             else
             {
                 NSLog(@"result:%@",result);
                 
             }
         }
         else
         {
             NSLog(@"error %@",connectionError);
         }
     }];
}


- (IBAction)btnMenuTapped:(id)sender
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
    
}
- (IBAction)btnChangePassword:(id)sender
{
    [self.scrollview endEditing:YES];
    isChangePassword =YES;
    _viewChangePassword.alpha = 0;
    _viewChangePassword.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        
        _viewChangePassword.alpha = 1;
        [self.viewContentChangePasswod endEditing:YES];
    }];
}
- (IBAction)btnSubmitPressed:(id)sender
{
    [self ChangePasswodandCompletionHandler:^(bool resultPassword) {
        if (resultPassword == YES)
        {
            NSLog(@"Password Change Completed");
            [self.scrollview endEditing:YES];
            [self.viewContentChangePasswod endEditing:YES];
        }
    }];
}

- (IBAction)btnClosePressed:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _viewChangePassword.alpha = 0;
    } completion: ^(BOOL finished)
     {
         _viewChangePassword.hidden = finished;
         [self.scrollview endEditing:YES];
         [self.viewContentChangePasswod endEditing:YES];
         isChangePassword = NO;
     }];
}

-(void)ChangePasswodandCompletionHandler:(void (^)(bool resultPassword))completionHandler
{
    if (_txtOldPassword.text.length==0&&_txtNewPassword.text.length==0&&_txtConfirmPassword.text.length==0)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewChangePassword animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = @"Enter all fields";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else if (_txtOldPassword.text.length==0)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewChangePassword animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = @"Enter old password";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else if (_txtNewPassword.text.length==0)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewChangePassword animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = @"Enter new password";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else if (_txtConfirmPassword.text.length==0)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewChangePassword animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = @"Enter confirm password";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else if (_txtNewPassword.text.length<10)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewChangePassword animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelFont = [UIFont boldSystemFontOfSize:12];
        hud.labelText = @"Password must contain at least 10 characters";
        hud.margin = 10.f;
        hud.yOffset = 10.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else if (![_txtNewPassword.text isEqualToString:_txtConfirmPassword.text])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewChangePassword animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = @"Confirm password should be same";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            hud.mode = MBProgressHUDModeText;
            hud.color=[UIColor whiteColor];
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            
            hud.labelText = INTERNET_ERR;
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
        else
        {
            [self.viewContentChangePasswod endEditing:YES];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewChangePassword animated:YES];
            hud.color=[UIColor whiteColor];
            hud.labelText = @"Changing...";
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            hud.margin = 010.f;
            hud.yOffset = 20.f;
            [hud show:YES];
            
            
            NSString * key1 =@"userID";
            NSString * obj1 =[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"];
            
            NSString * key2 =@"OldPassword";
            NSString * obj2 =_txtOldPassword.text;
            
            NSString * key3 =@"NewPassword";
            NSString * obj3 =_txtNewPassword.text;
            
            
            NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                           initWithObjects:@[obj1,obj2,obj3]
                                           forKeys:@[key1,key2,key3]];
            
            NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
            NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
            NSLog(@"DATA %@",jsonString);
            NSMutableData *body = [NSMutableData data];
            
            [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@changepassword",BaseURL]]];
            NSLog(@"addrating URL:%@",request.URL);
            [request setHTTPBody:body];
            [request setHTTPMethod:@"POST"];
            request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
            
            [NSURLConnection sendAsynchronousRequest: request
                                               queue: [NSOperationQueue mainQueue]
                                   completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if (error || !data)
                 {
                     
                     NSLog(@"Server Error : %@", error);
                     
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(true);
                 }
                 else
                 {
                     NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                            options:0
                                                                              error:NULL];
                     NSLog(@"Result %@",result);
                     if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                     {
                         
                         
                         [self btnClosePressed:self];
                         [hud hide:YES];
                         MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                         [self.navigationController.view addSubview:hud];
                         
                         hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                         hud.mode = MBProgressHUDModeCustomView;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                         hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                         hud.delegate = self;
                         hud.labelText = @"Changed Successfully";
                         
                         [hud show:YES];
                         [hud hide:YES afterDelay:2];
                         _txtPassword.text = _txtNewPassword.text;
                         _imgPlusPassword.image=[UIImage imageNamed:@"tick"];
                         _txtOldPassword.text = @"";
                         _txtNewPassword.text = @"";
                         _txtConfirmPassword.text = @"";
                         
                     }
                     else
                     {
                         NSLog(@"%@",SERVER_ERR);
                         
                         NSString * errorString = SERVER_ERR;
                         if ([[result valueForKeyPath:@"UserDetails"]isEqualToString:@"Invalid Password"])
                         {
                             errorString = @"Invalid Old Password";
                         }
                         
                         [hud hide:YES];
                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                         
                         hud.mode = MBProgressHUDModeText;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=kMBProgressHUDLabelColor;
                         hud.backgroundColor=kMBProgressHUDBackgroundColor;
                         
                         hud.labelText = errorString;
                         hud.margin = 10.f;
                         hud.yOffset = 20.f;
                         hud.removeFromSuperViewOnHide = YES;
                         [hud hide:YES afterDelay:2];
                         
                     }
                     
                     completionHandler(true);
                 }
             }];
        }
    }
}

-(void)getCityDetails
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@city",BaseURL]]];
    NSLog(@"city:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 cityArr=[[NSArray alloc]init];
                 cityArr=[result valueForKeyPath:@"CityDetails"];
                 
             }
             else
             {
                 
             }
         }
     }];
    
}
-(void)GotoAddressNotify
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobZipCodeViewController *ZipCodeObj=[[OostaJobZipCodeViewController alloc]initWithNibName:@"OostaJobZipCodeViewController IPad" bundle:nil];
        ZipCodeObj.strZipcode = strZipcode;
        [self.navigationController pushViewController:ZipCodeObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobZipCodeViewController *ZipCodeObj=[[OostaJobZipCodeViewController alloc]initWithNibName:@"OostaJobZipCodeViewController" bundle:nil];
        ZipCodeObj.strZipcode = strZipcode;
        [self.navigationController pushViewController:ZipCodeObj animated:YES];
    }
    else
    {
        OostaJobZipCodeViewController *ZipCodeObj=[[OostaJobZipCodeViewController alloc]initWithNibName:@"OostaJobZipCodeViewController Small" bundle:nil];
        ZipCodeObj.strZipcode = strZipcode;
        [self.navigationController pushViewController:ZipCodeObj animated:YES];
    }
    
}
@end
