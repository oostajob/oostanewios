//
//  OostaJobContractorCalendarViewController.m
//  OostaJob
//
//  Created by Apple on 21/12/15.
//  Copyright © 2015 Armor. All rights reserved.
//

#import "OostaJobContractorCalendarViewController.h"
#import "SWRevealViewController.h"
@interface OostaJobContractorCalendarViewController ()<SWRevealViewControllerDelegate,CKCalendarDelegate>
{
    NSDateFormatter *dateFormat;
    NSMutableArray *arrSelectedTimings,*arrSelectedTimingsCustomer;
    NSMutableDictionary * dictSelectedTimings;
    NSMutableDictionary * dictCustomerSelectedTimings;
    NSDate * selectedDate;
    NSMutableArray *arrDatesofMonth;
}
@end

@implementation OostaJobContractorCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    // Do any additional setup after loading the view from its nib.
    [self setupUI];
    [self.imgBlur assignBlur];
    SWRevealViewController *revealController = [self revealViewController];
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];

    for (UIButton * btn in self.btnTimings)
    {
        [btn setImage:[UIImage imageNamed:@"circle-mark.png"] forState:UIControlStateNormal];
    }
    
    [self settingTheCalendarView];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        _lblHeader.font = [UIFont fontWithName:FONT_BOLD size:30.0];
        _lblSchedule.font = [UIFont fontWithName:FONT_BOLD size:20.0];
        for (UILabel *lblTimeing in _lblTimings)
        {
            lblTimeing.font = [UIFont fontWithName:FONT_BOLD size:18.0];
        }
        _btnFulldayNotAvailable.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:20.0];
        _btnReset.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:20.0];
        _btnDone.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:20.0];
    }
    else
    {
        _lblHeader.font = [UIFont fontWithName:FONT_BOLD size:20.0];
        _lblSchedule.font = [UIFont fontWithName:FONT_BOLD size:14.0];
        for (UILabel *lblTimeing in _lblTimings)
        {
            lblTimeing.font = [UIFont fontWithName:FONT_BOLD size:10.0];
        }
        _btnFulldayNotAvailable.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:13.0];
        _btnReset.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:13.0];
        _btnDone.titleLabel.font = [UIFont fontWithName:FONT_BOLD size:16.0];
    }
    _btnReset.layer.cornerRadius = 3.0f;
    _btnReset.layer.masksToBounds = YES;
    
    _btnFulldayNotAvailable.layer.cornerRadius = 3.0f;
    _btnFulldayNotAvailable.layer.masksToBounds = YES;
    
}
#pragma mark Calendar
-(void)settingTheCalendarView
{
    dictSelectedTimings = [[NSMutableDictionary alloc]init];
    dictCustomerSelectedTimings = [[NSMutableDictionary alloc]init];
    arrSelectedTimings =[[NSMutableArray alloc]initWithObjects:@"0",@"0",@"0",@"0",@"0", nil];
    selectedDate = [[NSDate date] dateByAddingTimeInterval:60*60*24];
   
    NSLog(@"Selected Date:%@",selectedDate);
    
    self.calendar.delegate = self;
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.calendar.titleFont =[UIFont fontWithName:FONT_BOLD size:18.0f];
    self.calendar.dateOfWeekFont =[UIFont fontWithName:FONT_BOLD size:15.0f];
    dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    
    [dictSelectedTimings setObject:arrSelectedTimings forKey:[dateFormat stringFromDate:selectedDate]];
    
    
    NSLog(@"dictSelectedTimings:%@",dictSelectedTimings);
    self.calendar.onlyShowCurrentMonth = NO;
    self.calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    self.calendar.backgroundColor=[UIColor whiteColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    
    
    
    arrDatesofMonth =[[NSMutableArray alloc]init];
    NSDate *nextDate;
    NSDate *startDate = [self startOfMonth:selectedDate];
    NSDate *endDate = [self getLastDateMonth:selectedDate];
    for ( nextDate = startDate ; [nextDate compare:endDate] < 0 ; nextDate = [nextDate dateByAddingTimeInterval:24*60*60] )
    {
        [arrDatesofMonth addObject:nextDate];
    }
    NSLog(@"arrDatesofMonth:%@",arrDatesofMonth);
    [self getCalendarDetailsndCompletionHandler:^(bool resultCalendarDetails) {
        NSLog(@"Calendar Details");
    }];
}

- (void)localeDidChange
{
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date
{
    if ([date compare:[NSDate date]]== NSOrderedAscending)
    {
        return YES;
    }
    return NO;
}

#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date
{
    // TODO: play with the coloring if we want to...
    if ([self dateIsDisabled:date])
    {
        dateItem.backgroundColor = [UIColor whiteColor];
        dateItem.textColor = [UIColor lightGrayColor];
        dateItem.disable = @"YES";
    }
    else if ([[dateFormat stringFromDate:date] isEqualToString:[dateFormat stringFromDate:selectedDate] ])
    {
        NSArray *arr = [[NSArray alloc]initWithObjects:@"1",@"1",@"1",@"1",@"1", nil];
        NSArray *arr1 = [[NSArray alloc]initWithArray:[dictSelectedTimings valueForKeyPath:[dateFormat stringFromDate:date]]];
        arr=[arr sortedArrayUsingSelector:@selector(compare:)];
        arr1=[arr1 sortedArrayUsingSelector:@selector(compare:)];
        
        if ([arr isEqualToArray:arr1])
        {
            dateItem.backgroundColor = [UIColor lightGrayColor];
            dateItem.selectedBackgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            dateItem.backgroundColor = [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            dateItem.selectedBackgroundColor = [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
        }
        
        dateItem.selectedTextColor = [UIColor whiteColor];
        dateItem.textColor = [UIColor whiteColor];
        dateItem.disable = @"SELECTED";

    }
    else
    {
        NSArray *arr = [[NSArray alloc]initWithObjects:@"1",@"1",@"1",@"1",@"1", nil];
        NSArray *arr1 = [[NSArray alloc]initWithArray:[dictSelectedTimings valueForKeyPath:[dateFormat stringFromDate:date]]];
        arr=[arr sortedArrayUsingSelector:@selector(compare:)];
        arr1=[arr1 sortedArrayUsingSelector:@selector(compare:)];
        
        if ([arr isEqualToArray:arr1])
        {
            dateItem.backgroundColor = [UIColor lightGrayColor];
            dateItem.selectedBackgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            dateItem.backgroundColor = [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
            dateItem.selectedBackgroundColor = [UIColor colorWithRed:0.984 green:0.000 blue:0.451 alpha:1.000];
        }
        dateItem.selectedTextColor = [UIColor whiteColor];
        dateItem.textColor = [UIColor whiteColor];
        dateItem.disable = @"NO";
    }
    
    
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    return ![self dateIsDisabled:date];
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date
{
    if ([selectedDate compare:date] != NSOrderedSame)
    {
        selectedDate = date;
        if ([self containsKey:[dateFormat stringFromDate:date]]==YES)
        {
            arrSelectedTimings =[[NSMutableArray alloc]initWithArray:[[dictSelectedTimings valueForKeyPath:[dateFormat stringFromDate:date]] mutableCopy]];
        }
        else
        {
            arrSelectedTimings =[[NSMutableArray alloc]initWithObjects:@"0",@"0",@"0",@"0",@"0", nil];
            
        }
       
        [self checkTheButtons:arrSelectedTimings];
        
        if ([self containsCustomerKey:[dateFormat stringFromDate:date]]==YES)
        {
            arrSelectedTimingsCustomer =[[NSMutableArray alloc]initWithArray:[[dictCustomerSelectedTimings valueForKeyPath:[dateFormat stringFromDate:date]] mutableCopy]];
            [self checkTheButtonsForDisable:arrSelectedTimingsCustomer];
        }
//        else
//        {
//            arrSelectedTimingsCustomer =[[NSMutableArray alloc]initWithObjects:@"0",@"0",@"0",@"0",@"0", nil];
//            
//        }
        
        
    }
    
    
}
- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date
{
    arrDatesofMonth = [[NSMutableArray alloc]init];
    NSDate *nextDate;
    NSDate *startDate = [self startOfMonth:[date dateByAddingTimeInterval:24*60*60]];
    NSDate *endDate = [self getLastDateMonth:[date dateByAddingTimeInterval:24*60*60]];
    for ( nextDate = startDate ; [nextDate compare:endDate] < 0 ; nextDate = [nextDate dateByAddingTimeInterval:24*60*60] )
    {
        [arrDatesofMonth addObject:nextDate];
    }
    NSLog(@"arrDatesofMonth:%@",arrDatesofMonth);
    return YES;
}
- (void)calendar:(CKCalendarView *)calendar didLayoutInRect:(CGRect)frame
{
    NSLog(@"calendar layout: %@", NSStringFromCGRect(frame));
    CGRect frameOther = _viewOtherContents.frame;
    frameOther.origin.y = IS_IPAD?(self.calendar.frame.origin.y + self.calendar.frame.size.height+14):self.calendar.frame.origin.y + self.calendar.frame.size.height;
    _viewOtherContents.frame = frameOther;
    _ScrlVw.scrollEnabled=YES;
    _ScrlVw.contentSize =CGSizeMake(_ScrlVw.frame.size.width, frameOther.origin.y+frameOther.size.height);

}

- (IBAction)btnFulldayNotAvailablePressed:(id)sender
{
    arrSelectedTimings =[[NSMutableArray alloc]initWithObjects:@"1",@"1",@"1",@"1",@"1", nil];
    [self checkTheButtons:arrSelectedTimings];
    [self.calendar reloadData];
    
    if ([self containsCustomerKey:[dateFormat stringFromDate:selectedDate]]==YES)
    {
        arrSelectedTimingsCustomer =[[NSMutableArray alloc]initWithArray:[[dictCustomerSelectedTimings valueForKeyPath:[dateFormat stringFromDate:selectedDate]] mutableCopy]];
        [self checkTheButtonsForDisable:arrSelectedTimingsCustomer];
    }
}

- (IBAction)btnResetPressed:(id)sender
{
    BOOL isContain = NO;
    for (int i=0; i<arrDatesofMonth.count; i++)
    {
        NSDate *date = arrDatesofMonth[i];
        if ([self containsKey:[dateFormat stringFromDate:date]])
        {
            [dictSelectedTimings removeObjectForKey:[dateFormat stringFromDate:date]];
            isContain = YES;
        }
    }
    if (isContain==YES)
    {
        arrSelectedTimings =[[NSMutableArray alloc]initWithObjects:@"0",@"0",@"0",@"0",@"0", nil];
        [self checkTheButtons:arrSelectedTimings];
        [self.calendar reloadData];
    }
    
    if ([self containsCustomerKey:[dateFormat stringFromDate:selectedDate]]==YES)
    {
        arrSelectedTimingsCustomer =[[NSMutableArray alloc]initWithArray:[[dictCustomerSelectedTimings valueForKeyPath:[dateFormat stringFromDate:selectedDate]] mutableCopy]];
        [self checkTheButtonsForDisable:arrSelectedTimingsCustomer];
    }
    
}
- (IBAction)btnTimingPressed:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    if (![[[[dictCustomerSelectedTimings valueForKeyPath:[dateFormat stringFromDate:selectedDate]] mutableCopy] objectAtIndex:btn.tag-501] isEqualToString:@"1"])
    {
        int val = 0;
        if ([UIImagePNGRepresentation(btn.currentImage) isEqualToData:UIImagePNGRepresentation([UIImage imageNamed:@"circle-close.png"])]){
            val = 0;
            [btn setImage:[UIImage imageNamed:@"circle-mark.png"] forState:UIControlStateNormal];
        }
        else
        {
            [btn setImage:[UIImage imageNamed:@"circle-close.png"] forState:UIControlStateNormal];
            val = 1;
        }
        [arrSelectedTimings replaceObjectAtIndex:btn.tag-501 withObject:[NSString stringWithFormat:@"%d", val]];
        
        [dictSelectedTimings setObject:arrSelectedTimings forKey:[dateFormat stringFromDate:selectedDate]];
        
        NSArray *arr = [[NSArray alloc]initWithObjects:@"1",@"1",@"1",@"1",@"1", nil];
        NSArray *arr1 = [[NSArray alloc]initWithArray:arrSelectedTimings];
        arr=[arr sortedArrayUsingSelector:@selector(compare:)];
        arr1=[arr1 sortedArrayUsingSelector:@selector(compare:)];
        [self.calendar reloadData];
        NSLog(@"dictSelectedTimings_check:%@",dictSelectedTimings);
    }
    
    
    
}
- (IBAction)btnMenuTapped:(id)sender
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}

- (BOOL)containsKey: (NSString *)key {
    NSLog(@"key==>>%@",key);
    BOOL retVal = 0;
    NSArray *allKeys = [dictSelectedTimings allKeys];
     NSLog(@"allKeys==>>%@",allKeys);
    retVal = [allKeys containsObject:key];
    NSLog(@"retVal==>>%d",retVal);
    return retVal;
}

- (BOOL)containsCustomerKey: (NSString *)key
{
    NSLog(@"key==>>%@",key);
    BOOL retVal = 0;
    NSArray *allKeys = [dictCustomerSelectedTimings allKeys];
    NSLog(@"allKeys==>>%@",allKeys);
    retVal = [allKeys containsObject:key];
     NSLog(@"retVal==>>%d",retVal);
    return retVal;
}




-(NSDate *)getLastDateMonth:(NSDate *)currDate
{
    
    NSCalendar *gregCalendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components=[gregCalendar components:NSMonthCalendarUnit|NSYearCalendarUnit fromDate:currDate];
    NSInteger month=[components month];
    NSInteger year=[components year];
    
    if (month==12) {
        [components setYear:year+1];
        [components setMonth:1];
    }
    else {
        [components setMonth:month+1];
    }
    [components setDay:1];
    
    return [[gregCalendar dateFromComponents:components] dateByAddingTimeInterval:0];
    
}
-(void)checkTheButtons:(NSMutableArray *)CheckArr
{
    arrSelectedTimings = [[NSMutableArray alloc]init];
    
    NSLog(@"CheckArr==>>%@",CheckArr);
    
    for (int i=0; i<[CheckArr count]; i++)
    {
        if ([[CheckArr objectAtIndex:i] isEqualToString:@"1"])
        {
            [(UIButton *)[self.viewOtherContents viewWithTag:501+i] setImage:[UIImage imageNamed:@"circle-close.png"] forState:UIControlStateNormal];
            
//            [(UIButton *)[self.viewOtherContents viewWithTag:501+i] setUserInteractionEnabled:NO];
            
            
        }
        else
        {
//            [(UIButton *)[self.viewOtherContents viewWithTag:501+i] setUserInteractionEnabled:YES];
            
            [(UIButton *)[self.viewOtherContents viewWithTag:501+i] setImage:[UIImage imageNamed:@"circle-mark.png"] forState:UIControlStateNormal];
        }
        [arrSelectedTimings addObject:[CheckArr objectAtIndex:i]];
        NSLog(@"check button val==>>%@",arrSelectedTimings);
    }
    
    [dictSelectedTimings setObject:arrSelectedTimings forKey:[dateFormat stringFromDate:selectedDate]];
    NSLog(@"dictSelectedTimings:%@",dictSelectedTimings);
}

-(void)checkTheButtonsForDisable:(NSMutableArray *)CheckArr
{
    
    NSLog(@"CheckCustomerArr==>>%@",CheckArr);
    
    for (int i=0; i<[CheckArr count]; i++)
    {
        if ([[CheckArr objectAtIndex:i] isEqualToString:@"1"])
        {
            [(UIButton *)[self.viewOtherContents viewWithTag:501+i] setImage:[UIImage imageNamed:@"closeNew.png"] forState:UIControlStateNormal];
            
        }
     }
    
    
}





-(NSDate *) startOfMonth:(NSDate *)currDate
{
    NSCalendar * calendar = [NSCalendar currentCalendar];
    
    NSDateComponents * currentDateComponents = [calendar components: NSYearCalendarUnit | NSMonthCalendarUnit fromDate: currDate];
    NSDate * startOfMonth = [calendar dateFromComponents: currentDateComponents];
    
    return startOfMonth;
}
- (IBAction)btnDonePressed:(id)sender
{
    [self updateCalendarandCompletionHandler:^(bool resultCalendar) {
        if (resultCalendar == YES)
        {
            NSLog(@"Calendar Completed");
        }
    }];
}

#pragma mark-Update Calendar
-(void)updateCalendarandCompletionHandler:(void (^)(bool resultCalendar))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Scheduling...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    
    
    NSMutableArray *arrCalendar = [[NSMutableArray alloc]init];
    NSArray *arrAllKeys = [dictSelectedTimings allKeys];
    NSMutableArray *arrAvailableTimings =[[NSMutableArray alloc]initWithObjects:@"8am-10am",@"10am-12pm",@"12pm-2pm",@"2pm-4pm",@"4pm-6pm", nil];
    
    for (int i=0; i<arrAllKeys.count; i++)
    {
        NSArray *arr = [[NSArray alloc]initWithObjects:@"0",@"0",@"0",@"0",@"0", nil];
        NSArray *arr1 = [[NSArray alloc]initWithArray:[dictSelectedTimings valueForKeyPath:arrAllKeys[i]]];
        NSArray *arr2 = [[NSArray alloc]initWithArray:[dictCustomerSelectedTimings valueForKeyPath:arrAllKeys[i]]];
        arr=[arr sortedArrayUsingSelector:@selector(compare:)];
        arr1=[arr1 sortedArrayUsingSelector:@selector(compare:)];
        
        if ([arr isEqualToArray:arr1]&&[arr isEqualToArray:arr2])
        {
            [dictSelectedTimings removeObjectForKey:arrAllKeys[i]];
        }
    }
    if (arrAllKeys!=nil)
    {
        arrAllKeys = [[NSArray alloc]init];
    }
    arrAllKeys = [dictSelectedTimings allKeys];
    
    for (int i=0; i<arrAllKeys.count; i++)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:arrAllKeys[i] forKey:@"date"];
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
        NSMutableArray *arrTime = [[NSMutableArray alloc]init];
        
        for (int j=0; j<[[dictSelectedTimings valueForKey:arrAllKeys[i]] count]; j++)
        {
            if ([[[dictSelectedTimings valueForKey:arrAllKeys[i]] objectAtIndex:j] isEqualToString:@"1"]||[[[dictCustomerSelectedTimings valueForKey:arrAllKeys[i]] objectAtIndex:j] isEqualToString:@"1"])
            {
                [arrTime addObject:arrAvailableTimings[j]];
            }
            
            
        }
        NSString *strSelectedTimings =[NSString stringWithFormat:@"%@",arrTime];
        strSelectedTimings = [[strSelectedTimings componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
        NSString *trimmedString = [strSelectedTimings stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceCharacterSet]];
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@",    " withString:@","];
        [dict setObject:trimmedString forKey:@"Times"];
        [arrCalendar addObject:dict];
    }
    
    NSString * key1 =@"Calander";
    
    
    NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                   initWithObjects:@[arrCalendar]
                                   forKeys:@[key1]];
    
    NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
    NSLog(@"DATA %@",jsonString);
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@updatecalander/%@",BaseURL,[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"]]]];
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSLog(@"resultCalendar URL:%@",request.URL);
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
    
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             
             NSLog(@"Server Error : %@", error);
             
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             completionHandler(true);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
             {
                 
                 [self.navigationController popViewControllerAnimated:YES];
                 [hud hide:YES];
                 MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                 [self.navigationController.view addSubview:hud];
                 
                 hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                 hud.mode = MBProgressHUDModeCustomView;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                 hud.delegate = self;
                 hud.labelText = @"Scheduled Successfully";
                 [hud show:YES];
                 [hud hide:YES afterDelay:2];
                 
             }
             else
             {
                 NSLog(@"%@",SERVER_ERR);
                 
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 
             }
             
             completionHandler(true);
         }
     }];
    }
}

/*
-(void)arrayBooleanValueBasedOnNotAvaliableDate:(NSMutableArray *)arrDate timings:(NSMutableArray *)arrTime availableTimings:(NSMutableArray *)arrAvailableTimings storingDictionary:(NSMutableDictionary *)dictSelectedTimings
{
    for (int i=0; i<arrDate.count; i++)
    {
        NSMutableArray *arrTimings=[[NSMutableArray alloc]initWithObjects:@"0",@"0",@"0",@"0",@"0", nil];
        NSArray *arrServiceTimings = [[NSString stringWithFormat:@"%@",arrTime[i]] componentsSeparatedByString:@","];
        
        NSLog(@"ArrayServiceTimings==>>%@",arrServiceTimings);
        
        for (int j=0; j<arrServiceTimings.count; j++)
        {
            if ([arrAvailableTimings containsObject:arrServiceTimings[j]])
            {
                NSUInteger index = [arrAvailableTimings indexOfObject:arrServiceTimings[j]];
                [arrTimings replaceObjectAtIndex:index withObject:@"1"];
            }
            
        }
        
        NSLog(@"ArrayTimings==>>%@",arrTimings);
        [dictSelectedTimings setObject:arrTimings forKey:arrDate[i]];
        
        
    }

}

*/


-(void)getCalendarDetailsndCompletionHandler:(void (^)(bool resultCalendarDetails))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Loading Previous Timings...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getcalender/%@",BaseURL,[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"]]]];
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSLog(@"getcalender:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             completionHandler(true);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 [hud hide:YES];
                 
                 NSMutableArray *arrDate = [[NSMutableArray alloc]init];
                 NSMutableArray *arrTime = [[NSMutableArray alloc]init];
                 arrDate = [result valueForKeyPath:@"CalenderDetails.notAvailabledate"];
                 arrTime = [result valueForKeyPath:@"CalenderDetails.notAvailableTime"];
                 NSLog(@"array Date==>>%@",arrDate);
                 NSLog(@"array Timing==>>%@",arrTime);
            
                 //for Customer Disable
                 NSMutableArray *arrCustomerDate = [[NSMutableArray alloc]init];
                 NSMutableArray *arrCustomerTime = [[NSMutableArray alloc]init];
                 arrCustomerDate = [result valueForKeyPath:@"CalenderDetailsCustomer.notAvailabledate"];
                 arrCustomerTime = [result valueForKeyPath:@"CalenderDetailsCustomer.notAvailableTime"];
                 NSLog(@"arrCustomerDate==>>%@",arrCustomerDate);
                 NSLog(@"arrCustomerTime==>>%@",arrCustomerTime);
             
                NSMutableArray *arrAvailableTimings =[[NSMutableArray alloc]initWithObjects:@"8am-10am",@"10am-12pm",@"12pm-2pm",@"2pm-4pm",@"4pm-6pm", nil];
                 
//                 [self arrayBooleanValueBasedOnNotAvaliableDate:arrDate timings:arrTime availableTimings:arrAvailableTimings];
//                 
//                 [self arrayBooleanValueBasedOnNotAvaliableDate:arrCustomerDate timings:arrCustomerTime availableTimings:arrAvailableTimings];
       
                 for (int i=0; i<arrDate.count; i++)
                 {
                     NSMutableArray *arrTimings=[[NSMutableArray alloc]initWithObjects:@"0",@"0",@"0",@"0",@"0", nil];
                     
                     NSArray *arrServiceTimings = [[NSString stringWithFormat:@"%@",arrTime[i]] componentsSeparatedByString:@","];
                     
                     NSLog(@"ArrayServiceTimings==>>%@",arrServiceTimings);
                     
                     for (int j=0; j<arrServiceTimings.count; j++)
                     {
                         if ([arrAvailableTimings containsObject:arrServiceTimings[j]])
                         {
                             NSUInteger index = [arrAvailableTimings indexOfObject:arrServiceTimings[j]];
                             [arrTimings replaceObjectAtIndex:index withObject:@"1"];
                         }
                         
                     }
                     
                     NSLog(@"ArrayTimings==>>%@",arrTimings);
                     [dictSelectedTimings setObject:arrTimings forKey:arrDate[i]];
                     
                 }

                 NSLog(@"SelectedTimings_Contractor==>>%@",dictSelectedTimings);
                 
                 //Customer time enhancement
                 for (int i=0; i<arrCustomerDate.count; i++)
                 {
                     NSMutableArray *arrTimings=[[NSMutableArray alloc]initWithObjects:@"0",@"0",@"0",@"0",@"0", nil];
                     NSArray *arrServiceTimings = [[NSString stringWithFormat:@"%@",arrCustomerTime[i]] componentsSeparatedByString:@","];
                     
                     NSLog(@"ArrayCustomerServiceTimings[%d]==>>%@",i,arrServiceTimings);
                     
                     for (int j=0; j<arrServiceTimings.count; j++)
                     {
                         if ([arrAvailableTimings containsObject:arrServiceTimings[j]])
                         {
                             NSUInteger index = [arrAvailableTimings indexOfObject:arrServiceTimings[j]];
                             [arrTimings replaceObjectAtIndex:index withObject:@"1"];
                         }
                         
                     }
                     
                     NSLog(@"ArrayCustomerTimings==>>%@",arrTimings);
                     [dictCustomerSelectedTimings setObject:arrTimings forKey:arrCustomerDate[i]];
                     
                     
                 }
                 
                 NSLog(@"dictCustomerSelectedTimings==>>%@",dictCustomerSelectedTimings);
                 
               
                     if ([self containsKey:[dateFormat stringFromDate:selectedDate]]==YES)
                     {
                         arrSelectedTimings =[[NSMutableArray alloc]initWithArray:[[dictSelectedTimings  valueForKeyPath:[dateFormat stringFromDate:selectedDate]] mutableCopy]];
                         
                        
                         [self checkTheButtons:arrSelectedTimings];
                         
                         NSLog(@"arrSelectedTimings==>>%@",arrSelectedTimings);
                         
                         
                     }
                 
                 NSLog(@"dictSelectedTimings:%@",dictSelectedTimings);
                 [self.calendar reloadData];
                 
                 
            
                 if ([self containsCustomerKey:[dateFormat stringFromDate:selectedDate]]==YES)
                 {
                     arrSelectedTimingsCustomer =[[NSMutableArray alloc]initWithArray:[[dictCustomerSelectedTimings  valueForKeyPath:[dateFormat stringFromDate:selectedDate]] mutableCopy]];
                     
                    
                     [self checkTheButtonsForDisable:arrSelectedTimingsCustomer];
                     
                     NSLog(@"arrCustomerSelectedTimings==>>%@",arrSelectedTimings);
                     
                 }
       
                 NSLog(@"dictSelectedTimings:%@",dictSelectedTimings);
                 [self.calendar reloadData];
                 completionHandler(true);
                 
             }
             else
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(true);
             }
         }
     }];
    }
}


@end
