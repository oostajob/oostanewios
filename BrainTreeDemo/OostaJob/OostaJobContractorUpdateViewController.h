//
//  OostaJobCustomerRegisterationViewController.h
//  OostaJob
//
//  Created by Apple on 20/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PECropViewController.h"
#import "LocationPickerViewController.h"
@protocol OostaJobContractorUpdateViewControllerDelegate <NSObject>
@optional
- (void)getUpdate;
@end
@interface OostaJobContractorUpdateViewController : UIViewController<PECropViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,LocationPickerViewControllerDelegate,UITextFieldDelegate,MBProgressHUDDelegate>

@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIView *viewName;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusName;
@property (weak, nonatomic) IBOutlet UIView *viewEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusEmail;
@property (weak, nonatomic) IBOutlet UIView *viewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusPassword;
@property (weak, nonatomic) IBOutlet UIView *viewAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusAddress;
@property (weak, nonatomic) IBOutlet UIView *viewPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnPhotoCapture;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (weak, nonatomic) IBOutlet UILabel *lblIAgree;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsConditions;
- (IBAction)btnBackTapped:(id)sender;
- (IBAction)btnRegisterTapped:(id)sender;
- (IBAction)btnCameraTapped:(id)sender;
- (IBAction)btnCheckBoxTapped:(id)sender;
- (IBAction)btnTermsandConditionTapped:(id)sender;
- (IBAction)btnAddressTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtSSNNumber;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusSSN;
@property (weak, nonatomic) IBOutlet UIView *viewSSN;
@property (weak, nonatomic) IBOutlet UIView *viewExpertise;
@property (weak, nonatomic) IBOutlet UITextField *txtExpertise;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusExpertise;
@property (weak, nonatomic) IBOutlet UIView *viewHourlyRate;
@property (weak, nonatomic) IBOutlet UITextField *txtHourlyRate;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusHourlyRate;
@property (weak, nonatomic) IBOutlet UIView *viewLicense;
@property (weak, nonatomic) IBOutlet UITextField *txtLicense;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlusLicense;
- (IBAction)btnExpertise:(id)sender;
- (IBAction)btnLicensePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgLicense;
@property (strong, nonatomic) IBOutlet UIView *viewTermsAndConditions;
@property (weak, nonatomic) IBOutlet UIWebView *webViewTermsAndConditions;
@property (weak, nonatomic) IBOutlet UILabel *lblTermsAndConditions;
- (IBAction)btnTermsAndConditionsClosePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblExpertiseAt;
@property (weak, nonatomic) IBOutlet UIButton *btnExpertiseAtDone;
- (IBAction)btnExpertiseAtDonePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblExpertise;
@property (strong, nonatomic) IBOutlet UIView *viewExpertiseAt;
@property (strong, nonatomic) NSString *strEx;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)btnChangePassword:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewChangePassword;
@property (weak, nonatomic) IBOutlet UIView *viewContentChangePasswod;
@property (weak, nonatomic) IBOutlet UILabel *lblChangePasswordHeader;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)btnSubmitPressed:(id)sender;
- (IBAction)btnClosePressed:(id)sender;

@property (nonatomic, assign)   id<OostaJobContractorUpdateViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblUploadYourPicture;
@end
