//
//  OostaJobAboutUsViewController.h
//  OostaJob
//
//  Created by Apple on 17/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OostaJobAboutUsViewController : UIViewController<MBProgressHUDDelegate>
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBg;
- (IBAction)btnMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIWebView *webviewAboutUs;

@end
