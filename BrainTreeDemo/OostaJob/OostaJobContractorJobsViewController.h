//
//  OostaJobContractorJobsHomeViewController.h
//  OostaJob
//
//  Created by Armor on 20/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OostaJobContractorJobsViewController : UIViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate>
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UIButton *btnBidding;
@property (strong, nonatomic) IBOutlet UIButton *btnHire;
@property (strong, nonatomic) IBOutlet UIButton *btnRatenow;
@property (strong, nonatomic) IBOutlet UIButton *btnClosed;
@property (strong, nonatomic) IBOutlet UITableView *tblBidding;
@property (strong, nonatomic) IBOutlet UITableView *tblHire;
@property (strong, nonatomic) IBOutlet UITableView *tblRatenow;
@property (strong, nonatomic) IBOutlet UITableView *tblClosed;
@property (strong, nonatomic) IBOutlet UIView *viewListView;
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;
@property (strong, nonatomic) IBOutlet UIButton *btnRequestJob;
-(IBAction)btnTapped:(id)sender;
- (IBAction)btnMenuTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;
- (IBAction)requestaService:(id)sender;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *viewController;

@property (strong, nonatomic) IBOutlet UIViewController *optionViewController;
@property (nonatomic, retain) UIPopoverController *customTblPopover;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
- (IBAction)btnCancelPressed:(id)sender;
- (IBAction)btnClosePressed:(id)sender;
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, retain) NSArray *list;
@property (strong, nonatomic) UIView *dropDown;
@property (strong, nonatomic) IBOutlet UIButton *btnList;
@property (strong, nonatomic) NSString *strHome;
@property (strong, nonatomic) NSString *strBids;
@end
