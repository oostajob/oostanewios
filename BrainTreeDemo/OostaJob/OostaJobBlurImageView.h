//
//  OostaJobBlurImageView.h
//  OostaJob
//
//  Created by Apple on 20/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AsyncImageView.h"

@interface OostaJobBlurImageView : AsyncImageView
-(void)assignBlur;
@end
