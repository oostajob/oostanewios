//
//  OostaJobReviewViewController.h
//  OostaJob
//
//  Created by Apple on 13/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CKCalendarView.h"
@interface OostaJobReviewViewController : UIViewController<CKCalendarDelegate>
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (strong, nonatomic) IBOutlet UIImageView *imgSroll;
@property (strong, nonatomic) IBOutlet UICollectionView *cllView;
@property (strong, nonatomic) IBOutlet UIScrollView *ScrolView;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblWork;
@property (strong, nonatomic) IBOutlet UILabel *lblReviews;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblRates;
@property (strong, nonatomic) IBOutlet UIButton *BtnbidAmount;
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) IBOutlet UIButton* UpArrowBtn;
@property (strong, nonatomic) UIImage *imgBackGroundImage;
@property (strong, nonatomic) NSMutableArray *arrSelected;
@property (strong, nonatomic) NSString * indexofSelected;
-(IBAction)UpArrowBtnTouched:(id)sender;
- (IBAction)BackBtnTouched:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *HomeBtnTouched;
- (IBAction)btnReview:(id)sender;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgStars;
@property (weak, nonatomic) IBOutlet UIView *viewImgStars;

@property (strong, nonatomic)NSString *strUserId;
@property (strong, nonatomic)NSString *strJobpostID;
@property (strong, nonatomic)NSString *ContractorID;

//Calendar
@property (strong, nonatomic) IBOutlet UIView *viewCalander;
@property (weak, nonatomic) IBOutlet UIView *viewContentCalendar;
@property (weak, nonatomic) IBOutlet CKCalendarView *calendar;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedDate;
@property (weak, nonatomic) IBOutlet UIButton *btnMeet;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UIView *viewOtherContents;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerViewAvailableTimings;
- (IBAction)btnMeetUpPressed:(id)sender;
- (IBAction)btnSkipPressed:(id)sender;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property (weak, nonatomic) IBOutlet UIScrollView *scrlCalendar;
- (IBAction)btnCalendarClosedTapped:(id)sender;
- (IBAction)btnNitNow:(id)sender;
- (IBAction)btnHomePressed:(id)sender;

@end
