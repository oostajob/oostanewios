//
//  OostaJobCustomAnnotation.m
//  OostaJob
//
//  Created by Apple on 12/12/15.
//  Copyright © 2015 Armor. All rights reserved.
//

#import "OostaJobCustomAnnotation.h"


@implementation OostaJobCustomAnnotation

@synthesize coordinate;

@synthesize title;

@synthesize time;

@synthesize subTitle;

-(id)initWithCoordinate:(CLLocationCoordinate2D) c  title:(NSString *) t  subTitle:(NSString *)timed time:(NSString *)tim
{
    self.coordinate=c;
    self.time=tim;
    self.subTitle=timed;
    self.title=t;
    return self;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D) c title:(NSString *)tit
{
    self.coordinate=c;
    self.title=tit;
    return self;
}

@end