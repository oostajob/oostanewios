//
//  OostaJobCustomerBuildJobViewController.m
//  OostaJob
//
//  Created by Apple on 11/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobBuildJobViewController.h"
#import "ImageCache.h"
#import "OostaJobListOfJobsViewController.h"
#import "OostaJobDescriptionViewController.h"
#import "LocationPickerViewController.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "OostaJobMediaCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "MXLMediaView.h"
#import "UIImage+ImageCompress.h"
#import "OostaJobLoginViewController.h"
#import "OostaJobRegisterViewController.h"
#import "OostaJobCustomerJobsViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "OostaJobZipCodeViewController.h"
#import <Photos/Photos.h>

@interface OostaJobBuildJobViewController ()<LocationPickerViewControllerDelegate,MXLMediaViewDelegate>
{
    ALAssetsLibrary * assetsLibrary_;
    UIImage *choosenImage;
    KLCPopup* popupViewCatagory;
    KLCPopup* popupViewGreat;
    NSArray *cityArr;
    NSUInteger currentSubTypeIndex;
    NSUInteger currentTappedIndex;
    NSMutableArray *arrTableDataList;
    NSMutableDictionary *dictSelectedIndexArr;
    NSMutableDictionary *dictSelectedValueArr;
    NSMutableDictionary *dictValueArr;
    NSMutableDictionary *dictQuestArr;
    NSMutableDictionary *dictQuestTypeArr;
    NSMutableDictionary *dictTagArr;
    NSMutableDictionary *dictTagKeyArr;
    NSMutableDictionary *dictMedia;
    NSMutableDictionary *dictSelected;
    NSIndexPath *currentIndexPath;
    
    NSString * strLongitude;
    NSString * strLatitude;
    NSString * strZipcode;
    NSString * strAddress;
    NSUInteger mediaCount;
    NSMutableArray *arrKeys;
    CAKeyframeAnimation * anim;
    NSURL *tmpVideoURl;
    
    BOOL isComeFromSkipLogin;
}
@end

@implementation OostaJobBuildJobViewController
@synthesize library;
- (void)viewDidLoad
{
    currentSubTypeIndex=0;
    strAddress=[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Address"];
    strLatitude=[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Lat"];
    strLongitude=[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Lng"];
    dictMedia = [[NSMutableDictionary alloc]init];
    mediaCount = 0;
    isComeFromSkipLogin = NO;
    if ([_strBuildJob isEqualToString:@"YES"]&&[[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"DICTSKIPLOGIN"] count]!=0)
    {
        isComeFromSkipLogin = YES;
        NSMutableDictionary * dictSkipLogin = [[NSMutableDictionary alloc]init];
        dictSkipLogin = [[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"DICTSKIPLOGIN"] mutableCopy];
        self.arrSelectedJob = [[NSMutableArray alloc]init];
        self.arrSelectedJob = [[dictSkipLogin valueForKeyPath:@"arrSelectedJob"] mutableCopy];
        _strOther = [NSString stringWithFormat:@"%@",[dictSkipLogin valueForKeyPath:@"strOther"]];
        strAddress = [NSString stringWithFormat:@"%@",[dictSkipLogin valueForKeyPath:@"strAddress"]];
        strLatitude = [NSString stringWithFormat:@"%@",[dictSkipLogin valueForKeyPath:@"strLatitude"]];
        strLongitude = [NSString stringWithFormat:@"%@",[dictSkipLogin valueForKeyPath:@"strLongitude"]];
        strZipcode = [NSString stringWithFormat:@"%@",[dictSkipLogin valueForKeyPath:@"strZipcode"]];
        
        dictSelected = [[NSMutableDictionary alloc]init];
        dictSelected = [[dictSkipLogin valueForKeyPath:@"dictSelected"] mutableCopy];
        
        dictSelectedIndexArr = [[NSMutableDictionary alloc]init];
        dictSelectedIndexArr =  (NSMutableDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:[dictSkipLogin valueForKeyPath:@"dictSelectedIndexArr"]];
        
        dictSelectedValueArr = [[NSMutableDictionary alloc]init];
        dictSelectedValueArr =  (NSMutableDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:[dictSkipLogin valueForKeyPath:@"dictSelectedValueArr"]];
        
        
        dictMedia = [[NSMutableDictionary alloc]init];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"dictMedia.plist"];
        dictMedia =  (NSMutableDictionary*) [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
        //        dictMedia =  (NSMutableDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:[dictSkipLogin valueForKeyPath:@"dictMedia"]];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DICTSKIPLOGIN"];
        
    }
    [super viewDidLoad];
    [self setupUI];
    currentTappedIndex = 1;
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.scrlView.scrollEnabled=YES;
    
    _lblJobType.text=[NSString stringWithFormat:@"%@",[_arrSelectedJob  valueForKeyPath:@"job_name"]];
    if ([_lblJobType.text isEqualToString:@"Others"])
    {
        _lblJobType.text=[NSString stringWithFormat:@"%@: %@",_lblJobType.text,_strOther];
    }
    
    
    [self setBackgroundImage];
    [self CheckInterNetConnection];
    
}
-(void)CheckInterNetConnection
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        [self getJobSubType];
        [self getCityDetails];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"SCREEN"] isEqualToString:@"REGISTEREDCUSTOMER"]||[[[NSUserDefaults standardUserDefaults]objectForKey:@"SCREEN"] isEqualToString:@"CUSTOMERJOBS"])
    {
        _PostBtn.hidden=NO;
        _viewRegister.hidden=YES;
    }
    else
    {
        _PostBtn.hidden=YES;
        _viewRegister.hidden=NO;
    }
    
}
-(void)setBackgroundImage
{
    NSLog(@"arrSelectedJob :%@",self.arrSelectedJob);
    NSString *imageCacheKey = [[self.arrSelectedJob  valueForKeyPath:@"job_image"] stringByDeletingPathExtension];
    if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
    {
        _imgBlur.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
        [self.imgBlur assignBlur];
    }
    else
    {
        NSURL *imageURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_arrSelectedJob  valueForKeyPath:@"job_image"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                self.imgBlur.image = [UIImage imageWithData:imageData];
                [[ImageCache sharedImageCache] storeImage:self.imgBlur.image withKey:imageCacheKey];
                [self.imgBlur assignBlur];
            });
        });
    }
    
    [self CreateBlurEffectOnBackgroundImage];
}
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.PostBtn.layer.cornerRadius=5.0;
        [self.PostBtn.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        self.lblTitle.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblTitle.textColor=[UIColor lightGrayColor];
        self.lblJobType.font=[UIFont fontWithName:FONT_BOLD size:23.0f];
        self.BtnWhatNext.layer.cornerRadius=5.0;
        [self.BtnWhatNext.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        self.lblNotice.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.lblDetails.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        _lblViewCatagory.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        [self.btnLogin.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        [self.btnRegister.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        
        
    }
    else
    {
        self.PostBtn.layer.cornerRadius=5.0;
        [self.PostBtn.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        self.lblTitle.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
        self.lblTitle.textColor=[UIColor lightGrayColor];
        self.lblJobType.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.BtnWhatNext.layer.cornerRadius=5.0;
        [self.BtnWhatNext.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        self.lblNotice.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblDetails.font=[UIFont fontWithName:FONT_THIN size:14.0f];
        _lblViewCatagory.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
        [self.btnLogin.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        [self.btnRegister.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
    }
    self.btnRegister.layer.cornerRadius=5.0;
    self.btnRegister.layer.masksToBounds=YES;
    self.btnLogin.layer.cornerRadius=5.0;
    self.btnLogin.layer.masksToBounds=YES;
}
-(void)CreateBlurEffectOnBackgroundImage
{
    [self.imgBlur assignBlur];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void)ViewCreation
{
    NSUInteger NumberofQuestions=arrCharacteristics.count ;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        for (int i=0; i<NumberofQuestions; i++)
        {
            
            UIView *viewCategory=[[UIView alloc]initWithFrame:CGRectMake(0, i*121, 768, 120)];
            viewCategory.backgroundColor=[UIColor colorWithWhite:1.000 alpha:0.200];
            viewCategory.tag=i+101;
            [self.scrlView addSubview:viewCategory];
            
            UILabel *lblCategory=[[UILabel alloc]initWithFrame:CGRectMake(8, 0, 700, 120)];
            
            lblCategory.numberOfLines=3;
            lblCategory.textColor=[UIColor whiteColor];
            lblCategory.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
            lblCategory.tag=i+201;
            [viewCategory addSubview:lblCategory];
            
            UIImageView *imgCatagory=[[UIImageView alloc]initWithFrame:CGRectMake(710, 40, 35, 35)];
            imgCatagory.image=[UIImage imageNamed:@"add.png"];
            imgCatagory.tag=i+301;
            [viewCategory addSubview:imgCatagory];
            
            UIButton *BtnCatagory = [UIButton buttonWithType:UIButtonTypeCustom];
            [BtnCatagory addTarget:self
                            action:@selector(CatagoryBtnTouched:)
                  forControlEvents:UIControlEventTouchUpInside];
            [BtnCatagory.titleLabel setFont:[UIFont systemFontOfSize:12]];
            [BtnCatagory setTitle:@"" forState:UIControlStateNormal];
            BtnCatagory.tag=i+401;
            BtnCatagory.frame = CGRectMake(0,0,768,120);
            [viewCategory addSubview:BtnCatagory];
            
            if (i==1)
            {
                lblCategory.text=[[JobSubTypeArr objectAtIndex:currentSubTypeIndex]valueForKeyPath:@"subtype_name"];
                imgCatagory.image=[UIImage imageNamed:@"tick.png"];
            }
            else if (i==2)
            {
                lblCategory.text=@"Select Job Address";
            }
            else if (i==0)
            {
                lblCategory.text=@"Include some pictures and videos of the job";
                UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
                flowLayout.itemSize = CGSizeMake(120, 120);
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                UICollectionView *collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, 700, 120) collectionViewLayout:flowLayout];
                collectionView.backgroundColor=[UIColor clearColor];
                collectionView.tag=501;
                collectionView.hidden=YES;
                collectionView.delegate=self;
                collectionView.dataSource=self;
                
                
                [viewCategory addSubview:collectionView];
                
                if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    [collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                }
                else
                {
                    [collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    
                }
            }
            //            else if (i==(NumberofQuestions-1))
            //            {
            //                lblCategory.text=@"Write down some notes about the job description, specifiation, size, etc.";
            //            }
            else
            {
                lblCategory.text=[[arrCharacteristics objectAtIndex:i] valueForKeyPath:@"char_name"];
                imgCatagory.image=[UIImage imageNamed:@"add.png"];
            }
            
            self.scrlView.contentSize=CGSizeMake(768,120*i);
        }
        self.scrlView.contentSize=CGSizeMake(768,self.scrlView.contentSize.height+150);
        
    }
    else
    {
        for (int i=0; i<NumberofQuestions; i++)
        {
            UIView *viewCategory=[[UIView alloc]initWithFrame:CGRectMake(0, i*61, 320, 60)];
            viewCategory.backgroundColor=[UIColor colorWithWhite:1.000 alpha:0.200];
            viewCategory.tag=i+101;
            [self.scrlView addSubview:viewCategory];
            
            UILabel *lblCategory=[[UILabel alloc]initWithFrame:CGRectMake(8, 0, 280, 60)];
            
            lblCategory.numberOfLines=3;
            lblCategory.textColor=[UIColor whiteColor];
            lblCategory.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
            lblCategory.tag=i+201;
            [viewCategory addSubview:lblCategory];
            
            UIImageView *imgCatagory=[[UIImageView alloc]initWithFrame:CGRectMake(295, 20, 16, 16)];
            imgCatagory.image=[UIImage imageNamed:@"add.png"];
            imgCatagory.tag=i+301;
            [viewCategory addSubview:imgCatagory];
            
            UIButton *BtnCatagory = [UIButton buttonWithType:UIButtonTypeCustom];
            [BtnCatagory addTarget:self
                            action:@selector(CatagoryBtnTouched:)
                  forControlEvents:UIControlEventTouchUpInside];
            [BtnCatagory.titleLabel setFont:[UIFont systemFontOfSize:12]];
            [BtnCatagory setTitle:@"" forState:UIControlStateNormal];
            BtnCatagory.tag=i+401;
            BtnCatagory.frame = CGRectMake(0,0,320,60);
            [viewCategory addSubview:BtnCatagory];
            if (i==1)
            {
                lblCategory.text=[[JobSubTypeArr objectAtIndex:currentSubTypeIndex]valueForKeyPath:@"subtype_name"];
                imgCatagory.image=[UIImage imageNamed:@"tick.png"];
            }
            else if (i==2)
            {
                lblCategory.text=@"Select Job Address";
            }
            else if (i==0)
            {
                lblCategory.text=@"Include some pictures and videos of the job";
                UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                flowLayout.itemSize = CGSizeMake(60, 60);
                UICollectionView *collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, 280, 60) collectionViewLayout:flowLayout];
                collectionView.backgroundColor=[UIColor clearColor];
                collectionView.tag=501;
                collectionView.hidden=YES;
                collectionView.delegate=self;
                collectionView.dataSource=self;
                
                
                [viewCategory addSubview:collectionView];
                
                if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                {
                    [collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell IPad" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                }
                else
                {
                    [collectionView registerNib:[UINib nibWithNibName:@"OostaJobMediaCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
                    
                }
            }
            //            else if (i==(NumberofQuestions-1))
            //            {
            //                lblCategory.text=@"Write down some notes about the job description, specifiation, size, etc.";
            //            }
            else
            {
                lblCategory.text=[[arrCharacteristics objectAtIndex:i] valueForKeyPath:@"char_name"];
                imgCatagory.image=[UIImage imageNamed:@"add.png"];
            }
            self.scrlView.contentSize=CGSizeMake(320,60*i);
        }
        self.scrlView.contentSize=CGSizeMake(320,self.scrlView.contentSize.height+90);
    }
}
-(void)getJobSubType
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Loading your needs...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getsubjobtype/%@",BaseURL,[_arrSelectedJob valueForKeyPath:@"jobtype_id"]]]];
    NSLog(@"getsubjobtype:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 JobSubTypeArr=[[NSMutableArray alloc]init];
                 JobSubTypeArr=[result valueForKeyPath:@"JobsubtypeDetails"];
                 NSLog(@"Job Subtypes Details%@",JobSubTypeArr);
                 [hud hide:YES];
                 [self getJobSubTypeCharacteristics];
                 
             }
             else
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
         }
     }];
}

-(void)getJobSubTypeCharacteristics
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Loading Characteristics...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getjobsubtypechar/%@",BaseURL,[JobSubTypeArr[currentSubTypeIndex] valueForKeyPath:@"jobsubtype_id"]]]];
    NSLog(@"getjobsubtypechar:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             [hud hide:YES];
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 NSMutableArray * arr=[[NSMutableArray alloc]init];
                 arrCharacteristics=[[NSMutableArray alloc]init];
                 [arr addObject:[result valueForKeyPath:@"JobsubtypecharDetails"]];
                 [arrCharacteristics addObject:@"Add Media"];
                 [arrCharacteristics addObject:@"Add SubCategory"];
                 [arrCharacteristics addObject:@"Add Address"];
                 for (int i=0; i<[[arr objectAtIndex:0] count]; i++)
                 {
                     [arrCharacteristics addObject:[[arr objectAtIndex:0] objectAtIndex:i]];
                 }
                 //                 [arrCharacteristics addObject:@"Add Desc"];
                 NSLog(@"arrCharacteristics:%@",arrCharacteristics);
                 NSLog(@"arrCharacteristics Count:%lu",(unsigned long)arrCharacteristics.count);
                 [hud hide:YES];
                 
                 [self ViewCreation];
                 [self assignValuestoParticularQuestions];
                 
                 if ([dictMedia count]!=0)
                 {
                     [self reloadtheCollectionView];
                 }
                 
                 
             }
             else
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
         }
     }];
}

-(void)getCityDetails
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@city",BaseURL]]];
    NSLog(@"city:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 cityArr=[[NSArray alloc]init];
                 cityArr=[result valueForKeyPath:@"CityDetails"];
                 
             }
             else
             {
                 
             }
         }
     }];
    
}
-(void)CatagoryBtnTouched:(UIButton *)sender
{
    NSLog(@"CatagoryBtnTouched");
    currentTappedIndex = sender.tag-401;
    NSLog(@"currentTappedIndex:%lu",(unsigned long)currentTappedIndex);
    arrTableDataList = [[NSMutableArray alloc]init];
    if (currentTappedIndex == 1)
    {
        _lblViewCatagory.text=@"Select best match for your need";
        arrTableDataList = [JobSubTypeArr mutableCopy];
        [self adjustPopview];
        [_tblCaragoryList reloadData];
        
    }
    if (currentTappedIndex == 2)
    {
        [self gotoAddress];
    }
    else if([[dictQuestTypeArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] isEqualToString:@"Multiple Option"]||[[dictQuestTypeArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] isEqualToString:@"Single Option"])
    {
        [self setKeyBoardNotificationCenters];
        arrTableDataList = [[dictValueArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] mutableCopy];
        NSLog(@"arrTableDataList:%@",arrTableDataList);
        _lblViewCatagory.text=[dictQuestArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
        [self adjustPopview];
        [_tblCaragoryList reloadData];
    }
    else if ([[dictQuestTypeArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] isEqualToString:@"Text"])
    {
        
        for (UIView *view in [_scrlView subviews])
        {
            if ((view.tag-101) == currentTappedIndex)
            {
                UILabel *lbl=(UILabel *)[view viewWithTag:201+currentTappedIndex];
                
                OostaJobDescriptionViewController *descObj=[[OostaJobDescriptionViewController alloc]initWithNibName:IS_IPAD ?@"OostaJobDescriptionViewController IPad":@"OostaJobDescriptionViewController" bundle:nil];
                descObj.delegate=self;
                descObj.currentValue=lbl.text;
                descObj.currentTappedIndex=[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex];
                descObj.arrContent=arrCharacteristics;
                [self.navigationController pushViewController:descObj animated:YES];
            }
        }
    }
    /*else if (currentTappedIndex==arrCharacteristics.count-1)
     {
     for (UIView *view in [_scrlView subviews])
     {
     if ((view.tag-101) == currentTappedIndex)
     {
     UILabel *lbl=(UILabel *)[view viewWithTag:201+currentTappedIndex];
     
     OostaJobDescriptionViewController *descObj=[[OostaJobDescriptionViewController alloc]initWithNibName:IS_IPAD ?@"OostaJobDescriptionViewController IPad":@"OostaJobDescriptionViewController" bundle:nil];
     descObj.delegate=self;
     descObj.currentValue=lbl.text;
     descObj.currentTappedIndex=[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex];
     descObj.arrContent=arrCharacteristics;
     [self.navigationController pushViewController:descObj animated:YES];
     }
     }
     }*/
    else if (currentTappedIndex==0)
    {
        
        [self openGalleryorCamera];
        
    }
    
    
}
-(void)openGalleryorCamera
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"Photo Album", nil), nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Camera", nil)];
    }
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    [actionSheet showFromToolbar:self.navigationController.toolbar];
}

-(IBAction)BackBtnTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)scrolltoParticularView:(UIView *)view completion:(void (^)(BOOL success))completionBlock
{
    
    CGPoint point = CGPointMake(0, view.frame.origin.y);
    [self.scrlView setContentOffset:point animated:NO];
    if (completionBlock!=nil) {
        completionBlock(true);
    }
}
-(IBAction)PostJobBtnTouched:(id)sender
{
    NSLog(@"selected:%@",dictSelected);
    NSMutableArray *allKey=[[dictSelected allKeys] mutableCopy];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:allKey];
    [sortedArray sortUsingSelector:@selector(localizedStandardCompare:)];
    allKey = [[NSMutableArray alloc]init];
    allKey = [sortedArray mutableCopy];
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for (int i=0; i<arrCharacteristics.count; i++)
    {
        if (![allKey containsObject:[NSString stringWithFormat:@"%d",i]])
        {
            [arr addObject:[NSString stringWithFormat:@"%d",i]];
        }
    }
    NSLog(@"arr:%@",arr);
    if (arr.count==0)
    {
        NSLog(@"Post");
        [self PostJob];
    }
    else
    {
        anim = [ CAKeyframeAnimation animationWithKeyPath:@"transform" ] ;
        anim.values = [ NSArray arrayWithObjects:
                       [ NSValue valueWithCATransform3D:CATransform3DMakeTranslation(-5.0f, 0.0f, 0.0f) ],
                       [ NSValue valueWithCATransform3D:CATransform3DMakeTranslation(5.0f, 0.0f, 0.0f) ],
                       nil ] ;
        anim.autoreverses = YES ;
        anim.repeatCount = 2.0f ;
        anim.duration = 0.07f ;
        
        for (int i=(arr.count-1); i>=0; i--)
        {
            NSLog(@"[arr objectAtIndex:i]:%@",[arr objectAtIndex:i]);
            for (UIView *view in [_scrlView subviews])
            {
                if ((view.tag-101)==[[arr objectAtIndex:i] intValue])
                {
                    [self scrolltoParticularView:view completion:^(BOOL success) {
                        if (success==YES)
                        {
                            [ view.layer addAnimation:anim forKey:nil ] ;
                        }
                    }];
                    break;
                }
            }
        }
    }
}
-(IBAction)HomeBtnTouched:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
-(IBAction)WhatNextBtnTouched:(id)sender
{
    [popupViewGreat dismissPresentingPopup];
    /*SWRevealViewController *revealController = self.revealViewController;
     UIViewController *newFrontController = nil;
     if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
     {
     OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController IPad" bundle:nil];
     newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
     }
     else if (IS_IPHONE5)
     {
     OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
     newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
     }
     else
     {
     OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
     newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
     }
     [revealController pushFrontViewController:newFrontController animated:YES];*/
    
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];
    OostaJobCustomerJobsViewController *CustomerfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomerfrontViewController = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomerfrontViewController = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
    }
    else
    {
        CustomerfrontViewController = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
    }
    
    CustomerRearViewController *CustomrrearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController" bundle:nil];
    }
    else
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomerfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomrrearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    UINavigationController *viewControllerNavigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    
    self.window.rootViewController = viewControllerNavigationController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}

- (IBAction)CloseBtnTouched:(id)sender
{
    [self.viewPopupContent removeFromSuperview];
    [self.tblCaragoryList endEditing:YES];
    [self clear];
}


#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrTableDataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        UITextField *txtField = IS_IPAD?[[UITextField alloc] initWithFrame:CGRectMake(110, 0, 500, 88)]:[[UITextField alloc] initWithFrame:CGRectMake(70, 0, 200, 44)];
        txtField.placeholder = @"Enter Details";
        txtField.font=[UIFont fontWithName:FONT_BOLD size:IS_IPAD?22.0:16.0];
        txtField.textAlignment = NSTextAlignmentLeft;
        txtField.autocorrectionType = UITextAutocorrectionTypeNo;
        [txtField setDelegate:self];
        txtField.tag=100;
        txtField.userInteractionEnabled=NO;
        [cell.contentView addSubview:txtField];
        txtField.hidden=YES;
    }
    
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if (currentTappedIndex==1)
    {
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[[arrTableDataList objectAtIndex:indexPath.row] valueForKeyPath:@"subtype_name"]];
        UITextField *txtField = (UITextField*)[cell viewWithTag:100];
        txtField.hidden=YES;
        txtField.userInteractionEnabled=NO;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        NSLog(@"Cato");
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[arrTableDataList objectAtIndex:indexPath.row]];
        UITextField *txtField = (UITextField*)[cell viewWithTag:100];
        if ([[NSString stringWithFormat:@"%@",[arrTableDataList objectAtIndex:indexPath.row]] rangeOfString:@"Other"].length!=0)
        {
            txtField.hidden=NO;
            txtField.text=[dictSelectedValueArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
            
        }
        else
        {
            txtField.hidden=YES;
            txtField.userInteractionEnabled=NO;
        }
        
        if ([[dictQuestTypeArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] isEqualToString:@"Multiple Option"])
        {
            if ([[dictSelectedIndexArr valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] containsObject:indexPath])
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                
            }
        }
        else if ([[dictQuestTypeArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] isEqualToString:@"Single Option"])
        {
            if ([[dictSelectedIndexArr valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] containsObject:indexPath])
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                
            }
        }
        
    }
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        cell.textLabel.font = [UIFont fontWithName:FONT_THIN size:23.0f];
    }
    else
    {
        cell.textLabel.font = [UIFont fontWithName:FONT_THIN size:16.0f];
    }
    
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (currentTappedIndex==1)
    {
        for (UIView *view in [_scrlView subviews])
        {
            if ((view.tag-101) == currentTappedIndex)
            {
                UILabel *lbl=(UILabel *)[view viewWithTag:201+currentTappedIndex];
                lbl.text=[NSString stringWithFormat:@"%@",[[arrTableDataList objectAtIndex:indexPath.row] valueForKeyPath:@"subtype_name"]];
                currentSubTypeIndex=indexPath.row;
                
                [self clearAllViews];
                [self getJobSubTypeCharacteristics];
                
                [self.viewPopupContent removeFromSuperview];
                
            }
        }
    }
    else if([[dictQuestTypeArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] isEqualToString:@"Multiple Option"]||[[dictQuestTypeArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] isEqualToString:@"Single Option"])
    {
        
        if ([[NSString stringWithFormat:@"%@",[arrTableDataList objectAtIndex:indexPath.row]] rangeOfString:@"Other"].length!=0)
        {
            
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            UITextField *txtField = (UITextField*)[cell viewWithTag:100];
            txtField.userInteractionEnabled=YES;
            [txtField becomeFirstResponder];
            currentIndexPath=indexPath;
            
        }
        else
        {
            
            if ([[dictQuestTypeArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] isEqualToString:@"Multiple Option"])
            {
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                NSMutableArray *arrSelected = [[NSMutableArray alloc]init];
                arrSelected = [[dictSelectedIndexArr valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] mutableCopy];
                NSLog(@"arrSelected:%@",arrSelected);
                if ([arrSelected containsObject:indexPath])
                {
                    [arrSelected removeObject:indexPath];
                }
                else
                {
                    [arrSelected addObject:indexPath];
                }
                [dictSelectedIndexArr setObject:arrSelected forKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
                [tableView reloadData];
                
            }
            else if ([[dictQuestTypeArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] isEqualToString:@"Single Option"])
            {
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                NSMutableArray *arrSelected = [[NSMutableArray alloc]init];
                [arrSelected addObject:indexPath];
                [dictSelectedIndexArr setObject:arrSelected forKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
                [tableView reloadData];
                [self.viewPopupContent removeFromSuperview];
            }
            
            
            [self setTappedValues];
        }
        
    }
}
-(void)setTappedValues
{
    NSMutableArray *arrSelected = [[NSMutableArray alloc]init];
    arrSelected = [[dictSelectedIndexArr valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] mutableCopy];
    
    if (arrSelected.count!=0)
    {
        [arrSelected sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSInteger r1 = [obj1 row];
            NSInteger r2 = [obj2 row];
            if (r1 > r2) {
                return (NSComparisonResult)NSOrderedDescending;
            }
            if (r1 < r2) {
                return (NSComparisonResult)NSOrderedAscending;
            }
            return (NSComparisonResult)NSOrderedSame;
        }];
        
        NSMutableArray *arrVal=[[NSMutableArray alloc]init];
        
        
        for (NSIndexPath *indexPath in arrSelected)
        {
            
            NSString *str=[[dictValueArr valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] objectAtIndex:indexPath.row];
            
            if (([[NSString stringWithFormat:@"%@",str] rangeOfString:@"Other"].length!=0))
            {
                str = [NSString stringWithFormat:@"%@: %@",str,[dictSelectedValueArr valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]]];
            }
            NSString *trimmedString = [str stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceCharacterSet]];
            [arrVal addObject:trimmedString];
            
        }
        NSLog(@"arrVal:%@",arrVal);
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
        NSString *str =[NSString stringWithFormat:@"%@",arrVal];
        str = [[str componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
        NSString *trimmedString = [str stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        trimmedString = [trimmedString stringByReplacingOccurrencesOfString:@",    " withString:@", "];
        
        for (UIView *view in [_scrlView subviews])
        {
            if ((view.tag-101) == currentTappedIndex)
            {
                UILabel *lbl=(UILabel *)[view viewWithTag:201+currentTappedIndex];
                UIImageView *img=(UIImageView *)[view viewWithTag:301+currentTappedIndex];
                img.image=[UIImage imageNamed:@"tick.png"];
                lbl.text=trimmedString;
                [self setSelectedValueinaDictionary];
                
            }
        }
        
        
    }
    else
    {
        for (UIView *view in [_scrlView subviews])
        {
            if ((view.tag-101) == currentTappedIndex)
            {
                UILabel *lbl=(UILabel *)[view viewWithTag:201+currentTappedIndex];
                UIImageView *img=(UIImageView *)[view viewWithTag:301+currentTappedIndex];
                img.image=[UIImage imageNamed:@"add.png"];
                lbl.text=[dictQuestArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
                [self removeSelectedValueinaDictionary];
            }
        }
    }
    
    
}
-(void)clearAllViews
{
    for (UIView *view in [_scrlView subviews])
    {
        [view removeFromSuperview];
    }
}
-(void)assignValuestoParticularQuestions
{
    if (!isComeFromSkipLogin)
    {
        dictSelectedIndexArr = [[NSMutableDictionary alloc]init];
        dictSelectedValueArr = [[NSMutableDictionary alloc]init];
    }
    
    
    dictValueArr = [[NSMutableDictionary alloc]init];
    dictQuestArr = [[NSMutableDictionary alloc]init];
    dictQuestTypeArr = [[NSMutableDictionary alloc]init];
    dictTagArr = [[NSMutableDictionary alloc]init];
    dictTagKeyArr  = [[NSMutableDictionary alloc]init];
    
    
    for (int i=3 ; i<arrCharacteristics.count; i++)
    {
        
        [dictQuestArr setObject:[[arrCharacteristics objectAtIndex:i] valueForKeyPath:@"char_name"] forKey:[NSString stringWithFormat:@"%d",i]];
        [dictQuestTypeArr setObject:[[arrCharacteristics objectAtIndex:i] valueForKeyPath:@"question_type"] forKey:[NSString stringWithFormat:@"%d",i]];
        [dictTagArr setObject:[[arrCharacteristics objectAtIndex:i] valueForKeyPath:@"tag_status"] forKey:[NSString stringWithFormat:@"%d",i]];
        [dictTagKeyArr setObject:[[arrCharacteristics objectAtIndex:i] valueForKeyPath:@"tag_keyword"] forKey:[NSString stringWithFormat:@"%d",i]];
        [dictValueArr setObject:[[arrCharacteristics objectAtIndex:i] valueForKeyPath:@"char_value.option"] forKey:[NSString stringWithFormat:@"%d",i]];
        if (!isComeFromSkipLogin)
        {
            [dictSelectedIndexArr setObject:@[] forKey:[NSString stringWithFormat:@"%d",i]];
            
        }
    }
    if (isComeFromSkipLogin)
    {
        for (int i=0; i<[dictSelected count]; i++)
        {
            if ([dictSelected valueForKeyPath:[NSString stringWithFormat:@"%d",i+1]])
            {
                for (UIView *view in [_scrlView subviews])
                {
                    if ((view.tag-101) == i+1)
                    {
                        UILabel *lbl=(UILabel *)[view viewWithTag:201+i+1];
                        UIImageView *img=(UIImageView *)[view viewWithTag:301+i+1];
                        img.image=[UIImage imageNamed:@"tick.png"];
                        lbl.text= [dictSelected valueForKeyPath:[NSString stringWithFormat:@"%d",i+1]];
                    }
                }
            }
            
        }
        
    }
    else
    {
        dictSelected = [[NSMutableDictionary alloc]init];
        [self setSelectedValueinaDictionary];
    }
    
    
    
    
    NSLog(@"dictValueArr:%@",dictValueArr);
    NSLog(@"dictQuestArr:%@",dictQuestArr);
    NSLog(@"dictQuestTypeArr:%@",dictQuestTypeArr);
    NSLog(@"dictTagArr:%@",dictTagArr);
    NSLog(@"dictTagKeyArr:%@",dictTagKeyArr);
    NSLog(@"dictSelectedIndexArr:%@",dictSelectedIndexArr);
}

-(void)setSelectedValueinaDictionary
{
    for (UIView *view in [_scrlView subviews])
    {
        if ((view.tag-101) == currentTappedIndex)
        {
            UILabel *lbl=(UILabel *)[view viewWithTag:201+currentTappedIndex];
            [dictSelected setObject:lbl.text forKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
        }
    }
}
-(void)removeSelectedValueinaDictionary
{
    [dictSelected removeObjectForKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
}
-(void)adjustPopview
{
    if (arrTableDataList.count<6)
    {
        [self setTableViewheightOfTable:_tblCaragoryList ByArrayName:arrTableDataList];
    }
    else
    {
        if (IS_IPAD)
        {
            _tblCaragoryList.frame=CGRectMake(0, 69, 700, 450);
        }
        else
        {
            _tblCaragoryList.frame=CGRectMake(0, 49, 300, 250);
        }
        
    }
    
    CGRect buttonFrame=_btnClose.frame;
    
    if (IS_IPAD)
    {
        buttonFrame.origin.y=_tblCaragoryList.frame.origin.y+_tblCaragoryList.frame.size.height+20;
    }
    else
    {
        buttonFrame.origin.y=_tblCaragoryList.frame.origin.y+_tblCaragoryList.frame.size.height+30;
    }
    
    _btnClose.frame = buttonFrame;
    
    CGRect popUpFrame = _PopupView.frame;
    popUpFrame.size.height = _btnClose.frame.origin.y+_btnClose.frame.size.height+5;
    _PopupView.frame = popUpFrame;
    
    
    self.PopupView.center = self.PopupView.superview.center;
    self.viewPopupContent.frame=self.view.bounds;
    [self.view addSubview:self.viewPopupContent];
    [self.viewPopupContent bringSubviewToFront:self.view];
    [self.PopupView bringSubviewToFront:self.viewPopupContent];
    
    
}
-(void)setTableViewheightOfTable :(UITableView *)tableView ByArrayName:(NSArray *)array
{
    
    CGFloat height =IS_IPAD?88:44;
    NSLog(@"height:%f",height);
    height *= array.count;
    
    CGRect tableFrame = tableView.frame;
    tableFrame.size.height = height;
    tableView.frame = tableFrame;
    
}
#pragma mark KeyBoard adjust Popup
-(void) setKeyBoardNotificationCenters
{
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillShow:)
                                                 name: UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardDidShow:)
                                                 name: UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillDisappear:)
                                                 name: UIKeyboardWillHideNotification object:nil];
}
- (void) keyboardWillShow: (NSNotification*) aNotification
{
    [UIView animateWithDuration: [self keyboardAnimationDurationForNotification: aNotification] animations:^{
        
    } completion:^(BOOL finished) {
    }];
}

- (void) keyboardDidShow: (NSNotification*) aNotification
{
    [UIView animateWithDuration: [self keyboardAnimationDurationForNotification: aNotification] animations:^{
        
        //change the frame of your talbleiview via kbsize.height
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+15, 0.0);
        self.scrlPopupContents.contentInset = contentInsets;
        self.scrlPopupContents.scrollIndicatorInsets = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        NSLog(@"_btnClose.bounds.origin:%f,%f",_btnClose.frame.origin.x,_btnClose.frame.origin.y);
        if (CGRectContainsPoint(aRect, _btnClose.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0, (_btnClose.frame.origin.y+_btnClose.frame.size.height)-kbSize.height);
            [self.scrlPopupContents setContentOffset:scrollPoint animated:YES];
        }
        
        
    } completion:^(BOOL finished) {
        CGSize r = self.tblCaragoryList.contentSize;
        [self.tblCaragoryList scrollRectToVisible:CGRectMake(0, r.height-10, r.width, 10) animated:YES];
    }];
    
}
- (void) keyboardWillDisappear: (NSNotification*) aNotification
{
    NSLog(@"keyboardWillDisappear");
    [UIView animateWithDuration: [self keyboardAnimationDurationForNotification: aNotification] animations:^{
        [self adjustPopview];
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        self.scrlPopupContents.contentInset = contentInsets;
        self.scrlPopupContents.scrollIndicatorInsets = contentInsets;
    } completion:^(BOOL finished) {
        UITableViewCell *cell = [_tblCaragoryList cellForRowAtIndexPath:currentIndexPath];
        UITextField *txtField = (UITextField*)[cell viewWithTag:100];
        NSLog(@"txtField:%@",txtField.text);
        if ([[dictQuestTypeArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] isEqualToString:@"Multiple Option"])
        {
            if (txtField.text.length>0)
            {
                [_tblCaragoryList deselectRowAtIndexPath:currentIndexPath animated:YES];
                NSMutableArray *arrSelected = [[NSMutableArray alloc]init];
                arrSelected = [[dictSelectedIndexArr valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] mutableCopy];
                NSLog(@"arrSelected:%@",arrSelected);
                if (![arrSelected containsObject:currentIndexPath])
                {
                    [arrSelected addObject:currentIndexPath];
                }
                
                [dictSelectedIndexArr setObject:arrSelected forKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
                [_tblCaragoryList reloadData];
            }
            else
            {
                [_tblCaragoryList deselectRowAtIndexPath:currentIndexPath animated:YES];
                NSMutableArray *arrSelected = [[NSMutableArray alloc]init];
                arrSelected = [[dictSelectedIndexArr valueForKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] mutableCopy];
                NSLog(@"arrSelected:%@",arrSelected);
                if ([arrSelected containsObject:currentIndexPath])
                {
                    [arrSelected removeObject:currentIndexPath];
                }
                
                [dictSelectedIndexArr setObject:arrSelected forKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
                [_tblCaragoryList reloadData];
            }
            
            txtField.userInteractionEnabled=NO;
            [dictSelectedValueArr setObject:txtField.text forKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
        }
        else if ([[dictQuestTypeArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]] isEqualToString:@"Single Option"])
        {
            if (txtField.text.length>0)
            {
                [_tblCaragoryList deselectRowAtIndexPath:currentIndexPath animated:YES];
                NSMutableArray *arrSelected = [[NSMutableArray alloc]init];
                [arrSelected addObject:currentIndexPath];
                [dictSelectedIndexArr setObject:arrSelected forKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
                [_tblCaragoryList reloadData];
                [self.viewPopupContent removeFromSuperview];
                txtField.userInteractionEnabled=NO;
                [dictSelectedValueArr setObject:txtField.text forKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
            }
        }
        
        [self setTappedValues];
    }];
}
- (NSTimeInterval) keyboardAnimationDurationForNotification:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    NSValue* value = [info objectForKey: UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval duration = 0;
    [value getValue: &duration];
    
    return duration;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)getDesc:(NSString *)description andthecurrentTappedIndex:(NSString *)currentTapIndex
{
    currentTappedIndex=[currentTapIndex integerValue];
    NSLog(@"description:%@",description);
    if (description.length!=0)
    {
        for (UIView *view in [_scrlView subviews])
        {
            if ((view.tag-101) == currentTappedIndex)
            {
                UILabel *lbl=(UILabel *)[view viewWithTag:201+currentTappedIndex];
                UIImageView *img=(UIImageView *)[view viewWithTag:301+currentTappedIndex];
                img.image=[UIImage imageNamed:@"tick.png"];
                lbl.text=description;
                
                [self setSelectedValueinaDictionary];
                
            }
        }
    }
    /*else if(currentTappedIndex == arrCharacteristics.count-1)
     {
     for (UIView *view in [_scrlView subviews])
     {
     if ((view.tag-101) == currentTappedIndex)
     {
     UILabel *lbl=(UILabel *)[view viewWithTag:201+currentTappedIndex];
     UIImageView *img=(UIImageView *)[view viewWithTag:301+currentTappedIndex];
     img.image=[UIImage imageNamed:@"add.png"];
     lbl.text=@"Write down some notes about the job description, specifiation, size, etc.";
     [self removeSelectedValueinaDictionary];
     }
     }
     
     }*/
    else
    {
        for (UIView *view in [_scrlView subviews])
        {
            if ((view.tag-101) == currentTappedIndex)
            {
                UILabel *lbl=(UILabel *)[view viewWithTag:201+currentTappedIndex];
                UIImageView *img=(UIImageView *)[view viewWithTag:301+currentTappedIndex];
                img.image=[UIImage imageNamed:@"add.png"];
                [self removeSelectedValueinaDictionary];
            }
        }
    }
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    
    [self clear];
}
-(void)clear
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

#pragma mark - Address
- (void)gotoAddress
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        LocationPickerViewController *locObj=[[LocationPickerViewController alloc]initWithNibName:@"LocationPickerViewController IPad" bundle:nil];
        locObj.delegate=self;
        locObj.strLat=strLatitude;
        locObj.strLong=strLongitude;
        locObj.strAddress=strAddress;
        [self.navigationController pushViewController:locObj animated:YES];
    }
    else
    {
        LocationPickerViewController *locObj=[[LocationPickerViewController alloc]initWithNibName:@"LocationPickerViewController" bundle:nil];
        locObj.delegate=self;
        locObj.strLat=strLatitude;
        locObj.strLong=strLongitude;
        locObj.strAddress=strAddress;
        [self.navigationController pushViewController:locObj animated:YES];
    }
    
}


- (void)getAddress:(NSString*)address andtheLat:(NSString *)latitude andtheLong:(NSString *)longitude andtheZipcode:(NSString *)zipcode
{
    NSLog(@"address:%@",address);
    
    
    strLongitude=longitude;
    strLatitude=latitude;
    strZipcode=zipcode;
    strAddress=address;
    
    if ([[cityArr valueForKeyPath:@"zipcode"] containsObject:strZipcode])
    {
        if (address.length!=0)
        {
            for (UIView *view in [_scrlView subviews])
            {
                if ((view.tag-101) == currentTappedIndex)
                {
                    UILabel *lbl=(UILabel *)[view viewWithTag:201+currentTappedIndex];
                    UIImageView *img=(UIImageView *)[view viewWithTag:301+currentTappedIndex];
                    img.image=[UIImage imageNamed:@"tick.png"];
                    lbl.text=address;
                    [self setSelectedValueinaDictionary];
                }
            }
        }
        else
        {
            for (UIView *view in [_scrlView subviews])
            {
                if ((view.tag-101) == currentTappedIndex)
                {
                    UILabel *lbl=(UILabel *)[view viewWithTag:201+currentTappedIndex];
                    UIImageView *img=(UIImageView *)[view viewWithTag:301+currentTappedIndex];
                    img.image=[UIImage imageNamed:@"add.png"];
                    lbl.text=[dictQuestArr valueForKeyPath:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
                    [self removeSelectedValueinaDictionary];
                    
                }
            }
        }
    }
    else
    {
        
        [self GotoAddressNotify];
    }
    
    
}

#pragma mark Camera
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Photo Album", nil)])
    {
        [self openPhotoAlbum];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Camera", nil)])
    {
        [self showCamera];
    }
}
- (void)showCamera
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie,(NSString *) kUTTypeImage, nil];
        [picker setVideoMaximumDuration:120.0f];
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    
}

- (void)openPhotoAlbum
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie,(NSString *) kUTTypeImage, nil];
        [controller setVideoMaximumDuration:120.0f];
        [self presentViewController:controller animated:YES completion:NULL];
    }];
    
    
}
- (ALAssetsLibrary *)assetsLibrary
{
    if (assetsLibrary_) {
        return assetsLibrary_;
    }
    assetsLibrary_ = [[ALAssetsLibrary alloc] init];
    return assetsLibrary_;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    mediaCount = mediaCount+1;
    // Manage the media (photo)
    if (dictMedia.count<25)
    {
        NSString * mediaType = info[UIImagePickerControllerMediaType];
        
        if([mediaType isEqualToString:(__bridge NSString *)kUTTypeImage])
        {
            NSLog(@"Image");
            
            int totalImage=0;
            for (int i=0; i<dictMedia.count; i++)
            {
                if([[[dictMedia allKeys] objectAtIndex:i] rangeOfString:@"Image"].length!=0)
                {
                    totalImage=totalImage+1;
                }
            }
            if (totalImage<20)
            {
                // Manage tasks in background thread
                if (picker.sourceType==UIImagePickerControllerSourceTypeCamera)
                {
                    
                    UIImage * editedImage = (UIImage *)info[UIImagePickerControllerEditedImage];
                    UIImage * imageToSave = (editedImage ?: (UIImage *)info[UIImagePickerControllerOriginalImage]);
                    
                    UIImage * finalImageToSave = nil;
                    /* Modify image's size before save it to photos album
                     *
                     *  CGSize sizeToSave = CGSizeMake(imageToSave.size.width, imageToSave.size.height);
                     *  UIGraphicsBeginImageContextWithOptions(sizeToSave, NO, 0.f);
                     *  [imageToSave drawInRect:CGRectMake(0.f, 0.f, sizeToSave.width, sizeToSave.height)];
                     *  finalImageToSave = UIGraphicsGetImageFromCurrentImageContext();
                     *  UIGraphicsEndImageContext();
                     */
                    finalImageToSave = imageToSave;
                    
                    [self logFile:[NSString stringWithFormat:@"Image Selected from camera -- %lu",(unsigned long)mediaCount]];
                    
                    dispatch_async(dispatch_get_main_queue(),
                                   ^{
                                       [dictMedia setObject:finalImageToSave forKey:[NSString stringWithFormat:@"Image%lu",(unsigned long)mediaCount]];
                                       [self reloadtheCollectionView];
                                   });
                    
                    
                    void (^completion)(NSURL *, NSError *) = ^(NSURL *assetURL, NSError *error) {
                        if (error) {
                            NSLog(@"%s: Write the image data to the assets library (camera roll): %@",
                                  __PRETTY_FUNCTION__, [error localizedDescription]);
                        }
                        
                        NSLog(@"%s: Save image with asset url %@ (absolute path: %@), type: %@", __PRETTY_FUNCTION__,
                              assetURL, [assetURL absoluteString], [assetURL class]);
                        
                        
                        [self logFile:[NSString stringWithFormat:@"%s: Save image with asset url %@ (absolute path: %@), type: %@", __PRETTY_FUNCTION__,assetURL, [assetURL absoluteString], [assetURL class]]];
                        
                        
                    };
                    
                    void (^failure)(NSError *) = ^(NSError *error) {
                        if (error) NSLog(@"%s: Failed to add the asset to the custom photo album: %@",
                                         __PRETTY_FUNCTION__, [error localizedDescription]);
                        
                        [self logFile:[NSString stringWithFormat:@"%s: Failed to add the asset to the custom photo album: %@",
                                       __PRETTY_FUNCTION__, [error localizedDescription]]];
                    };
                    
                    
                    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
                    
                    if (status == PHAuthorizationStatusAuthorized) {
                        [self.assetsLibrary saveImage:finalImageToSave
                                              toAlbum:@"OOstaJob"
                                           completion:completion
                                              failure:failure];
                    }
                    else if (status == PHAuthorizationStatusDenied) {
                        //denied.
                    }
                    else if (status == PHAuthorizationStatusNotDetermined) {
                        //determined.
                        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                            
                            if (status == PHAuthorizationStatusAuthorized) {
                                
                                [self.assetsLibrary saveImage:finalImageToSave
                                                      toAlbum:@"OOstaJob"
                                                   completion:completion
                                                      failure:failure];
                            }
                            else {
                                //denied.
                            }
                        }];
                    }
                    else if (status == PHAuthorizationStatusRestricted) {
                        //Restricted access
                    }
                }
                else
                {
                    [self logFile:[NSString stringWithFormat:@"Image Selected from Gallery -- %lu",(unsigned long)mediaCount]];
                    
                    UIImage * editedImage = (UIImage *)info[UIImagePickerControllerEditedImage];
                    UIImage * imageToSave = (editedImage ?: (UIImage *)info[UIImagePickerControllerOriginalImage]);
                    [dictMedia setObject:imageToSave forKey:[NSString stringWithFormat:@"Image%lu",(unsigned long)mediaCount]];
                    [self reloadtheCollectionView];
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                                message:@"Image limit is 20"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
        else
        {
            [self logFile:[NSString stringWithFormat:@"Video Selected from camera -- %lu",(unsigned long)mediaCount]];
            
            int totalVideo=0;
            for (int i=0; i<dictMedia.count; i++)
            {
                if([[[dictMedia allKeys] objectAtIndex:i] rangeOfString:@"Video"].length!=0)
                {
                    totalVideo=totalVideo+1;
                }
            }
            if (totalVideo<5)
            {
                if (picker.sourceType==UIImagePickerControllerSourceTypeCamera)
                {
                    
                    NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
                    
                    dispatch_async(dispatch_get_main_queue(),
                                   ^{
                                       [dictMedia setObject:videoUrl forKey:[NSString stringWithFormat:@"Video%lu",(unsigned long)mediaCount]];
                                       [self reloadtheCollectionView];
                                   });
                    
                    
                    NSString *albumName=@"OOstaJob";
                    [self.assetsLibrary saveVideo:videoUrl toAlbum:albumName completion:^(NSURL *assetURL, NSError *error)
                     {
                         if (!error)
                         {
                         }
                         
                     } failure:^(NSError *error) {
                         NSLog(@"Failed");
                         
                     }];
                }
                else
                {
                    
                    [self logFile:[NSString stringWithFormat:@"Video Selected from gallery -- %lu",(unsigned long)mediaCount]];
                    
                    NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
                    AVURLAsset *avUrl = [AVURLAsset assetWithURL:videoUrl];
                    CMTime time = [avUrl duration];
                    int seconds = ceil(time.value/time.timescale);
                    
                    if (seconds<=120&&videoUrl!=nil)
                    {
                        [dictMedia setObject:videoUrl forKey:[NSString stringWithFormat:@"Video%lu",(unsigned long)mediaCount]];
                        [self reloadtheCollectionView];
                    }
                    else
                    {
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                                        message:@"Video length should less than 2.00 minute"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                        /**** Trim ****/
                        
                        
                        /* AVURLAsset *asset = [AVURLAsset URLAssetWithURL:videoUrl options:nil];
                         AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
                         
                         NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                         NSString *outputURL = paths[0];
                         NSFileManager *manager = [NSFileManager defaultManager];
                         [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
                         NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
                         [dateFormatter setDateFormat:@"dd-MM-YYYY"];
                         int random = arc4random() % 9999999;
                         NSString * randomName;
                         randomName=[NSString stringWithFormat:@"%@-%d-Video.MOV",[dateFormatter stringFromDate:[NSDate date]],random];
                         
                         outputURL = [outputURL stringByAppendingPathComponent:randomName];
                         // Remove Existing File
                         [manager removeItemAtPath:outputURL error:nil];
                         
                         
                         exportSession.outputURL = [NSURL fileURLWithPath:outputURL];
                         exportSession.shouldOptimizeForNetworkUse = YES;
                         exportSession.outputFileType = AVFileTypeQuickTimeMovie;
                         CMTime start = CMTimeMakeWithSeconds(1.0, 600); // you will modify time range here
                         CMTime duration = CMTimeMakeWithSeconds(120.0, 600);
                         CMTimeRange range = CMTimeRangeMake(start, duration);
                         exportSession.timeRange = range;
                         [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
                         {
                         switch (exportSession.status) {
                         case AVAssetExportSessionStatusCompleted:
                         [dictMedia setObject:[NSURL fileURLWithPath:outputURL] forKey:[NSString stringWithFormat:@"Video%lu",(unsigned long)mediaCount]];
                         [self reloadtheCollectionView];
                         NSLog(@"Export Complete %ld %@", (long)exportSession.status, exportSession.error);
                         break;
                         case AVAssetExportSessionStatusFailed:
                         NSLog(@"Failed:%@",exportSession.error);
                         break;
                         case AVAssetExportSessionStatusCancelled:
                         NSLog(@"Canceled:%@",exportSession.error);
                         break;
                         default:
                         break;
                         }
                         
                         //[exportSession release];
                         }];*/
                    }
                    
                    
                }
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                                message:@"Video limit is 5"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                        message:@"Maximum limit reached"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}


-(void)cropVideo:(NSURL*)videoToTrimURL
{
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:videoToTrimURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *outputURL = paths[0];
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager createDirectoryAtPath:outputURL withIntermediateDirectories:YES attributes:nil error:nil];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd-MM-YYYY"];
    int random = arc4random() % 9999999;
    NSString * randomName;
    randomName=[NSString stringWithFormat:@"%@-%d-Video.MOV",[dateFormatter stringFromDate:[NSDate date]],random];
    
    outputURL = [outputURL stringByAppendingPathComponent:randomName];
    // Remove Existing File
    [manager removeItemAtPath:outputURL error:nil];
    
    
    exportSession.outputURL = [NSURL fileURLWithPath:outputURL];
    exportSession.shouldOptimizeForNetworkUse = YES;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    CMTime start = CMTimeMakeWithSeconds(1.0, 600); // you will modify time range here
    CMTime duration = CMTimeMakeWithSeconds(120.0, 600);
    CMTimeRange range = CMTimeRangeMake(start, duration);
    exportSession.timeRange = range;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         switch (exportSession.status) {
             case AVAssetExportSessionStatusCompleted:
             {
                 [dictMedia setObject:[NSURL fileURLWithPath:outputURL] forKey:[NSString stringWithFormat:@"Video%lu",(unsigned long)mediaCount]];
                 dispatch_async(dispatch_get_main_queue(),
                                ^{ [self reloadtheCollectionView]; });
                 NSLog(@"Export Complete %ld %@", (long)exportSession.status, exportSession.error);
                 break;
             }
             case AVAssetExportSessionStatusFailed:
                 NSLog(@"Failed:%@",exportSession.error);
                 break;
             case AVAssetExportSessionStatusCancelled:
                 NSLog(@"Canceled:%@",exportSession.error);
                 break;
             default:
                 break;
         }
         
         //[exportSession release];
     }];
}


-(void)reloadtheCollectionView
{
    
    [self logFile:@"Collection view reloaded"];
    
    UICollectionView *collectionView;
    for (UIView *view in [_scrlView subviews])
    {
        if ((view.tag-101) == 0)
        {
            UILabel *lbl=(UILabel *)[view viewWithTag:201+0];
            collectionView=(UICollectionView *)[view viewWithTag:501];
            UIImageView *img=(UIImageView *)[view viewWithTag:301+0];
            UIButton *btn=(UIButton *)[view viewWithTag:401+0];
            btn.hidden=YES;
            img.image=[UIImage imageNamed:@"tick.png"];
            lbl.hidden=YES;
            collectionView.hidden = NO;
            [collectionView reloadData];
            [dictSelected setObject:@"Media" forKey:[NSString stringWithFormat:@"%lu",(unsigned long)0]];
            
            
        }
    }
    
    NSInteger section = [collectionView numberOfSections] - 1;
    NSInteger item = [collectionView numberOfItemsInSection:section] - 1;
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
    [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:(UICollectionViewScrollPositionLeft) animated:YES];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark collection view cell paddings

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return IS_IPAD?UIEdgeInsetsMake(10, 10, 0, 0):UIEdgeInsetsMake(5, 5, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return IS_IPAD?10.0:5.0;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    arrKeys = [[NSMutableArray alloc]init];
    arrKeys = [[dictMedia allKeys] mutableCopy];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:arrKeys];
    [sortedArray sortUsingSelector:@selector(localizedStandardCompare:)];
    arrKeys = [[NSMutableArray alloc]init];
    arrKeys = [sortedArray mutableCopy];
    NSLog(@"arrKeys:%@",arrKeys);
    if (arrKeys.count!=0)
    {
        [arrKeys addObject:@"Plus"];
    }
    
    return arrKeys.count;
}

- (OostaJobMediaCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OostaJobMediaCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    cell.imgThumbNail.contentMode = UIViewContentModeScaleAspectFill;
    cell.imgThumbNail.clipsToBounds = YES;
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgThumbNail];
    
    if ([[arrKeys objectAtIndex:indexPath.row] rangeOfString:@"Image"].length!=0)
    {
        cell.imgThumbNail.image=[dictMedia valueForKeyPath:[NSString stringWithFormat:@"%@",[arrKeys objectAtIndex:indexPath.row]]];
        cell.imgPlay.hidden=YES;
        cell.btnDelete.hidden=NO;
    }
    else if ([[arrKeys objectAtIndex:indexPath.row] rangeOfString:@"Video"].length!=0)
    {
        cell.imgPlay.hidden=NO;
        
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[dictMedia valueForKeyPath:[NSString stringWithFormat:@"%@",[arrKeys objectAtIndex:indexPath.row]]] options:nil];
        AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        generator.appliesPreferredTrackTransform = YES;
        NSError *error;
        CGImageRef imageRef = [generator copyCGImageAtTime:CMTimeMake(1, 2) actualTime:NULL error:&error];
        if (!error)
        {
            UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                cell.imgThumbNail.image=image;
                
            });
        }
        cell.btnDelete.hidden=NO;
    }
    else
    {
        cell.imgThumbNail.image = [UIImage imageNamed:@"plus_Build.png"];
        cell.imgPlay.hidden=YES;
        cell.btnDelete.hidden=YES;
    }
    cell.btnDelete.tag=indexPath.row;
    [cell.btnDelete addTarget:self action:@selector(deleteVideo:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnDidSelect.tag=indexPath.row;
    [cell.btnDidSelect addTarget:self action:@selector(didSelect:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
-(void)didSelect:(UIButton *)sender
{
    
    
    if ([[arrKeys objectAtIndex:sender.tag] rangeOfString:@"Image"].length!=0)
    {
        /* MXLMediaView *mediaView = [[MXLMediaView alloc] init];
         [mediaView setDelegate:self];
         
         [mediaView showImage:[dictMedia valueForKeyPath:[NSString stringWithFormat:@"%@",[arrKeys objectAtIndex:sender.tag]]] inParentView:self.view completion:^{
         NSLog(@"Done showing MXLMediaView");
         
         
         }];*/
        MXLMediaView *mediaView = [[MXLMediaView alloc] init];
        mediaView.dictMedia =  dictMedia;
        mediaView.strBidding = @"YES";
        mediaView.selectedIndex = sender.tag;
        [mediaView setDelegate:self];
        [mediaView addingMediaInScrollViewinParentView:self.view completion:^{
            NSLog(@"Done showing MXLMediaView");
        }];
    }
    else if ([[arrKeys objectAtIndex:sender.tag] rangeOfString:@"Video"].length!=0)
    {
        /* MXLMediaView *mediaView = [[MXLMediaView alloc] init];
         [mediaView setDelegate:self];
         
         // The best video on the Internet.
         NSURL *videoURL = [dictMedia valueForKeyPath:[NSString stringWithFormat:@"%@",[arrKeys objectAtIndex:sender.tag]]];
         
         [mediaView showVideoWithURL:videoURL inParentView:self.navigationController.view completion:^{
         NSLog(@"Complete");
         
         }];*/
        MXLMediaView *mediaView = [[MXLMediaView alloc] init];
        mediaView.dictMedia =  dictMedia;
        mediaView.strBidding = @"YES";
        mediaView.selectedIndex = sender.tag;
        [mediaView setDelegate:self];
        [mediaView addingMediaInScrollViewinParentView:self.view completion:^{
            NSLog(@"Done showing MXLMediaView");
        }];
    }
    else
    {
        currentTappedIndex = 0;
        [self openGalleryorCamera];
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    /*  if ([[arrKeys objectAtIndex:indexPath.row] rangeOfString:@"Image"].length!=0)
     {
     MXLMediaView *mediaView = [[MXLMediaView alloc] init];
     [mediaView setDelegate:self];
     
     [mediaView showImage:[dictMedia valueForKeyPath:[NSString stringWithFormat:@"%@",[arrKeys objectAtIndex:indexPath.row]]] inParentView:self.view completion:^{
     NSLog(@"Done showing MXLMediaView");
     }];
     }
     else if ([[arrKeys objectAtIndex:indexPath.row] rangeOfString:@"Video"].length!=0)
     {
     MXLMediaView *mediaView = [[MXLMediaView alloc] init];
     [mediaView setDelegate:self];
     
     // The best video on the Internet.
     NSURL *videoURL = [dictMedia valueForKeyPath:[NSString stringWithFormat:@"%@",[arrKeys objectAtIndex:indexPath.row]]];
     
     [mediaView showVideoWithURL:videoURL inParentView:self.navigationController.view completion:^{
     NSLog(@"Complete");
     
     }];
     }
     else
     {
     currentTappedIndex = 0;
     [self openGalleryorCamera];
     }*/
    
}


-(void)deleteVideo:(UIButton *)sender
{
    NSLog(@"tag:%ld",(long)sender.tag);
    currentTappedIndex = 0;
    [dictMedia removeObjectForKey:[arrKeys objectAtIndex:sender.tag]];
    for (UIView *view in [_scrlView subviews])
    {
        if ((view.tag-101) == currentTappedIndex)
        {
            UILabel *lbl=(UILabel *)[view viewWithTag:201+currentTappedIndex];
            UICollectionView *collectionView=(UICollectionView *)[view viewWithTag:501];
            if (dictMedia.count==0)
            {
                lbl.hidden=NO;
                collectionView.hidden = YES;
                UIImageView *img=(UIImageView *)[view viewWithTag:301+currentTappedIndex];
                img.image=[UIImage imageNamed:@"add.png"];
                
                UIButton *btn=(UIButton *)[view viewWithTag:401+currentTappedIndex];
                btn.hidden=NO;
                [dictSelected removeObjectForKey:[NSString stringWithFormat:@"%lu",(unsigned long)currentTappedIndex]];
            }
            else
            {
                lbl.hidden=YES;
                collectionView.hidden = NO;
                UIImageView *img=(UIImageView *)[view viewWithTag:301+currentTappedIndex];
                img.image=[UIImage imageNamed:@"tick.png"];
                UIButton *btn=(UIButton *)[view viewWithTag:401+currentTappedIndex];
                btn.hidden=YES;
            }
            
            [collectionView reloadData];
        }
    }
}

#pragma mark MXLMediaView delegate
-(void)mediaView:(MXLMediaView *)mediaView didReceiveLongPressGesture:(id)gesture {
    NSLog(@"MXLMediaViewDelgate: Long pressed received");
}

-(void)mediaViewWillDismiss:(MXLMediaView *)mediaView {
    _viewRegister.hidden=YES;
    NSLog(@"MXLMediaViewDelgate: Will dismiss");
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
}

-(void)mediaViewDidDismiss:(MXLMediaView *)mediaView {
    _viewRegister.hidden=YES;
    NSLog(@"MXLMediaViewDelgate: Did dismiss");
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

-(void)PostJob
{
    
    CGFloat scaleSize = 0.2f;
    UIImage *smallImage = [UIImage imageWithCGImage:choosenImage.CGImage
                                              scale:scaleSize
                                        orientation:choosenImage.imageOrientation];
    choosenImage=smallImage;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Posting Job...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    NSString *URLString =[NSString stringWithFormat:@"%@postjobimgupload",BaseURL];
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc]init];
    NSMutableArray *arrDictKey = [[NSMutableArray alloc]init];
    arrDictKey = [[dictMedia allKeys] mutableCopy];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:arrDictKey];
    [sortedArray sortUsingSelector:@selector(localizedStandardCompare:)];
    arrDictKey = [[NSMutableArray alloc]init];
    arrDictKey = [sortedArray mutableCopy];
    
    for (int i=0; i<arrDictKey.count; i++)
    {
        if ([[arrDictKey objectAtIndex:i] rangeOfString:@"Image"].length!=0)
        {
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"dd-MM-YYYY"];
            int random = arc4random() % 9999999;
            NSString * randomName;
            randomName=[NSString stringWithFormat:@"%@-%d-Photo.png",[dateFormatter stringFromDate:[NSDate date]],random];
            
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"( \" )"];
            randomName = [[randomName componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
            NSLog(@" Name %@",randomName);
            
            [parameters setObject:randomName forKey:[NSString stringWithFormat:@"postjob%d",i+1]];
            
            
        }
        else if ([[arrDictKey objectAtIndex:i] rangeOfString:@"Video"].length!=0)
        {
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"dd-MM-YYYY"];
            int random = arc4random() % 9999999;
            NSString * randomName;
            randomName=[NSString stringWithFormat:@"%@-%d-Video.mp4",[dateFormatter stringFromDate:[NSDate date]],random];
            
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"( \" )"];
            randomName = [[randomName componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
            NSLog(@" Name %@",randomName);
            
            [parameters setObject:randomName forKey:[NSString stringWithFormat:@"postjob%d",i+1]];
        }
    }
    
    NSLog(@"parameters:%@",parameters);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                                                          {
                                                                              for (int i=0; i<arrDictKey.count; i++)
                                                                              {
                                                                                  
                                                                                  if ([[arrDictKey objectAtIndex:i] rangeOfString:@"Image"].length!=0)
                                                                                  {
                                                                                      NSLog(@"fileName:%@",[parameters valueForKey:[NSString stringWithFormat:@"postjob%d",i+1]]);
                                                                                      [formData appendPartWithFileData:UIImagePNGRepresentation([UIImage compressImage:[dictMedia valueForKey:[NSString stringWithFormat:@"%@",[arrDictKey objectAtIndex:i]]]
                                                                                                                                                         compressRatio:0.7f]) name:[NSString stringWithFormat:@"postjob%d",i+1] fileName:[parameters valueForKey:[NSString stringWithFormat:@"postjob%d",i+1]] mimeType:@"image/png"];
                                                                                  }
                                                                                  else if ([[arrDictKey objectAtIndex:i] rangeOfString:@"Video"].length!=0)
                                                                                  {
                                                                                      NSURL *uploadURL = [NSURL fileURLWithPath:[[NSTemporaryDirectory() stringByAppendingPathComponent:[self createRandomName]] stringByAppendingString:@".mp4"]];
                                                                                      /*[self convertVideoToLowQuailtyWithInputURL:[dictMedia valueForKey:[NSString stringWithFormat:@"%@",[arrDictKey objectAtIndex:i]]] outputURL:uploadURL andCompletionHandler:^(bool result) {
                                                                                       if (result==YES)
                                                                                       {
                                                                                       NSData *dataVideo=[NSData dataWithContentsOfURL:[dictMedia valueForKey:[NSString stringWithFormat:@"%@",[arrDictKey objectAtIndex:i]]]];
                                                                                       [formData appendPartWithFileData:dataVideo name:[NSString stringWithFormat:@"postjob%d",i+1] fileName:[parameters valueForKey:[NSString stringWithFormat:@"postjob%d",i+1]] mimeType:@"video/mp4"];
                                                                                       }
                                                                                       }];*/
                                                                                      NSData *dataVideo=[NSData dataWithContentsOfURL:[dictMedia valueForKey:[NSString stringWithFormat:@"%@",[arrDictKey objectAtIndex:i]]]];
                                                                                      [formData appendPartWithFileData:dataVideo name:[NSString stringWithFormat:@"postjob%d",i+1] fileName:[parameters valueForKey:[NSString stringWithFormat:@"postjob%d",i+1]] mimeType:@"video/mp4"];
                                                                                      
                                                                                      
                                                                                      
                                                                                  }
                                                                                  
                                                                              }
                                                                          }
                                                                               success:^(AFHTTPRequestOperation *operation, id responseObject)
                                                                          {
                                                                              NSLog(@"Success %@", responseObject);
                                                                              [self callPostJobFortheContent:parameters andCompletionHandler:^(bool result) {
                                                                                  
                                                                                  if (result==YES)
                                                                                  {
                                                                                      [hud hide:YES];
                                                                                      
                                                                                  }
                                                                              }];
                                                                          }
                                                                               failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                                                          {
                                                                              [hud hide:YES];
                                                                              MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                                                                              
                                                                              hud.mode = MBProgressHUDModeText;
                                                                              hud.color=[UIColor whiteColor];
                                                                              hud.labelColor=kMBProgressHUDLabelColor;
                                                                              hud.backgroundColor=kMBProgressHUDBackgroundColor;
                                                                              
                                                                              hud.labelText = SERVER_ERR;
                                                                              hud.margin = 10.f;
                                                                              hud.yOffset = 20.f;
                                                                              hud.removeFromSuperViewOnHide = YES;
                                                                              [hud hide:YES afterDelay:2];
                                                                          }];
    
    
}

-(void)callPostJobFortheContent:(NSMutableDictionary *)dictParameters andCompletionHandler:(void (^)(bool result))completionHandler
{
    
    
    NSString * key1 =@"userID";
    NSString * obj1 =[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];
    
    NSString * key2 =@"jobtypeID";
    NSString * obj2 =[_arrSelectedJob valueForKeyPath:@"jobtype_id"];
    
    NSString * key3 =@"jobsubtypeID";
    NSString * obj3 =[JobSubTypeArr[currentSubTypeIndex] valueForKeyPath:@"jobsubtype_id"];
    
    NSString * key4 =@"Address";
    NSString * obj4 =strAddress;
    
    NSString * key5 =@"zipcode";
    NSString * obj5 =strZipcode;
    
    NSString * key6 =@"lat";
    NSString * obj6 =strLatitude;
    
    NSString * key7 =@"lng";
    NSString * obj7 =strLongitude;
    
    NSMutableArray *arrDictKey = [[NSMutableArray alloc]init];
    arrDictKey = [[dictMedia allKeys] mutableCopy];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:arrDictKey];
    [sortedArray sortUsingSelector:@selector(localizedStandardCompare:)];
    arrDictKey = [[NSMutableArray alloc]init];
    arrDictKey = [sortedArray mutableCopy];
    NSMutableArray *arrPostMedia=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrDictKey.count; i++)
    {
        NSMutableDictionary *dictM=[[NSMutableDictionary alloc]init];
        if ([[arrDictKey objectAtIndex:i] rangeOfString:@"Image"].length!=0)
        {
            [dictM setObject:@"1" forKey:@"mediaType"];
            [dictM setObject:[NSString stringWithFormat:@"%@",[dictParameters valueForKey:[NSString stringWithFormat:@"postjob%d",i+1]]] forKey:@"mediaContent"];
        }
        else if ([[arrDictKey objectAtIndex:i] rangeOfString:@"Video"].length!=0)
        {
            [dictM setObject:@"2" forKey:@"mediaType"];
            [dictM setObject:[NSString stringWithFormat:@"%@",[dictParameters valueForKey:[NSString stringWithFormat:@"postjob%d",i+1]]] forKey:@"mediaContent"];
        }
        [arrPostMedia addObject:dictM];
    }
    
    
    NSData * JsonDataMedia =[NSJSONSerialization dataWithJSONObject:arrPostMedia options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonStringMedia= [[NSString alloc] initWithData:JsonDataMedia encoding:NSUTF8StringEncoding];
    NSData *dataMedia = [jsonStringMedia dataUsingEncoding:NSUTF8StringEncoding];
    id jsonMedia = [NSJSONSerialization JSONObjectWithData:dataMedia options:0 error:nil];
    
    NSString * key8 =@"media";
    NSString * obj8 =jsonMedia;
    
    NSMutableArray *arrAnswer=[[NSMutableArray alloc]init];
    
    for (int i=0; i<dictQuestArr.count; i++)
    {
        NSMutableDictionary *dictAnswer=[[NSMutableDictionary alloc]init];
        
        [dictAnswer setObject:[dictQuestArr valueForKey:[NSString stringWithFormat:@"%d",i+3]] forKey:@"quest_name"];
        [dictAnswer setObject:[dictSelected valueForKey:[NSString stringWithFormat:@"%d",i+3]] forKey:@"answer"];
        [dictAnswer setObject:[dictTagArr valueForKey:[NSString stringWithFormat:@"%d",i+3]] forKey:@"tag_key"];
        [dictAnswer setObject:[dictTagKeyArr valueForKey:[NSString stringWithFormat:@"%d",i+3]] forKey:@"tag_keyword"];
        [arrAnswer addObject:dictAnswer];
    }
    
    NSData * JsonDataAnswer =[NSJSONSerialization dataWithJSONObject:arrAnswer options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonStringAnswer= [[NSString alloc] initWithData:JsonDataAnswer encoding:NSUTF8StringEncoding];
    NSData *dataAnswer = [jsonStringAnswer dataUsingEncoding:NSUTF8StringEncoding];
    id jsonAnswer = [NSJSONSerialization JSONObjectWithData:dataAnswer options:0 error:nil];
    NSString * key9 =@"answerkey";
    NSString * obj9 =jsonAnswer;
    
    NSString * key10 =@"description";
    //    NSString * obj10 =[dictSelected valueForKeyPath:[NSString stringWithFormat:@"%u",(arrCharacteristics.count-1)]];
    NSString * obj10 = @"";
    NSLog(@"description:%@",obj10);
    
    NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                   initWithObjects:@[obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10]
                                   forKeys:@[key1,key2,key3,key4,key5,key6,key7,key8,key9,key10]];
    
    NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
    NSLog(@"DATA %@",jsonString);
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@postjob",BaseURL]]];
    NSLog(@"PostJob URL:%@",request.URL);
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
             hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             completionHandler(true);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
             {
                 NSUserDefaults *Braintreeid = [NSUserDefaults standardUserDefaults];
                 [Braintreeid setObject:[result valueForKeyPath:@"braintreeID"] forKey:@"BrainTreeid"];
                 [Braintreeid synchronize];
                 
                 MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                 [self.navigationController.view addSubview:hud];
                 
                 hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                 hud.mode = MBProgressHUDModeCustomView;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                 hud.delegate = self;
                 hud.labelText = @"Done";
                 [hud show:YES];
                 [hud hide:YES afterDelay:2];
                 
                 KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutCenter);
                 popupViewGreat = [KLCPopup popupWithContentView:_PopupContent
                                                        showType:KLCPopupShowTypeBounceInFromTop
                                                     dismissType:KLCPopupDismissTypeBounceOutToBottom
                                                        maskType:KLCPopupMaskTypeDimmed
                                        dismissOnBackgroundTouch:NO
                                           dismissOnContentTouch:NO];
                 [popupViewGreat showWithLayout:layout];
                 
                 completionHandler(true);
             }
             else
             {
                 
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(true);
             }
         }
     }];
}

- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL
                                   outputURL:(NSURL*)outputURL
                        andCompletionHandler:(void (^)(bool result))completionHandler
{
    //setup video writer
    AVAsset *videoAsset = [[AVURLAsset alloc] initWithURL:inputURL options:nil];
    
    AVAssetTrack *videoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    
    CGSize videoSize = videoTrack.naturalSize;
    
    NSDictionary *videoWriterCompressionSettings =  [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:1250000], AVVideoAverageBitRateKey, nil];
    
    NSDictionary *videoWriterSettings = [NSDictionary dictionaryWithObjectsAndKeys:AVVideoCodecH264, AVVideoCodecKey, videoWriterCompressionSettings, AVVideoCompressionPropertiesKey, [NSNumber numberWithFloat:videoSize.width], AVVideoWidthKey, [NSNumber numberWithFloat:videoSize.height], AVVideoHeightKey, nil];
    
    AVAssetWriterInput* videoWriterInput = [AVAssetWriterInput
                                            assetWriterInputWithMediaType:AVMediaTypeVideo
                                            outputSettings:videoWriterSettings];
    
    videoWriterInput.expectsMediaDataInRealTime = YES;
    
    videoWriterInput.transform = videoTrack.preferredTransform;
    
    AVAssetWriter *videoWriter = [[AVAssetWriter alloc] initWithURL:outputURL fileType:AVFileTypeQuickTimeMovie error:nil];
    
    [videoWriter addInput:videoWriterInput];
    
    //setup video reader
    NSDictionary *videoReaderSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange] forKey:(id)kCVPixelBufferPixelFormatTypeKey];
    
    AVAssetReaderTrackOutput *videoReaderOutput = [[AVAssetReaderTrackOutput alloc] initWithTrack:videoTrack outputSettings:videoReaderSettings];
    
    AVAssetReader *videoReader = [[AVAssetReader alloc] initWithAsset:videoAsset error:nil];
    
    [videoReader addOutput:videoReaderOutput];
    
    //setup audio writer
    AVAssetWriterInput* audioWriterInput = [AVAssetWriterInput
                                            assetWriterInputWithMediaType:AVMediaTypeAudio
                                            outputSettings:nil];
    
    audioWriterInput.expectsMediaDataInRealTime = NO;
    
    [videoWriter addInput:audioWriterInput];
    
    //setup audio reader
    AVAssetTrack* audioTrack = [[videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    
    AVAssetReaderOutput *audioReaderOutput = [AVAssetReaderTrackOutput assetReaderTrackOutputWithTrack:audioTrack outputSettings:nil];
    
    AVAssetReader *audioReader = [AVAssetReader assetReaderWithAsset:videoAsset error:nil];
    
    [audioReader addOutput:audioReaderOutput];
    
    [videoWriter startWriting];
    
    //start writing from video reader
    [videoReader startReading];
    
    [videoWriter startSessionAtSourceTime:kCMTimeZero];
    
    dispatch_queue_t processingQueue = dispatch_queue_create("processingQueue1", NULL);
    
    [videoWriterInput requestMediaDataWhenReadyOnQueue:processingQueue usingBlock:
     ^{
         
         while ([videoWriterInput isReadyForMoreMediaData])
         {
             
             CMSampleBufferRef sampleBuffer;
             
             if ([videoReader status] == AVAssetReaderStatusReading &&
                 (sampleBuffer = [videoReaderOutput copyNextSampleBuffer]))
             {
                 
                 [videoWriterInput appendSampleBuffer:sampleBuffer];
                 CFRelease(sampleBuffer);
             }
             
             else {
                 
                 [videoWriterInput markAsFinished];
                 
                 if ([videoReader status] == AVAssetReaderStatusCompleted)
                 {
                     
                     //start writing from audio reader
                     [audioReader startReading];
                     
                     [videoWriter startSessionAtSourceTime:kCMTimeZero];
                     
                     dispatch_queue_t processingQueue = dispatch_queue_create("processingQueue2", NULL);
                     
                     [audioWriterInput requestMediaDataWhenReadyOnQueue:processingQueue usingBlock:^{
                         
                         while (audioWriterInput.readyForMoreMediaData)
                         {
                             
                             CMSampleBufferRef sampleBuffer;
                             
                             if ([audioReader status] == AVAssetReaderStatusReading &&
                                 (sampleBuffer = [audioReaderOutput copyNextSampleBuffer])) {
                                 
                                 [audioWriterInput appendSampleBuffer:sampleBuffer];
                                 CFRelease(sampleBuffer);
                             }
                             
                             else {
                                 
                                 [audioWriterInput markAsFinished];
                                 
                                 if ([audioReader status] == AVAssetReaderStatusCompleted)
                                 {
                                     
                                     [videoWriter finishWritingWithCompletionHandler:^()
                                      {
                                          tmpVideoURl=outputURL;
                                          completionHandler(true);
                                          
                                      }];
                                     
                                 }
                             }
                         }
                         
                     }
                      ];
                 }
             }
         }
     }
     ];
}

-(NSString *)createRandomName
{
    NSTimeInterval timeStamp = [ [ NSDate date ] timeIntervalSince1970 ];
    NSString *randomName = [ NSString stringWithFormat:@"M%f", timeStamp];
    randomName = [ randomName stringByReplacingOccurrencesOfString:@"." withString:@"" ];
    return randomName;
}
- (IBAction)btnLoginTapped:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:NSStringFromClass([self class]) forKey:@"OostaJobBuildJobViewController"];
    NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
    [dict setObject:_arrSelectedJob forKey:@"arrSelectedJob"];
    [dict setObject:[NSString stringWithFormat:@"%@",_strOther] forKey:@"strOther"];
    
    [dict setObject:[NSString stringWithFormat:@"%@",strLongitude] forKey:@"strLongitude"];
    [dict setObject:[NSString stringWithFormat:@"%@",strLatitude] forKey:@"strLatitude"];
    [dict setObject:[NSString stringWithFormat:@"%@",strZipcode] forKey:@"strZipcode"];
    [dict setObject:[NSString stringWithFormat:@"%@",strAddress] forKey:@"strAddress"];
    NSData *dataVal = [NSKeyedArchiver archivedDataWithRootObject:dictSelectedIndexArr];
    [dict setObject:dataVal forKey:@"dictSelectedIndexArr"];
    
    NSData *dataVal1 = [NSKeyedArchiver archivedDataWithRootObject:dictSelectedValueArr];
    [dict setObject:dataVal1 forKey:@"dictSelectedValueArr"];
    
    [dict setObject:dictSelected forKey:@"dictSelected"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"dictMedia.plist"];
    [NSKeyedArchiver archiveRootObject:dictMedia toFile:filePath];
    
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"DICTSKIPLOGIN"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"SKIPLOGIN" forKey:@"SKIPLOGIN"];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobLoginViewController *LoginObj=[[OostaJobLoginViewController alloc]initWithNibName:@"OostaJobLoginViewController IPad" bundle:nil];
        LoginObj.strPage=@"BUILD";
        [self.navigationController pushViewController:LoginObj animated:YES];
        
    }
    else if (IS_IPHONE5)
    {
        OostaJobLoginViewController *LoginObj=[[OostaJobLoginViewController alloc]initWithNibName:@"OostaJobLoginViewController" bundle:nil];
        LoginObj.strPage=@"BUILD";
        [self.navigationController pushViewController:LoginObj animated:YES];
        
    }
    else
    {
        OostaJobLoginViewController *LoginObj=[[OostaJobLoginViewController alloc]initWithNibName:@"OostaJobLoginViewController Small" bundle:nil];
        LoginObj.strPage=@"BUILD";
        [self.navigationController pushViewController:LoginObj animated:YES];
        
    }
    
}

- (IBAction)btnRegisterTapped:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:NSStringFromClass([self class]) forKey:@"OostaJobBuildJobViewController"];
    NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
    [dict setObject:_arrSelectedJob forKey:@"arrSelectedJob"];
    [dict setObject:[NSString stringWithFormat:@"%@",_strOther] forKey:@"strOther"];
    
    [dict setObject:[NSString stringWithFormat:@"%@",strLongitude] forKey:@"strLongitude"];
    [dict setObject:[NSString stringWithFormat:@"%@",strLatitude] forKey:@"strLatitude"];
    [dict setObject:[NSString stringWithFormat:@"%@",strZipcode] forKey:@"strZipcode"];
    [dict setObject:[NSString stringWithFormat:@"%@",strAddress] forKey:@"strAddress"];
    NSData *dataVal = [NSKeyedArchiver archivedDataWithRootObject:dictSelectedIndexArr];
    [dict setObject:dataVal forKey:@"dictSelectedIndexArr"];
    
    NSData *dataVal1 = [NSKeyedArchiver archivedDataWithRootObject:dictSelectedValueArr];
    [dict setObject:dataVal1 forKey:@"dictSelectedValueArr"];
    
    [dict setObject:dictSelected forKey:@"dictSelected"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"dictMedia.plist"];
    [NSKeyedArchiver archiveRootObject:dictMedia toFile:filePath];
    
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"DICTSKIPLOGIN"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"SKIPLOGIN" forKey:@"SKIPLOGIN"];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobRegisterViewController *RegisterObj=[[OostaJobRegisterViewController alloc]initWithNibName:@"OostaJobRegisterViewController IPad" bundle:nil];
        RegisterObj.strPage=@"BUILD";
        [self.navigationController pushViewController:RegisterObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobRegisterViewController *RegisterObj=[[OostaJobRegisterViewController alloc]initWithNibName:@"OostaJobRegisterViewController" bundle:nil];
        RegisterObj.strPage=@"BUILD";
        [self.navigationController pushViewController:RegisterObj animated:YES];
    }
    else
    {
        OostaJobRegisterViewController *RegisterObj=[[OostaJobRegisterViewController alloc]initWithNibName:@"OostaJobRegisterViewController Small" bundle:nil];
        RegisterObj.strPage=@"BUILD";
        [self.navigationController pushViewController:RegisterObj animated:YES];
    }
    
}
-(void)GotoAddressNotify
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobZipCodeViewController *ZipCodeObj=[[OostaJobZipCodeViewController alloc]initWithNibName:@"OostaJobZipCodeViewController IPad" bundle:nil];
        ZipCodeObj.strZipcode = strZipcode;
        [self.navigationController pushViewController:ZipCodeObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobZipCodeViewController *ZipCodeObj=[[OostaJobZipCodeViewController alloc]initWithNibName:@"OostaJobZipCodeViewController" bundle:nil];
        ZipCodeObj.strZipcode = strZipcode;
        [self.navigationController pushViewController:ZipCodeObj animated:YES];
    }
    else
    {
        OostaJobZipCodeViewController *ZipCodeObj=[[OostaJobZipCodeViewController alloc]initWithNibName:@"OostaJobZipCodeViewController Small" bundle:nil];
        ZipCodeObj.strZipcode = strZipcode;
        [self.navigationController pushViewController:ZipCodeObj animated:YES];
    }
    
}

-(void)logFile:(NSString *)log{
    NSString *filepath = [self logfilePath];
    NSError *err;
    NSString *txtInFile;
    NSString * logstring;
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filepath] == YES) {
        txtInFile = [[NSString alloc] initWithContentsOfFile:filepath encoding:NSUnicodeStringEncoding error:& err];
    }
    if (txtInFile != NULL)
    {
    logstring = [NSString stringWithFormat:@"%@\n%@%@",txtInFile, [NSDate date], log];
    } else
    {
    logstring = [NSString stringWithFormat:@"%@%@",[NSDate date], log];
    }
    
    BOOL ok = [logstring writeToFile:filepath atomically:YES encoding:NSUnicodeStringEncoding error:&err];
    
    if (!ok) {
        NSLog(@"Error writing file at %@\n%@",
              filepath, [err localizedFailureReason]);
    }
}

-(NSString *)logfilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentPath = [paths objectAtIndex:0];
    return [NSString stringWithFormat:@"%@/logfile.txt",documentPath];
}

@end
