//
//  EarningsViewController.m
//  OostaJob
//
//  Created by Ankush on 17/01/18.
//  Copyright © 2018 codewave. All rights reserved.
//

#import "EarningsViewController.h"

@interface EarningsViewController ()
{
    KLCPopup* popup;
    NSString *nextmonth;
    NSNumber *earnings;
    NSString *thismonthearning;
    int v;
}
@end

@implementation EarningsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   // [self transparentbar];
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    self.navigationController.navigationBarHidden=YES;
    [self EarningsAPI:^(bool resultEarnings)
     {
         if (resultEarnings == YES) {
             self.amountlbl.numberOfLines=0;
             self.monthlbl.numberOfLines=0;
             self.totalEarningslbl.numberOfLines=0;

             
             NSLog(@"earnings :%@",earnings);
             self.amountlbl.text = [[earnings stringValue] stringByAppendingString:@"$"];
             
         }
     }];
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)OnclickJobHistory:(id)sender {
    ContractorHistoryController *Obj=[[ContractorHistoryController alloc]init];
    [self.navigationController pushViewController:Obj animated:YES];
    
    
}
- (IBAction)Onclickmenubtn:(id)sender {
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}

-(void)EarningsAPI:(void (^)(bool resultEarnings))completionHandler{
    NSUserDefaults *fetch = [NSUserDefaults standardUserDefaults];
    v = [fetch integerForKey:@"saveversion"];
    if (v == 0) {
        v++;
        
    }
    else
    {
        
        
        v++;
        
    }
    NSUserDefaults *dee = [NSUserDefaults standardUserDefaults];
    [dee setInteger:v forKey:@"saveversion"];
    [dee synchronize];
    
    
    
    NSString *userid;
    NSUserDefaults *userid1 = [NSUserDefaults standardUserDefaults];
    userid = [userid1 objectForKey:@"Useridcont"];
    
    
    
    NSString *version=[NSString stringWithFormat:@"%d",v];
    
    
    
    NSString *str = [userid stringByAppendingString:@"?v="];
    NSString *strfinal = [str stringByAppendingString:version];
    NSLog(@"userid :%@",userid);
    if ([userid isEqualToString:@"(null)"]) {
        completionHandler(false);
    }
    else
    {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Sending...";
        hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
        hud.activityIndicatorColor=[UIColor colorWithRed:0.971 green:0.000 blue:0.452 alpha:1.000];
        
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        
        NSString * key1 =@"contractorId";
        NSString * obj1 = [[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1]
                                       forKeys:@[key1]];
        
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        
//        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@contractorEarnings",BaseURL,userid]]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@contractorEarnings/%@",BaseURL,strfinal]]];
        
        NSLog(@"Earnings url :%@",request.URL);
        
       // [request setHTTPBody:body];
        [request setHTTPMethod:@"Get"];
        
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 completionHandler(false);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKey:@"Result"] isEqualToString:@"Success"])
                 {
                     [hud hide:YES];
                     MBProgressHUD*  HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                     [self.navigationController.view addSubview:HUD];
                     
                     HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                     HUD.mode = MBProgressHUDModeCustomView;
                     HUD.color=[UIColor whiteColor];
                     HUD.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     HUD.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     HUD.delegate = self;
                     HUD.labelText = @"Success";
                     [HUD show:YES];
                     [HUD hide:YES afterDelay:2];
                     [popup dismissPresentingPopup];
                     earnings = [result valueForKeyPath:@"total_earnings"];

                     completionHandler(true);

                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     
                     hud.labelText = @"Error";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(false);

                 }
                 
                 
             }
         }];
    }
    }
}
@end
