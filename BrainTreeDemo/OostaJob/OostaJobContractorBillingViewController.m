//
//  OostaJobContractorBillingViewController.m
//  OostaJob
//
//  Created by Armor on 24/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobContractorBillingViewController.h"
#import "SWRevealViewController.h"

#import <BraintreeCore/BraintreeCore.h>
#import <BraintreePayPal/BraintreePayPal.h>
#import <BraintreeCard/BraintreeCard.h>
#import "OostaJobCreditCardViewController.h"
@interface OostaJobContractorBillingViewController ()<BTAppSwitchDelegate, BTViewControllerPresentingDelegate,SWRevealViewControllerDelegate,BTDropInViewControllerDelegate,BTAppSwitchDelegate,MBProgressHUDDelegate>
{
    NSString * strClientToken;
    NSMutableArray *dummy;
}
@property (nonatomic, strong) BTAPIClient *braintreeClient;
@end

@implementation OostaJobContractorBillingViewController
@synthesize arraydetails;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self setupUI];
    [self.imgBlur assignBlur];
//    SWRevealViewController *revealController = [self revealViewController];
//    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
//    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
//
    self.navigationController.navigationBarHidden=YES;
    if (!TARGET_IPHONE_SIMULATOR)
//        self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:@"production_t2wns2y2_dfy45jdj3dxkmz5m"];
//    eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIzZWQ3ZTliMTViN2Q3ZDcyMWRjYWFiODQ3M2E1YzExOTlmOTY4MmEwNjE5MmFkMjQ4ODhlODI0YzAxYTU2ODhhfGNyZWF0ZWRfYXQ9MjAxNS0wNC0xNFQxODo1NzoyNi42NzM2ODc0MTgrMDAwMFx1MDAyNm1lcmNoYW50X2lkPWRjcHNweTJicndkanIzcW5cdTAwMjZwdWJsaWNfa2V5PTl3d3J6cWszdnIzdDRuYzgiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvZGNwc3B5MmJyd2RqcjNxbi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzL2RjcHNweTJicndkanIzcW4vY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIn0sInRocmVlRFNlY3VyZUVuYWJsZWQiOnRydWUsInRocmVlRFNlY3VyZSI6eyJsb29rdXBVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvZGNwc3B5MmJyd2RqcjNxbi90aHJlZV9kX3NlY3VyZS9sb29rdXAifSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQiLCJtZXJjaGFudEFjY291bnRJZCI6InN0Y2gybmZkZndzenl0dzUiLCJjdXJyZW5jeUlzb0NvZGUiOiJVU0QifSwiY29pbmJhc2VFbmFibGVkIjp0cnVlLCJjb2luYmFzZSI6eyJjbGllbnRJZCI6IjExZDI3MjI5YmE1OGI1NmQ3ZTNjMDFhMDUyN2Y0ZDViNDQ2ZDRmNjg0ODE3Y2I2MjNkMjU1YjU3M2FkZGM1OWIiLCJtZXJjaGFudEFjY291bnQiOiJjb2luYmFzZS1kZXZlbG9wbWVudC1tZXJjaGFudEBnZXRicmFpbnRyZWUuY29tIiwic2NvcGVzIjoiYXV0aG9yaXphdGlvbnM6YnJhaW50cmVlIHVzZXIiLCJyZWRpcmVjdFVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tL2NvaW5iYXNlL29hdXRoL3JlZGlyZWN0LWxhbmRpbmcuaHRtbCJ9LCJtZXJjaGFudElkIjoiZGNwc3B5MmJyd2RqcjNxbiIsInZlbm1vIjoib2ZmbGluZSIsImFwcGxlUGF5Ijp7InN0YXR1cyI6Im1vY2siLCJjb3VudHJ5Q29kZSI6IlVTIiwiY3VycmVuY3lDb2RlIjoiVVNEIiwibWVyY2hhbnRJZGVudGlmaWVyIjoibWVyY2hhbnQuY29tLmJyYWludHJlZXBheW1lbnRzLm1pY2tleXJlaXNzLkR1YWxBcHBsZVBheS5icmFpbnRyZWUiLCJzdXBwb3J0ZWROZXR3b3JrcyI6WyJ2aXNhIiwibWFzdGVyY2FyZCIsImFtZXgiXX19
//    sandbox_9dbg82cq_dcpspy2brwdjr3qn
    [self getClientToken];
//    [self tappedMyPayButton];
//    if ([[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"PAYMENT"] isEqualToString:@"YES"])
//    {
//        _viewUnsubscribe.hidden = NO;
//    }
//    else
//    {
//        _viewUnsubscribe.hidden = YES;
//    }
    // Do any additional setup after loading the view from its nib.
}
-(void)settingButton
{
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-UIDesign
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.lblBilling.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblPayUsing.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblSubscription.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblTitle.font=[UIFont fontWithName:FONT_BOLD size:45.0f];
        self.lbl30.font=[UIFont fontWithName:FONT_BOLD size:150.0f];
        self.lblDollar.font=[UIFont fontWithName:FONT_BOLD size:120.0f];
        self.lblPermonth.font=[UIFont fontWithName:FONT_BOLD size:40.0f];
        self.btnUnsubscribe.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
    }
    else if(IS_IPHONE5)
    {
        self.lblBilling.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblPayUsing.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.lblSubscription.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblTitle.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lbl30.font=[UIFont fontWithName:FONT_BOLD size:75.0f];
        self.lblDollar.font=[UIFont fontWithName:FONT_BOLD size:60.0f];
        self.lblPermonth.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.btnUnsubscribe.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        
    }
    else
    {
        self.lblBilling.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblPayUsing.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.lblSubscription.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblTitle.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lbl30.font=[UIFont fontWithName:FONT_BOLD size:75.0f];
        self.lblDollar.font=[UIFont fontWithName:FONT_BOLD size:60.0f];
        self.lblPermonth.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.btnUnsubscribe.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
    }
}

#pragma mark-Button Action
-(IBAction)btnUnsubscribe:(id)sender
{
    //Action for unSubScribe Page.
}
-(IBAction)btnPayPal:(id)sender
{
    /*Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        //Move to PayPal Page.
        if (!TARGET_IPHONE_SIMULATOR)
        {
            BTPayPalDriver *payPalDriver = [[BTPayPalDriver alloc] initWithAPIClient:self.braintreeClient];
            payPalDriver.viewControllerPresentingDelegate = self;
            payPalDriver.appSwitchDelegate = self; // Optional
            
            // Start the Vault flow, or...
            [payPalDriver authorizeAccountWithCompletion:^(BTPayPalAccountNonce *tokenizedPayPalAccount, NSError *error)
             {
                 NSLog(@"error:%@",error);
             }];
            
            // ...start the Checkout flow
            BTPayPalRequest *request = [[BTPayPalRequest alloc] initWithAmount:@"30.00"];
            request.currencyCode = @"USD";
            [payPalDriver requestOneTimePayment:request
                                     completion:^(BTPayPalAccountNonce *tokenizedPayPalAccount, NSError *error)
             {
                 NSLog(@"%@",tokenizedPayPalAccount);
                 
                 if (tokenizedPayPalAccount)
                 {
                     NSLog(@"Got a nonce! %@", tokenizedPayPalAccount.nonce);
                     BTPostalAddress *address = tokenizedPayPalAccount.billingAddress;
                     NSLog(@"Billing address:\n%@\n%@\n%@ %@\n%@ %@", address.streetAddress, address.extendedAddress, address.locality, address.region, address.postalCode, address.countryCodeAlpha2);
                     [self getPayment];
                     
                 } else if (error)
                 {
                     // Handle error
                 } else
                 {
                     // User cancelled
                 }
                 
             }];
            
            
            
            
            
        }
    }*/
    
    /*OostaJobCreditCardViewController * creditObj = [[OostaJobCreditCardViewController alloc]initWithNibName:@"OostaJobCreditCardViewController" bundle:nil];
    creditObj.strToken = strClientToken;
    [self.navigationController pushViewController:creditObj animated:YES];*/
    [self tappedMyPayButton];
    //[self tappedEarnings];
    
}
#pragma mark - BTViewControllerPresentingDelegate

// Required
- (void)paymentDriver:(id)paymentDriver
requestsPresentationOfViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Required
- (void)paymentDriver:(id)paymentDriver
requestsDismissalOfViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - BTAppSwitchDelegate

// Optional - display and hide loading indicator UI
- (void)appSwitcherWillPerformAppSwitch:(id)appSwitcher {
    [self showLoadingUI];
    
    // You may also want to subscribe to UIApplicationDidBecomeActiveNotification
    // to dismiss the UI when a customer manually switches back to your app since
    // the payment button completion block will not be invoked in that case (e.g.
    // customer switches back via iOS Task Manager)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideLoadingUI:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}

- (void)appSwitcherWillProcessPaymentInfo:(id)appSwitcher {
    [self hideLoadingUI:nil];
}

#pragma mark - Private methods

- (void)showLoadingUI
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
}

- (void)hideLoadingUI:(NSNotification *)notification
{
    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    
}

-(IBAction)btnVisa:(id)sender
{
    //move to Visa Page.
}
- (IBAction)btnMenuTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

//    SWRevealViewController *revealController = [self revealViewController];
//    [revealController revealToggle:self];
}
-(void)JobHiringContractorandCompletionHandler:(void (^)(bool resultJobHirre))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];

        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;

        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Hiring...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        NSString *userid1;
        NSUserDefaults *u = [NSUserDefaults standardUserDefaults];
        userid1 = [u objectForKey:@"useridlogin"];
        
        
        NSString *jobid1;
        NSUserDefaults *j = [NSUserDefaults standardUserDefaults];
        jobid1 = [j objectForKey:@"jobid1"];

        NSString * key1 =@"userID";
        NSString * obj1 =_struserid1;

        NSString * key2 =@"jobid";
        NSString * obj2 =_strpostid1;

        NSString * key3 =@"meetingTime";
        NSString * obj3 =self.selectedtime1;

        NSString * key4 =@"meetingDate";
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"YYYY-MM-dd"];
        NSString *date = [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:self.selecteddate1]];
        NSString * obj4 =date;

        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1,obj2,obj3,obj4]
                                       forKeys:@[key1,key2,key3,key4]];

        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];

        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];

        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@jobhired",BaseURL]]];
        NSLog(@"jobhired URL:%@",request.URL);
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];

        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {

                 NSLog(@"Server Error : %@", error);

                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];

                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;

                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(true);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);

                 if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                 {

//                     if(_delegate && [_delegate respondsToSelector:@selector(getreloadto:)])
//                     {
//                         [_delegate getreloadto:@"Rate"];
//                     }
                     [self.navigationController popViewControllerAnimated:YES];
                     [hud hide:YES];
                     MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                     [self.navigationController.view addSubview:hud];

                     hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                     hud.mode = MBProgressHUDModeCustomView;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     hud.delegate = self;
                     hud.labelText = @"Hired Successfully";
                     [hud show:YES];
                     [hud hide:YES afterDelay:2];

                 }
                 else
                 {
                     NSLog(@"%@",SERVER_ERR);

                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];

                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;

                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];

                 }

                 completionHandler(true);
             }
         }];

    }

}



-(void)Paymentbraintree
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Billing to server...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];

        
        NSString *amount1;
        NSUserDefaults *a = [NSUserDefaults standardUserDefaults];
        amount1 = [a objectForKey:@"amount1"];
        
        NSString *contractorid1;
        NSUserDefaults *c = [NSUserDefaults standardUserDefaults];
        contractorid1 = [c objectForKey:@"contractorid1"];
        
        NSString *userid1;
        
        NSUserDefaults *u = [NSUserDefaults standardUserDefaults];
        userid1 = [u objectForKey:@"useridlogin"];
        
        NSLog(@"useridlogin :%@",userid1);
        
        NSString *jobid1;
        NSUserDefaults *j = [NSUserDefaults standardUserDefaults];
        jobid1 = [j objectForKey:@"jobid1"];
        NSString * obj5;
        NSString * key1 =@"jobID";
        NSString * obj1 = jobid1;
        NSString * key2 =@"userID";
        NSString * obj2 = [[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];;
        NSString * key3 =@"contractorID";
        NSString * obj3 =contractorid1;
        NSString * key4 =@"amount";
        NSString * obj4 =amount1;
        NSString * key5 =@"transaction_for";
        NSLog(@"amount in oostaesh :%@",amount1);
        if ([_fromcomplete isEqualToString:@"fromcomplete"]) {
            obj5 =@"final";
        }
        else{
            obj5 =@"initial";

        }
        NSString * key6 =@"status";
        NSString * obj6 =@"success";
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1,obj2,obj3,obj4,obj5,obj6]
                                       forKeys:@[key1,key2,key3,key4,key5,key6]];
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://52.41.134.73/payAmount"]];
        NSLog(@"PayAmount:%@",request.URL);
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

        
        
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     [hud hide:YES];
                     
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     hud.labelText = @"Successfully paid";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     _viewUnsubscribe.hidden=YES;
                     [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"PAYMENT"];
                     
                    
                     if ([_fromcomplete isEqualToString:@"fromcomplete"]) {
                        // [self.navigationController popViewControllerAnimated:YES];
                         
                         [self CustomerjobCompleted:^(bool resultcompleted){
                             if (resultcompleted == YES) {
                                 OostaJobFeedbackViewController *DetObj;

                        
                         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
                         {
                             DetObj = [[OostaJobFeedbackViewController alloc]initWithNibName:@"OostaJobFeedbackViewController IPad" bundle:nil];
                         }
                         else if (IS_IPHONE5)
                         {
                             DetObj = [[OostaJobFeedbackViewController alloc]initWithNibName:@"OostaJobFeedbackViewController" bundle:nil];
                         }
                         else
                         {
                             DetObj = [[OostaJobFeedbackViewController alloc]initWithNibName:@"OostaJobFeedbackViewController Small" bundle:nil];
                         }
                         DetObj.strJobId=_strpostid1;
                         DetObj.arrDetails= arraydetails;

                         NSLog(@"arr details before :%@",DetObj.arrDetails);
                         [self.navigationController pushViewController:DetObj animated:YES];
                             }
                         }];

                     }
                     else
                     {
                     [self JobHiringContractorandCompletionHandler:^(bool resultJobHire) {
                                          if (resultJobHire == YES)
                                          {
                                              NSLog(@"Hired");
                                              [self gotoJobs];
                                          }
                                      }];
                     }
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                 }
             }
             
         }];
    }
}

-(void)CustomerjobCompleted:(void (^)(bool resultcompleted))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Loading...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        NSString * strKey = @"Jobrated";
        
        
        NSString *key1 = @"jobId";
        NSString *obj1 = _strpostid1;
        
        NSString *key2 = @"loggedUserId";
        NSString *obj2 = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"USERDETAILS.userID"];
        
        
        NSString *key3 = @"action_by";
        NSString *obj3 = @"customer";
        
        NSString *key4 = @"userID";
        NSString *obj4 = [arraydetails valueForKeyPath:@"contractorID"];
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1,obj2,obj3,obj4]
                                       forKeys:@[key1,key2,key3,key4]];
        
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@makeJobCompleted",BaseURL]]];
        NSLog(@"PostJob URL:%@",request.URL);
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = @"Job Successfully  Completed";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                     completionHandler(true);
                     
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(false);
                 }
             }
         }];
    }
}
-(void)gotoJobs
{
    SWRevealViewController *revealController = self.revealViewController;
    UIViewController *newFrontController = nil;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController IPad" bundle:nil];
        JobObj.redirectPage = @"Rate";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
    }
    else if (IS_IPHONE5)
    {
        OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
        JobObj.redirectPage = @"Rate";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
    }
    else
    {
        OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
        JobObj.redirectPage = @"Rate";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
    }
    [revealController pushFrontViewController:newFrontController animated:YES];
}
-(void)getPayment
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Billing to server...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        
        NSString * key1 =@"userID";
        NSString * obj1 =[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];
        NSString * key2 =@"plan";
        NSString * obj2 =@"1 month";
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1,obj2]
                                       forKeys:@[key1,key2]];
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@paymentconfigre",BaseURL]]];
        NSLog(@"paymentconfigre:%@",request.URL);
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     [hud hide:YES];
                     
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     hud.labelText = @"Successfully paid";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     _viewUnsubscribe.hidden=YES;
                     [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"PAYMENT"];
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                 }
             }
             
         }];
    }
}
-(void)getClientToken
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Getting Client Token...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        NSUserDefaults *brain = [NSUserDefaults standardUserDefaults];

        NSString *key1 = @"braintreeID";
        NSString *obj1 = [brain objectForKey:@"BrainTreeid"];
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1]
                                       forKeys:@[key1]];
        
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getclientaccesstoken",BaseURL]]];
        NSLog(@"getclientaccesstoken:%@",request.URL);
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"result"] isEqualToString:@"Success"])
                 {
                     
                     [hud hide:YES];
                     if (!TARGET_IPHONE_SIMULATOR)
                         self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:[NSString stringWithFormat:@"%@",[result valueForKeyPath:@"clientToken"]]];
                     strClientToken = [NSString stringWithFormat:@"%@",[result valueForKeyPath:@"clientToken"]];
                     [self tappedMyPayButton];
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                 }
             }
         }];
    }
}
-(IBAction)tappedEarnings{
    BTDropInViewController *dropInViewController = [[BTDropInViewController alloc]
                                                    initWithAPIClient:self.braintreeClient];
    dropInViewController.delegate = self;
    UIBarButtonItem *item = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                             target:self
                             action:@selector(userDidCancelPayment)];
    dropInViewController.navigationItem.leftBarButtonItem = item;
    dropInViewController.title = @"Earnings";
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithWhite:0.000 alpha:0.600]];
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 1);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                           shadow, NSShadowAttributeName,
                                                           [UIFont fontWithName:FONT_BOLD size:20.0], NSFontAttributeName, nil]];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[UIColor whiteColor]];
    
    UINavigationController *navigationController = [[UINavigationController alloc]
                                                    initWithRootViewController:dropInViewController];
    [self presentViewController:navigationController animated:YES completion:nil];

}

- (IBAction)tappedMyPayButton {
    
    // If you haven't already, create and retain a `BTAPIClient` instance with a tokenization
    // key or a client token from your server.
    // Typically, you only need to do this once per session.
    //self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:aClientToken];
    
    // Create a BTDropInViewController
    BTDropInViewController *dropInViewController = [[BTDropInViewController alloc]
                                                    initWithAPIClient:self.braintreeClient];
    NSString *amountadd;
    NSUserDefaults *amount = [NSUserDefaults standardUserDefaults];
    amountadd = [amount objectForKey:@"amount1"];
    
    dropInViewController.delegate = self;
    BTPaymentRequest *request = [[BTPaymentRequest alloc] init ];
    request.amount = amountadd;
    request.currencyCode = @"USD";
    request.summaryTitle = @"OostaJob";
    request.summaryDescription = @"";
    request.displayAmount = @"";
    request.callToActionText = @"PAY";
    request.shouldHideCallToAction = NO;
    dropInViewController.paymentRequest = request;
    

    
    // This is where you might want to customize your view controller (see below)
    
    // The way you present your BTDropInViewController instance is up to you.
    // In this example, we wrap it in a new, modally-presented navigation controller:
    UIBarButtonItem *item = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                             target:self
                             action:@selector(userDidCancelPayment)];
    dropInViewController.navigationItem.leftBarButtonItem = item;
    dropInViewController.title = @"Check Out";
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithWhite:0.000 alpha:0.600]];
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 1);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                           shadow, NSShadowAttributeName,
                                                           [UIFont fontWithName:FONT_BOLD size:20.0], NSFontAttributeName, nil]];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[UIColor whiteColor]];
    
    UINavigationController *navigationController = [[UINavigationController alloc]
                                                    initWithRootViewController:dropInViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)userDidCancelPayment {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dropInViewController:(BTDropInViewController *)viewController
  didSucceedWithTokenization:(BTPaymentMethodNonce *)paymentMethodNonce
{
    // Send payment method nonce to your server for processing
    NSLog(@"paymentMethodNonce:%@",paymentMethodNonce.nonce);
    [self postNonceToServer:paymentMethodNonce.nonce];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)postNonceToServer:(NSString *)paymentMethodNonce {
    // Update URL with your server
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            hud.mode = MBProgressHUDModeText;
            hud.color=[UIColor whiteColor];
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            
            hud.labelText = INTERNET_ERR;
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
        else
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.color=[UIColor whiteColor];
            hud.labelText = @"Paying...";
            hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
            hud.activityIndicatorColor=[UIColor colorWithRed:0.971 green:0.000 blue:0.452 alpha:1.000];
            
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            [hud show:YES];
            NSUserDefaults *brain = [NSUserDefaults standardUserDefaults];

            NSString *amountadd;
            NSUserDefaults *amount = [NSUserDefaults standardUserDefaults];
            amountadd = [amount objectForKey:@"amount1"];
            
            NSString * key1 =@"nonce";
            NSString * obj1 =paymentMethodNonce;
            NSString * key2 =@"amount";
            NSString * obj2 =amountadd;
            
            NSString *key3 = @"braintreeID";
            NSString *obj3 = [brain objectForKey:@"BrainTreeid"];
            
            NSLog(@"brain tree in postnonce :%@",[brain objectForKey:@"BrainTreeid"]);
            NSLog(@"amount :%@",amountadd);
            NSLog(@"nonce :%@",paymentMethodNonce);

            NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                           initWithObjects:@[obj1,obj2,obj3]
                                           forKeys:@[key1,key2,key3]];
            
            NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
            NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
            NSLog(@"DATA %@",jsonString);
            NSMutableData *body = [NSMutableData data];
            
            [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@checkout",BaseURL]]];
            [request setHTTPBody:body];
            [request setHTTPMethod:@"POST"];
            
            [NSURLConnection sendAsynchronousRequest: request
                                               queue: [NSOperationQueue mainQueue]
                                   completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if (error || !data)
                 {
                     NSLog(@"Server Error : %@", error);
                 }
                 else
                 {
                     NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                            options:0
                                                                              error:NULL];
                     NSLog(@"Result %@",result);
                     if ([[result valueForKeyPath:@"result.success"] boolValue] == 1)
                     {
                         [hud hide:YES];
                       //  [self getPayment];
                         
                         [self Paymentbraintree];
                     }
                     else
                     {
                         [hud hide:YES];
                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                         
                         hud.mode = MBProgressHUDModeText;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                         
                         hud.labelText = @"Payment unsuccessfull";
                         hud.margin = 10.f;
                         hud.yOffset = 20.f;
                         hud.removeFromSuperViewOnHide = YES;
                         [hud hide:YES afterDelay:2];
                     }
                     
                     
                 }
             }];
        }
    }
}
- (void)dropInViewControllerDidCancel:(__unused BTDropInViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}





@end
