//
//  OostaJobListOfJobsViewController.h
//  OostaJob
//
//  Created by Apple on 06/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OostaJobListOfJobsTableViewCell.h"

@interface OostaJobListOfJobsViewController : UIViewController<UITextFieldDelegate>
{
    NSMutableArray *JobTypeArr;
 }



@property (strong, nonatomic) IBOutlet UIView *ViewContent;
 //  Lable
@property (strong, nonatomic) IBOutlet UILabel *lblSelectservice;

//Table View
@property (strong, nonatomic) IBOutlet UITableView *tblSelectservice;


-(void)getJobType;
- (IBAction)btnMenu:(id)sender;

@end
