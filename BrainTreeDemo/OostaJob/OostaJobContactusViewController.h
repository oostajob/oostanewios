//
//  OostaJobContactusViewController.h
//  OostaJob
//
//  Created by Armor on 17/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Popupcontroller.h"
#import "CZPicker.h"

@interface OostaJobContactusViewController : UIViewController<CZPickerViewDataSource, CZPickerViewDelegate>
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;

@property (weak, nonatomic) IBOutlet UITextField *txtname;
@property (weak, nonatomic) IBOutlet UITextField *txtemail;
@property (weak, nonatomic) IBOutlet UIButton *btnsend;

@property (weak, nonatomic) IBOutlet UIView *viewName;
@property (weak, nonatomic) IBOutlet UIView *viewEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
- (IBAction)btnSend:(id)sender;
- (IBAction)btnMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtMessage;
@property (weak, nonatomic) IBOutlet UIScrollView *scrlContactus;
- (IBAction)Onclickpopup:(id)sender;

@end
