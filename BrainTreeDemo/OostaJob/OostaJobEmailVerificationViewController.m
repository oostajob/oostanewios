//
//  OostaJobEmailVerificationViewController.m
//  OostaJob
//
//  Created by Apple on 28/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobEmailVerificationViewController.h"
#import "OostaJobListOfJobsViewController.h"
#import "CustomerRearViewController.h"
#import "SWRevealViewController.h"
#import "OostaJobContractorStatusViewController.h"
#import "OostaJobContractorRegisterViewController.h"
#import "RearViewController.h"
#import "OostaJobWelcomeViewController.h"

@interface OostaJobEmailVerificationViewController ()<SWRevealViewControllerDelegate>
{
    int v;
}
@end

@implementation OostaJobEmailVerificationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    v=0;

    [[NSUserDefaults standardUserDefaults]setObject:@"EMAIL" forKey:@"SCREEN"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBarHidden=YES;
    [self.imgblur assignBlur];
    [self setupUI];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        [self repeatCheck];
    }
    // Do any additional setup after loading the view from its nib.
}
-(void)repeatCheck
{
    [self CheckActivactionProfileandCompletionHandler:^(bool resultProfile)
     {
         if (resultProfile==YES)
         {
             [self performSelector:@selector(repeatCheck) withObject:nil
                        afterDelay:2.0];
         }
         else
         {
             NSLog(@"result profile%@",resultProfile);
             NSLog(@"Go to next");
         }
     }];
}
#pragma mark - Setup UI
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        [self.btnResend.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        self.lblFirst.font=[UIFont fontWithName:FONT_THIN size:26.0f];
        self.lblSecond.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblThird.font=[UIFont fontWithName:FONT_THIN size:26.0f];
    }
    else
    {
        [self.btnResend.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:15.0f]];
        self.lblFirst.font=[UIFont fontWithName:FONT_THIN size:18.0f];
        self.lblSecond.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblThird.font=[UIFont fontWithName:FONT_THIN size:18.0f];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnResend:(id)sender
{
    [self ResendVerificationEmailandCompletionHandler:^(bool resultProfile)
     {
         NSLog(@"Resend Verification Called");
     }];
}

-(void)ResendVerificationEmailandCompletionHandler:(void (^)(bool resultEmail))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Sending...";
        hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
        hud.activityIndicatorColor=[UIColor colorWithRed:0.971 green:0.000 blue:0.452 alpha:1.000];
        
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        NSString *userId=[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@resendmail/%@",BaseURL,userId]]];
        NSLog(@"CheckVerification:%@",request.URL);
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 completionHandler(true);
                 
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     
                     NSLog(@"Mail Sent");
                     [hud hide:YES];
                     MBProgressHUD*  HUD = [[MBProgressHUD alloc] initWithView:self.view];
                     [self.view addSubview:HUD];
                     
                     HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                     HUD.mode = MBProgressHUDModeCustomView;
                     HUD.color=[UIColor whiteColor];
                     HUD.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                     HUD.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                     HUD.delegate = self;
                     HUD.labelText = @"Mail Sent";
                     [HUD show:YES];
                     [HUD hide:YES afterDelay:2];
                 }
                 else
                 {
                     NSLog(@"Result %@",result);
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                 }
                 completionHandler(true);
             }
             
         }];
    }
}
-(void)CheckActivactionProfileandCompletionHandler:(void (^)(bool resultProfile))completionHandler
{
    v++;
    NSString *strUserID=[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];
    NSLog(@"USERDETAILS:%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS"]);
    NSLog(@"strUserID:%@",strUserID);
    
    NSString *version=[NSString stringWithFormat:@"%d",v];

    
    
    NSString *str = [strUserID stringByAppendingString:@"?v="];
    NSString *strfinal = [str stringByAppendingString:version];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@verificationstatus/%@",BaseURL,strfinal]]];
    
    
//    NSMutableURLRequest *check = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://52.41.134.73/api/index.php/verificationstatus/913?v=1.1"]];
    
    NSLog(@"verificationstatus:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             completionHandler(true);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 NSLog(@"Result %@",result);
                 
                 if ([[result valueForKeyPath:@"EmailStatus"] isEqualToString:@"Active"])
                 {
                     if ([[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userType"] isEqualToString:@"1"])
                     {
                         [self gotoListOfJobs];
                         completionHandler(false);
                         [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCUSTOMER" forKey:@"SCREEN"];
                     }
                     else
                     {
                         completionHandler(false);
                         [self gotoProfileVerification];
                         
                     }
                     
                 }
                 else
                 {
                     completionHandler(true);
                 }
                 
             }
             else
             {
                 NSLog(@"Result %@",result);
                 completionHandler(true);
             }
             
         }
         
     }];
}
-(void)gotoProfileVerification
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobContractorStatusViewController *ContObj=[[OostaJobContractorStatusViewController alloc]initWithNibName:@"OostaJobContractorStatusViewController IPad" bundle:nil];
        [self.navigationController pushViewController:ContObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobContractorStatusViewController *ContObj=[[OostaJobContractorStatusViewController alloc]initWithNibName:@"OostaJobContractorStatusViewController" bundle:nil];
        [self.navigationController pushViewController:ContObj animated:YES];
    }
    else
    {
        OostaJobContractorStatusViewController *ContObj=[[OostaJobContractorStatusViewController alloc]initWithNibName:@"OostaJobContractorStatusViewController Small" bundle:nil];
        [self.navigationController pushViewController:ContObj animated:YES];
    }
    
}
-(void)gotoListOfJobs
{
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];
    OostaJobListOfJobsViewController *CustomerfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
    }
    else
    {
        CustomerfrontViewController = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
    }
    
    CustomerRearViewController *CustomrrearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController" bundle:nil];
    }
    else
    {
        CustomrrearViewController= [[CustomerRearViewController alloc] initWithNibName:@"CustomerRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomerfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:CustomrrearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    UINavigationController *viewControllerNavigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    
    self.window.rootViewController = viewControllerNavigationController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"SKIPLOGIN"] isEqualToString:@"SKIPLOGIN"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SKIPLOGIN"];
        [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] goToBuildYourJob];
    }
}
-(IBAction)backBtnTapped:(id)sender
{
    
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];

    OostaJobWelcomeViewController *frontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        frontViewController = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        frontViewController = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController" bundle:nil];
    }
    else
    {
        frontViewController = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController Small" bundle:nil];
    }
    
    RearViewController *rearViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        rearViewController= [[RearViewController alloc] initWithNibName:@"RearViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        rearViewController= [[RearViewController alloc] initWithNibName:@"RearViewController" bundle:nil];
    }
    else
    {
        rearViewController= [[RearViewController alloc] initWithNibName:@"RearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    self.window.rootViewController = self.viewController;
   
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];

}
@end
