//
//  OostaJobWelcomeViewController.h
//  OostaJob
//
//  Created by Apple on 20/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SWRevealViewController;
@interface OostaJobWelcomeViewController : UIViewController<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIPageControl *pagecontrol;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnSkipLogin;
- (IBAction)btnLoginTapped:(id)sender;
- (IBAction)btnRegisterTapped:(id)sender;
- (IBAction)pageControlTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *scrlCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerHeader1;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerSub1;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerHeader2;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerSub2;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerHeader3;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerSub3;

@property (strong, nonatomic) IBOutlet UIScrollView *scrlContractor;
@property (weak, nonatomic) IBOutlet UILabel *lblContractorHeader1;
@property (weak, nonatomic) IBOutlet UILabel *lblContractorSub1;
@property (weak, nonatomic) IBOutlet UILabel *lblContractorHeader2;
@property (weak, nonatomic) IBOutlet UILabel *lblContractorSub2;
@property (weak, nonatomic) IBOutlet UILabel *lblContractorHeader3;
@property (weak, nonatomic) IBOutlet UILabel *lblContractorSub3;
@property (weak, nonatomic) IBOutlet UILabel *lblContractorHeader4;
@property (weak, nonatomic) IBOutlet UILabel *lblContractorSub4;
@property (weak, nonatomic) IBOutlet UILabel *lblContractorHeader5;
@property (weak, nonatomic) IBOutlet UILabel *lblContractorSub5;
@property (weak, nonatomic) IBOutlet UILabel *lblContractorHeader6;
@property (weak, nonatomic) IBOutlet UILabel *lblContractorSub6;
@property (strong, nonatomic) NSString *strPage;
@property (weak, nonatomic) IBOutlet UILabel *lblMainTitle;
- (IBAction)btnMenu:(id)sender;
//-(void)gotoHowItWorksFor:(NSString *)userType;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *viewController;
- (IBAction)btnSkipLoginPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgCustomerDown;
@property (weak, nonatomic) IBOutlet UIImageView *imgContractorDown;
@end
