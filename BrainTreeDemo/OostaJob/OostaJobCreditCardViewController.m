//
//  OostaJobCreditCardViewController.m
//  OostaJob
//
//  Created by Apple on 09/02/16.
//  Copyright © 2016 Armor. All rights reserved.
//

#import "OostaJobCreditCardViewController.h"
#import <BraintreeCard/BraintreeCard.h>
#import <Braintree3DSecure/Braintree3DSecure.h>
@interface OostaJobCreditCardViewController ()<BTUICardFormViewDelegate>

@end

@implementation OostaJobCreditCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (!TARGET_IPHONE_SIMULATOR)
    self.cardFormView.optionalFields = self.cardFormView.optionalFields ^ BTUICardFormOptionalFieldsPostalCode;
    [self.imgBg assignBlur];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#if TARGET_IPHONE_SIMULATOR
- (void)cardFormViewDidChange:(BTUICardFormView *)cardFormView {
    if (cardFormView.valid) {
        NSLog(@"%@",[NSString stringWithFormat:
                                           @"😍 YOU DID IT \n"
                                           "Number:     %@\n"
                                           "Expiration: %@/%@\n"
                                           "CVV:        %@\n"
                                           "Postal:     %@",
                                           cardFormView.number,
                                           cardFormView.expirationMonth,
                                           cardFormView.expirationYear,
                                           cardFormView.cvv,
                                           cardFormView.postalCode]);
        [self.cardFormView endEditing:YES];;
    } else {
        NSLog(@"INVALID 🐴");
    }
}


- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnHome:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.cardFormView endEditing:YES];;
    
}
- (IBAction)btnPayPressed:(id)sender
{
    if (self.cardFormView.valid)
    {

        BTAPIClient *braintreeClient = [[BTAPIClient alloc] initWithAuthorization:_strToken];
        BTCardClient *cardClient = [[BTCardClient alloc] initWithAPIClient:braintreeClient];
        BTCard *card = [[BTCard alloc] initWithNumber:self.cardFormView.number
                                      expirationMonth:self.cardFormView.expirationMonth
                                       expirationYear:self.cardFormView.expirationYear
                                                  cvv:self.cardFormView.cvv];
        
        [cardClient tokenizeCard:card
                      completion:^(BTCardNonce *tokenizedCard, NSError *error) {
                          
                          NSLog(@"%@",tokenizedCard);
                          
                          if (tokenizedCard)
                          {
                              NSLog(@"Got a nonce! %@", tokenizedCard.nonce);
                               NSLog(@"%@", [tokenizedCard debugDescription]);
                              BTThreeDSecureDriver *threeDSecure = [[BTThreeDSecureDriver alloc] initWithAPIClient:braintreeClient delegate:self];
                              
                              [threeDSecure verifyCardWithNonce:tokenizedCard.nonce
                                                         amount:[NSDecimalNumber decimalNumberWithString:@"30"]
                                                     completion:^(BTThreeDSecureCardNonce * _Nullable threeDSecureCard, NSError * _Nullable error)
                               {
                                   if (error)
                                   {
                                       [self showErrorAlertWithMessage:error.description];
                                   }
                                   else
                                   {
                                     [self getPayment];
                                   }
                               }];
                              
                              
                          } else if (error)
                          {
                              [self showErrorAlertWithMessage:error.description];
                              // Handle error
                          } else
                          {
                              // User cancelled
                          }
                      }];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = @"Invalid card";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
}

-(void)getPayment
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Billing to server...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        
        NSString * key1 =@"userID";
        NSString * obj1 =[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];
        NSString * key2 =@"plan";
        NSString * obj2 =@"1 month";
        
        NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                       initWithObjects:@[obj1,obj2]
                                       forKeys:@[key1,key2]];
        NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
        NSLog(@"DATA %@",jsonString);
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@paymentconfigre",BaseURL]]];
        NSLog(@"paymentconfigre:%@",request.URL);
        [request setHTTPBody:body];
        [request setHTTPMethod:@"POST"];
        
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     [hud hide:YES];
                     
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     hud.labelText = @"Successfully paid";
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                     [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"PAYMENT"];
                 }
                 else
                 {
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     
                 }
             }
             
         }];
    }
}

#endif

- (void)showErrorAlertWithMessage:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
@end
