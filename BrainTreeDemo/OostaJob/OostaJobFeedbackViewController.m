//
//  OostaJobFeedbackViewController.m
//  OostaJob
//
//  Created by Apple on 17/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobFeedbackViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "OostaJobCustomerJobsViewController.h"
@interface OostaJobFeedbackViewController ()<UITextViewDelegate,UITextFieldDelegate,EDStarRatingProtocol,MBProgressHUDDelegate>
{
    KLCPopup* PostFeedback;
    NSString * rateTime;
    NSString * ratePrice;
    NSString * rateQuality;
    NSString * rateOverall;
    NSMutableArray *arrJobTypes;
    NSString * CurrentTextView;
}
@end

@implementation OostaJobFeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self CreateBlurEffectOnBackgroundImage];
    [self setupStars];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneWithNumberPad)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:FONT_BOLD size:18.0], NSFontAttributeName,
                                        [UIColor whiteColor], NSForegroundColorAttributeName,
                                        nil]
                              forState:UIControlStateNormal];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           doneButton,
                           nil];
    [numberToolbar sizeToFit];
    _txtMassage.inputAccessoryView = numberToolbar;
    _txtMassagePopup.inputAccessoryView = numberToolbar;
    
}
-(void)doneWithNumberPad
{
    if ([CurrentTextView isEqualToString:@"Message"])
    {
        if( self.txtMassage.text.length == 0)
        {
            self.txtMassage.textColor = [UIColor lightGrayColor];
            self.txtMassage.text = @"Write down some feedback...";
        }
        [self.txtMassage resignFirstResponder];
        CGPoint point = CGPointMake(0, 0);
        [self.scrollview setContentOffset:point animated:YES];
        
    }
    else
    {
        if( self.txtMassagePopup.text.length == 0)
        {
            self.txtMassagePopup.textColor = [UIColor lightGrayColor];
            self.txtMassagePopup.text = @"Write down some feedback...";
            
        }
        [PostFeedback dismissPresentingPopup];
        [self.txtMassagePopup resignFirstResponder];
    }
    
}
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.lblName.font=[UIFont fontWithName:FONT_BOLD size:35.0f];
        self.lblWork.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.lblAddress.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblRatings.font=[UIFont fontWithName:FONT_BOLD size:25];
        self.lblPrice.font=[UIFont fontWithName:FONT_BOLD size:25];
        self.lblQuality.font=[UIFont fontWithName:FONT_BOLD size:25];
        self.lblTime.font=[UIFont fontWithName:FONT_BOLD size:25];
        self.lblOverallSatisfaction.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblComplaintRegister.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        [self.PostBtn.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        self.PostBtn.layer.cornerRadius=5.0;
        self.PostBtn.layer.masksToBounds=YES;
        //Text view Placeholder name
        self.txtMassage.text = @"Write down some feedback...";
        self.txtMassage.textColor = [UIColor lightGrayColor];
        self.txtMassage.delegate = self;
        self.txtMassage.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        self.txtMassagePopup.text = @"Write down some feedback...";
        self.txtMassage.textColor = [UIColor lightGrayColor];
        self.txtMassagePopup.delegate = self;
        self.txtMassagePopup.font=[UIFont fontWithName:FONT_THIN size:25.0f];
        self.txtMassagePopup.layer.borderWidth=1.0;
        self.txtMassagePopup.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
        self.lblTypeYourComplaints .font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:24.0f]];
        [self.btnPost.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:24.0f]];
        _PopupView.layer.cornerRadius = 5.0;
        
    }
    else
    {
        self.lblName.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        self.lblWork.font=[UIFont fontWithName:FONT_THIN size:14.0f];
        self.lblAddress.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
        self.lblRatings.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
        self.lblPrice.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblQuality.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblTime.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblOverallSatisfaction.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lblComplaintRegister.font=[UIFont fontWithName:FONT_BOLD size:13.0f];
        [self.PostBtn.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        self.PostBtn.layer.cornerRadius=5.0;
        self.PostBtn.layer.masksToBounds=YES;
        //Text view Placeholder name
        self.txtMassage.text = @"Write down some feedback...";
        self.txtMassage.textColor = [UIColor lightGrayColor];
        self.txtMassage.delegate = self;
        self.txtMassage.font=[UIFont fontWithName:FONT_THIN size:15.0f];
        self.txtMassagePopup.text = @"Write down some feedback...";
        self.txtMassage.textColor = [UIColor lightGrayColor];
        self.self.txtMassagePopup.delegate = self;
        self.txtMassagePopup.font=[UIFont fontWithName:FONT_THIN size:15.0f];
        self.txtMassagePopup.layer.borderWidth=1.0;
        self.txtMassagePopup.layer.borderColor=[UIColor lightGrayColor].CGColor;
        
        self.lblTypeYourComplaints .font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:18.0f]];
        [self.btnPost.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:18.0f]];
        _PopupView.layer.cornerRadius =5.0;
        
    }
    [self setUpAllvalues];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"array details :%@",_strJobId);

    NSLog(@"array details :%@",_arrDetails);
}
-(void)setUpAllvalues
{
    self.imgProfile.imageURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_arrDetails valueForKeyPath:@"profilePhoto"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    self.lblName.text = [NSString stringWithFormat:@"%@",[_arrDetails valueForKeyPath:@"username"]];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:[[NSString stringWithFormat:@"%@",[_arrDetails valueForKeyPath:@"Lat"]] floatValue] longitude:[[NSString stringWithFormat:@"%@",[_arrDetails valueForKeyPath:@"Lng"]] floatValue]];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error){
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
         NSLog(@"locality %@",placemark.locality);
         NSLog(@"postalCode %@",placemark.postalCode);
         _lblAddress.text = [NSString stringWithFormat:@"%@, %@",placemark.locality,placemark.postalCode];
     }];
    self.imgProfile.layer.cornerRadius = _imgProfile.frame.size.width/2;
    self.imgProfile.layer.masksToBounds = YES;
    self.imgProfile.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.imgProfile.layer.borderWidth = 3.0;
    self.viewImage.layer.cornerRadius = _viewImage.frame.size.width/2;
    self.viewImage.layer.masksToBounds = YES;
    
    NSMutableArray * arrSelectedExpertise = [[NSMutableArray alloc]init];
    arrSelectedExpertise = [[[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.expertiseAT"]] stringByReplacingOccurrencesOfString:@" " withString:@""] componentsSeparatedByString:@","] mutableCopy];
    NSMutableArray *arrListOfJobTypes = [[NSMutableArray alloc]init];
    if (arrJobTypes.count==0)
    {
        [self getJobTypeandCompletionHandler:^(bool resultJobType) {
            if (resultJobType==YES)
            {
                for (int i=0; i<arrJobTypes.count; i++)
                {
                    if ([arrSelectedExpertise containsObject:[NSString stringWithFormat:@"%@",[[arrJobTypes objectAtIndex:i] valueForKeyPath:@"jobtype_id"]]])
                    {
                        [arrListOfJobTypes addObject:[NSString stringWithFormat:@"%@",[[arrJobTypes objectAtIndex:i] valueForKeyPath:@"job_name"]]];
                    }
                }
                
                NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
                NSString *strExpertiseAt =[NSString stringWithFormat:@"%@",arrListOfJobTypes];
                strExpertiseAt = [[strExpertiseAt componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
                NSString *trimmedStringAt = [strExpertiseAt stringByTrimmingCharactersInSet:
                                             [NSCharacterSet whitespaceCharacterSet]];
                trimmedStringAt = [trimmedStringAt stringByReplacingOccurrencesOfString:@",    " withString:@","];
                _lblWork.text = trimmedStringAt;
            }
            else
            {
                
            }
        }];
    }
    else
    {
        for (int i=0; i<arrJobTypes.count; i++)
        {
            if ([arrSelectedExpertise containsObject:[NSString stringWithFormat:@"%@",[[arrJobTypes objectAtIndex:i] valueForKeyPath:@"jobtype_id"]]])
            {
                [arrListOfJobTypes addObject:[NSString stringWithFormat:@"%@",[[arrJobTypes objectAtIndex:i] valueForKeyPath:@"job_name"]]];
            }
        }
        
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
        NSString *strExpertiseAt =[NSString stringWithFormat:@"%@",arrListOfJobTypes];
        strExpertiseAt = [[strExpertiseAt componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
        NSString *trimmedStringAt = [strExpertiseAt stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceCharacterSet]];
        trimmedStringAt = [trimmedStringAt stringByReplacingOccurrencesOfString:@",    " withString:@","];
        _lblWork.text = trimmedStringAt;
    }
    
}
-(void)setupStars
{
    _ratingTime.starImage = [[UIImage imageNamed:@"star-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ratingTime.starHighlightedImage = [[UIImage imageNamed:@"star-highlighted-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ratingTime.maxRating = 5.0;
    _ratingTime.delegate = self;
    _ratingTime.horizontalMargin = 1.0;
    _ratingTime.editable=YES;
    _ratingTime.rating= 0;
    _ratingTime.displayMode=EDStarRatingDisplayHalf;
    [_ratingTime  setNeedsDisplay];
    _ratingTime.tintColor = [UIColor whiteColor];
    
    _ratingPrice.starImage = [[UIImage imageNamed:@"star-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ratingPrice.starHighlightedImage = [[UIImage imageNamed:@"star-highlighted-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ratingPrice.maxRating = 5.0;
    _ratingPrice.delegate = self;
    _ratingPrice.horizontalMargin = 1.0;
    _ratingPrice.editable=YES;
    _ratingPrice.rating= 0;
    _ratingPrice.displayMode=EDStarRatingDisplayHalf;
    [_ratingPrice  setNeedsDisplay];
    _ratingPrice.tintColor = [UIColor whiteColor];
    
    
    _ratingQuality.starImage = [[UIImage imageNamed:@"star-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ratingQuality.starHighlightedImage = [[UIImage imageNamed:@"star-highlighted-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ratingQuality.maxRating = 5.0;
    _ratingQuality.delegate = self;
    _ratingQuality.horizontalMargin = 1.0;
    _ratingQuality.editable=YES;
    _ratingQuality.rating= 0;
    _ratingQuality.displayMode=EDStarRatingDisplayHalf;
    [_ratingQuality  setNeedsDisplay];
    _ratingQuality.tintColor = [UIColor whiteColor];
    
    
    _ratingOverall.starImage = [[UIImage imageNamed:@"star-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ratingOverall.starHighlightedImage = [[UIImage imageNamed:@"star-highlighted-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ratingOverall.maxRating = 5.0;
    _ratingOverall.delegate = self;
    _ratingOverall.horizontalMargin = 1.0;
    _ratingOverall.editable=YES;
    _ratingOverall.rating= 0;
    _ratingOverall.displayMode=EDStarRatingDisplayHalf;
    [_ratingOverall  setNeedsDisplay];
    _ratingOverall.tintColor = [UIColor whiteColor];
    
}
-(void)CreateBlurEffectOnBackgroundImage
{
    self.imgBlur.image = _imageBackGround;
    [self.imgBlur assignBlur];
}


#pragma mark -TextView Delegates

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView == _txtMassage)
    {
        CurrentTextView = @"Message";
        self.txtMassage.text = @"";
        self.txtMassage.textColor = [UIColor grayColor];
        CGPoint scrollPoint = CGPointMake(0.0,textView.frame.origin.y);
        [self.scrollview setContentOffset:scrollPoint animated:YES];
    }
    else
    {
        CurrentTextView = @"Complaint";
        self.txtMassagePopup.text = @"";
        self.txtMassagePopup.textColor = [UIColor  grayColor];
    }
    
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if (textView==_txtMassage)
    {
        if( self.txtMassage.text.length == 0)
        {
            self.txtMassage.textColor = [UIColor lightGrayColor];
            self.txtMassage.text = @"Write down some feedback...";
            [ self.txtMassage resignFirstResponder];
        }
    }
    else
    {
        if( self.txtMassagePopup.text.length == 0)
        {
            self.txtMassagePopup.textColor = [UIColor lightGrayColor];
            self.txtMassagePopup.text = @"Write down some feedback...";
            [ self.txtMassagePopup resignFirstResponder];
        }
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (IBAction)BackBtnTouched:(id)sender
{
   // [self.navigationController popViewControllerAnimated:YES];
    if ([self.check isEqualToString:@"from cancel"]) {
        [self popoup];
        
    }
    else
    {
        [self gotoJobs];
    }}

-(IBAction)PostBtnTouched:(id)sender;
{
    if (_txtMassage.text.length==0||[_txtMassage.text isEqualToString:@"Write down some feedback..."])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = @"Write down some feedback";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
        
    }
    else
    {
    [self PostFeedBackContractorandCompletionHandler:^(bool resultPostFeedBack) {
        if (resultPostFeedBack==YES)
        {
            NSLog(@"Sucess");
            if ([self.check isEqualToString:@"from cancel"]) {
                [self popoup];

            }
            else
            {
                [self gotoJobs];
            }

            
            
        }
    }];
    }
}

-(IBAction)ClickHereBtnTouched:(id)sender;
{
    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutAboveCenter);
    PostFeedback = [KLCPopup popupWithContentView:self.PopupView
                                         showType:KLCPopupShowTypeBounceInFromTop
                                      dismissType:KLCPopupDismissTypeBounceOutToBottom
                                         maskType:KLCPopupMaskTypeDimmed
                         dismissOnBackgroundTouch:NO
                            dismissOnContentTouch:NO];
    [_txtMassagePopup becomeFirstResponder];
    [PostFeedback showWithLayout:layout];
}

-(void)starsSelectionChanged:(EDStarRating *)control rating:(float)rating
{
    NSString *ratingString = [NSString stringWithFormat:@"%.1f", rating];
    if ([control isEqual:_ratingTime])
    {
        rateTime = ratingString;
    }
    else if ([control isEqual:_ratingPrice])
    {
        ratePrice = ratingString;
    }
    else if ([control isEqual:_ratingQuality])
    {
        rateQuality = ratingString;
    }
    else if ([control isEqual:_ratingOverall])
    {
        rateOverall = ratingString;
    }
    
}

-(void)PostFeedBackContractorandCompletionHandler:(void (^)(bool resultPostFeedBack))completionHandler
{
    if (_txtMassage.text.length==0||[_txtMassage.text isEqualToString:@"Write down some feedback..."])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = @"Write down some feedback";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
        completionHandler(true);
        
    }
    else
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            hud.mode = MBProgressHUDModeText;
            hud.color=[UIColor whiteColor];
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            
            hud.labelText = INTERNET_ERR;
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
        else
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.color=[UIColor whiteColor];
            hud.labelText = @"Posting Feedback...";
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            hud.margin = 010.f;
            hud.yOffset = 20.f;
            [hud show:YES];
            
            
            NSString * key1 =@"userID";
            NSString * obj1 =[_arrDetails valueForKeyPath:@"contractorID"];
            
            NSString * key2 =@"jobpostID";
            NSString * obj2 =_strJobId;
            
            NSString * key3 =@"ratingPrice";
            NSString * obj3 =ratePrice?ratePrice:@"";
            
            NSString * key4 =@"ratingTime";
            NSString * obj4 =rateTime?rateTime:@"";
            
            NSString * key5 =@"ratingQuality";
            NSString * obj5 =rateQuality?rateQuality:@"";
            
            NSString * key6 =@"ratingOverall";
            NSString * obj6 =rateOverall?rateOverall:@"";
            
            NSString * key7 =@"comment";
            NSString * obj7 =_txtMassage.text;
            
            NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                           initWithObjects:@[obj1,obj2,obj3,obj4,obj5,obj6,obj7]
                                           forKeys:@[key1,key2,key3,key4,key5,key6,key7]];
            
            NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
            NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
            NSLog(@"DATA %@",jsonString);
            NSMutableData *body = [NSMutableData data];
            
            [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@addrating",BaseURL]]];
            NSLog(@"addrating URL:%@",request.URL);
            [request setHTTPBody:body];
            [request setHTTPMethod:@"POST"];
            
            [NSURLConnection sendAsynchronousRequest: request
                                               queue: [NSOperationQueue mainQueue]
                                   completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if (error || !data)
                 {
                     
                     NSLog(@"Server Error : %@", error);
                     
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(false);
                 }
                 else
                 {
                     NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                            options:0
                                                                              error:NULL];
                     NSLog(@"Result %@",result);
                     if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                     {
                         
                         
                         
                         [hud hide:YES];
                         MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                         [self.navigationController.view addSubview:hud];
                         
                         hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                         hud.mode = MBProgressHUDModeCustomView;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                         hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                         hud.delegate = self;
                         hud.labelText = @"Rated Successfully";
                         [hud show:YES];
                         [hud hide:YES afterDelay:2];
                         completionHandler(true);

                        // [self gotoJobs];
                         
                     }
                     else
                     {
                         NSLog(@"%@",SERVER_ERR);
                         
                         [hud hide:YES];
                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                         
                         hud.mode = MBProgressHUDModeText;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=kMBProgressHUDLabelColor;
                         hud.backgroundColor=kMBProgressHUDBackgroundColor;
                         
                         hud.labelText = SERVER_ERR;
                         hud.margin = 10.f;
                         hud.yOffset = 20.f;
                         hud.removeFromSuperViewOnHide = YES;
                         [hud hide:YES afterDelay:2];
                         completionHandler(false);

                     }
                     
                 }
             }];
        }
    }
    
}
-(void)PostComplaintContractorandCompletionHandler:(void (^)(bool resultPostComplaint))completionHandler
{
    if (_txtMassagePopup.text.length==0||[_txtMassagePopup.text isEqualToString:@"Write down some feedback..."])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.PopupView animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.labelText = @"Write down some feedback";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
        completionHandler(true);
        
    }
    else
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            hud.mode = MBProgressHUDModeText;
            hud.color=[UIColor whiteColor];
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            
            hud.labelText = INTERNET_ERR;
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
        else
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.PopupView animated:YES];
            hud.color=[UIColor whiteColor];
            hud.labelText = @"Posting Complaint...";
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            hud.margin = 010.f;
            hud.yOffset = 20.f;
            [hud show:YES];
            
            
            NSString * key1 =@"userID";
            NSString * obj1 =[_arrDetails valueForKeyPath:@"contractorID"];
            
            NSString * key2 =@"jobpostID";
            NSString * obj2 =_strJobId;
            
            NSString * key3 =@"comment";
            NSString * obj3 =_txtMassagePopup.text;
            
            NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                           initWithObjects:@[obj1,obj2,obj3]
                                           forKeys:@[key1,key2,key3]];
            
            NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
            NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
            NSLog(@"DATA %@",jsonString);
            NSMutableData *body = [NSMutableData data];
            
            [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@addcomplients",BaseURL]]];
            NSLog(@"addcomplients URL:%@",request.URL);
            [request setHTTPBody:body];
            [request setHTTPMethod:@"POST"];
            
            [NSURLConnection sendAsynchronousRequest: request
                                               queue: [NSOperationQueue mainQueue]
                                   completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if (error || !data)
                 {
                     
                     NSLog(@"Server Error : %@", error);
                     
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.PopupView animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                     completionHandler(true);
                 }
                 else
                 {
                     NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                            options:0
                                                                              error:NULL];
                     NSLog(@"Result %@",result);
                     if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                     {
                         
                         
                         [self btnCancelPressed:self];
                         [hud hide:YES];
                         MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                         [self.navigationController.view addSubview:hud];
                         
                         hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                         hud.mode = MBProgressHUDModeCustomView;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                         hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                         hud.delegate = self;
                         hud.labelText = @"Complaint Registered";
                         [hud show:YES];
                         [hud hide:YES afterDelay:2];
                         [self gotoJobs];
                         
                     }
                     else
                     {
                         NSLog(@"%@",SERVER_ERR);
                         
                         [hud hide:YES];
                         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.PopupView animated:YES];
                         
                         hud.mode = MBProgressHUDModeText;
                         hud.color=[UIColor whiteColor];
                         hud.labelColor=kMBProgressHUDLabelColor;
                         hud.backgroundColor=kMBProgressHUDBackgroundColor;
                         
                         hud.labelText = SERVER_ERR;
                         hud.margin = 10.f;
                         hud.yOffset = 20.f;
                         hud.removeFromSuperViewOnHide = YES;
                         [hud hide:YES afterDelay:2];
                         
                     }
                     
                     completionHandler(true);
                 }
             }];
        }
    }
    
}

#pragma mark Job types
-(void)getJobTypeandCompletionHandler:(void (^)(bool resultJobType))completionHandler
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getjobtype",BaseURL]]];
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             completionHandler(false);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 
                 arrJobTypes=[[NSMutableArray alloc]init];
                 arrJobTypes=[result valueForKeyPath:@"JobtypeDetails"];
                 
                 completionHandler(true);
                 
             }
             else
             {
                 completionHandler(false);
             }
         }
     }];
    
}
- (void) scrollVievEditingFinished:(UITextField*)textField
{
    CGPoint point = CGPointMake(0, 0);
    [self.scrollview setContentOffset:point animated:YES];
}
- (IBAction)btnCancelPressed:(id)sender
{
    if( self.txtMassagePopup.text.length == 0)
    {
        self.txtMassagePopup.textColor = [UIColor lightGrayColor];
        self.txtMassagePopup.text = @"Write down some feedback...";
        
    }
    [PostFeedback dismissPresentingPopup];
    [self.txtMassagePopup resignFirstResponder];
}
- (IBAction)btnPostPressed:(id)sender
{
    [self PostComplaintContractorandCompletionHandler:^(bool resultPostComplaint) {
        if (resultPostComplaint)
        {
            NSLog(@"Completed");
        }
    }];
}

-(void)gotoJobs
{
    SWRevealViewController *revealController = self.revealViewController;
    UIViewController *newFrontController = nil;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController IPad" bundle:nil];
        JobObj.redirectPage = @"Closed";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
    }
    else if (IS_IPHONE5)
    {
        OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
        JobObj.redirectPage = @"Closed";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
    }
    else
    {
        OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
        JobObj.redirectPage = @"Closed";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
    }
    [revealController pushFrontViewController:newFrontController animated:YES];
}
-(IBAction)HomeBtnTouched:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

# pragma mark RepostApi
-(void)callPostJobFortheContent:(NSMutableArray *)Repostarray andCompletionHandler:(void (^)(bool result))completionHandler
{
    
    
    NSString * key1 =@"userID";
    NSString * obj1 =[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];
    
    NSString * key2 =@"jobtypeID";
    NSString * obj2 =[Repostarray valueForKeyPath:@"jobtypeID"];
    
    NSString * key3 =@"jobsubtypeID";
    NSString * obj3 =[Repostarray valueForKeyPath:@"jobsubtypeID"];
    
    NSString * key4 =@"Address";
    NSString * obj4 =[Repostarray valueForKeyPath:@"Address"];
    
    NSString * key5 =@"zipcode";
    NSString * obj5 =[Repostarray valueForKeyPath:@"zipcode"];
    
    NSString * key6 =@"lat";
    NSString * obj6 =[Repostarray valueForKeyPath:@"lat"];
    
    NSString * key7 =@"lng";
    NSString * obj7 =[Repostarray valueForKeyPath:@"lng"];
    int i;
    NSString *mediacontent;
    NSMutableArray *mediaarray = [[NSMutableArray alloc]init];
    NSLog(@"media check :%@",[[[Repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:0] valueForKeyPath:@"mediaType"]);
    NSLog(@"media check :%@",[[[Repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:0] valueForKeyPath:@"mediaContent"]);
    NSLog(@"repost count :%@",Repostarray);
    for (i=0; i<[Repostarray count] ;i++) {
        NSMutableDictionary *dictM=[[NSMutableDictionary alloc]init];
        NSLog(@"check index :%lu",[[Repostarray valueForKeyPath:@"jobmedialist"] count]);
        
        
        if(i >= [[Repostarray valueForKeyPath:@"jobmedialist"] count]){
        }
        else{
            if ([[Repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:i]!=0)
            {
                if ([[[[Repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:i] valueForKeyPath:@"mediaType"]  isEqual: @"1"]) {
                    [dictM setObject:@"1" forKey:@"mediaType"];
                    mediacontent =[[[Repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:i] valueForKeyPath:@"mediaContent"];
                    
                    mediacontent  = [mediacontent stringByReplacingOccurrencesOfString:@"public/assets/uploads/jobpost/" withString:@""];
                    
                    [dictM setObject:[NSString stringWithFormat:@"%@",mediacontent] forKey:@"mediaContent"];
                }
                
                if ([[[[Repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:i] valueForKeyPath:@"mediaType"]  isEqual: @"2"]) {
                    [dictM setObject:@"2" forKey:@"mediaType"];
                    mediacontent =[[[Repostarray valueForKeyPath:@"jobmedialist"] objectAtIndex:i] valueForKeyPath:@"mediaContent"];
                    
                    mediacontent  = [mediacontent stringByReplacingOccurrencesOfString:@"public/assets/uploads/jobpost/" withString:@""];
                    
                    [dictM setObject:[NSString stringWithFormat:@"%@",mediacontent] forKey:@"mediaContent"];
                }
            }
            [mediaarray addObject:dictM];
            
        }
        
    }
    NSLog(@"media check :%@",mediaarray);
    NSData * JsonDataMedia =[NSJSONSerialization dataWithJSONObject:[mediaarray valueForKeyPath:@"jobmedialist"] options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonStringMedia= [[NSString alloc] initWithData:JsonDataMedia encoding:NSUTF8StringEncoding];
    NSData *dataMedia = [jsonStringMedia dataUsingEncoding:NSUTF8StringEncoding];
    id jsonMedia = [NSJSONSerialization JSONObjectWithData:dataMedia options:0 error:nil];
    // NSString *myString = @"public/assets/uploads/jobpost/";
    
    
    NSString *  key8 =@"media";
    NSString *  obj8 =mediaarray;
    
    
    
    NSString * key9 =@"answerkey";
    NSString * obj9 =[Repostarray valueForKeyPath:@"jobanwserlist"];
    
    NSString * key10 =@"description";
    //    NSString * obj10 =[dictSelected valueForKeyPath:[NSString stringWithFormat:@"%u",(arrCharacteristics.count-1)]];
    NSString * obj10 = [Repostarray valueForKeyPath:@"description"];
    
    NSDictionary *jsonDictionary =[[NSDictionary alloc]
                                   initWithObjects:@[obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10]
                                   forKeys:@[key1,key2,key3,key4,key5,key6,key7,key8,key9,key10]];
    
    NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
    NSLog(@"DATA %@",jsonString);
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@postjob",BaseURL]]];
    NSLog(@"PostJob URL:%@",request.URL);
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
             hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             completionHandler(false);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
             {
                 
                 MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                 [self.navigationController.view addSubview:hud];
                 
                 hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                 hud.mode = MBProgressHUDModeCustomView;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                 hud.delegate = self;
                 hud.labelText = @"Done";
                 [hud show:YES];
                 [hud hide:YES afterDelay:2];
                 
                 //                 KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutCenter);
                 //                 popupViewGreat = [KLCPopup popupWithContentView:_PopupContent
                 //                                                        showType:KLCPopupShowTypeBounceInFromTop
                 //                                                     dismissType:KLCPopupDismissTypeBounceOutToBottom
                 //                                                        maskType:KLCPopupMaskTypeDimmed
                 //                                        dismissOnBackgroundTouch:NO
                 //                                           dismissOnContentTouch:NO];
                 //   [popupViewGreat showWithLayout:layout];
                 
                 completionHandler(true);
             }
             else
             {
                 
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 completionHandler(false);
             }
         }
     }];
}

-(void)popoup{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOstaJob"
                                                        message:@"Do you want to Repost the job?"
                                                       delegate:self
                                              cancelButtonTitle:@"Repost job"
                                              otherButtonTitles:@"Dismiss",nil];
    
        [alert show];
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self gotoJobs];

    }
    
    if (buttonIndex == 0)
    {
       
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            hud.mode = MBProgressHUDModeText;
            hud.color=[UIColor whiteColor];
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            hud.labelText = INTERNET_ERR;
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
        else
        {
          
            
            [self callPostJobFortheContent:_ratedarray andCompletionHandler:^(bool resultCanceltheJob)
             {
                 if (resultCanceltheJob == YES)
                 {
                     [self gotoJobs];
                     NSLog(@"Job posted");
                 }
                 
             }];
        }
    }
}
@end
