//
//  OostaJobZipCodeViewController.h
//  OostaJob
//
//  Created by Armor on 17/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OostaJobZipCodeViewController : UIViewController
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (weak, nonatomic) IBOutlet UILabel *lblZipcode;
@property (weak, nonatomic) IBOutlet UILabel *lblShot;
@property (weak, nonatomic) IBOutlet UILabel *lblNotify;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (strong, nonatomic) NSString *strZipcode;
- (IBAction)btnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end
