//
//  OostaJobRegisterViewController.h
//  OostaJob
//
//  Created by Apple on 20/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankDetailcontroller.h"
@interface OostaJobRegisterViewController : UIViewController
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (weak, nonatomic) IBOutlet UIButton *btnINeedAService;
@property (weak, nonatomic) IBOutlet UIButton *btnIamContractor;
- (IBAction)btnCustomerTapped:(id)sender;
- (IBAction)btnContractorTapped:(id)sender;
- (IBAction)btnBackTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewCustomer;
@property (weak, nonatomic) IBOutlet UIView *viewContractor;
@property (strong, nonatomic) NSString *strPage;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *viewController;
@end
