//
//  OostaJobFeedbackViewController.h
//  OostaJob
//
//  Created by Apple on 17/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KLCPopup.h"
#import "AsyncImageView.h"
#import "EDStarRating.h"
@interface OostaJobFeedbackViewController : UIViewController

//image view
@property (strong, nonatomic) IBOutlet AsyncImageView *imgProfile;
//Lable
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblWork;
@property (strong, nonatomic) IBOutlet UILabel *lblRatings;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblQuality;
@property (strong, nonatomic) IBOutlet UILabel *lblOverallSatisfaction;
@property (strong, nonatomic) IBOutlet UILabel *lblComplaintRegister;
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;

//Button
@property (strong, nonatomic) IBOutlet UIButton *PostBtn;
@property (strong, nonatomic) IBOutlet UIButton *ClickhereBtn;

//text view
@property (strong, nonatomic) IBOutlet UITextView *txtMassage;
@property (strong, nonatomic) IBOutlet UITextView *txtMassagePopup;
//UIView
@property (strong, nonatomic) IBOutlet UIView *PopupView;

@property (strong, nonatomic) UIImage *imageBackGround;
@property (strong, nonatomic) NSMutableArray *arrDetails;
@property (strong, nonatomic) NSString * strJobId;
//Actions
- (IBAction)BackBtnTouched:(id)sender;
-(IBAction)HomeBtnTouched:(id)sender;
-(IBAction)PostBtnTouched:(id)sender;
-(IBAction)ClickHereBtnTouched:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewImage;

@property (weak, nonatomic) IBOutlet EDStarRating *ratingTime;
@property (weak, nonatomic) IBOutlet EDStarRating *ratingPrice;
@property (weak, nonatomic) IBOutlet EDStarRating *ratingQuality;
@property (weak, nonatomic) IBOutlet EDStarRating *ratingOverall;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UILabel *lblTypeYourComplaints;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)btnCancelPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnPost;
- (IBAction)btnPostPressed:(id)sender;

@property (weak,nonatomic) NSMutableArray *ratedarray;
@property (weak,nonatomic) NSString *check;

@end
