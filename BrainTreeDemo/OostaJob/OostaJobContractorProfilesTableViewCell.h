//
//  OostaJobContractorProfilesTableViewCell.h
//  OostaJob
//
//  Created by Armor on 18/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OostaJobContractorProfilesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewContentAll;
@property (weak, nonatomic) IBOutlet UILabel *lblTblCustName;
@property (weak, nonatomic) IBOutlet UILabel *lblTblCity;
@property (weak, nonatomic) IBOutlet UILabel *lblTblJobType;
@property (weak, nonatomic) IBOutlet UILabel *lblTbljobReview;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfPic;
@property (weak, nonatomic) IBOutlet UILabel *lblTblDate;
@end
