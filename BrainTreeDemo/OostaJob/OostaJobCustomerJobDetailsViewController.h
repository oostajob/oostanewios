//
//  OostaJobPaintingsViewController.h
//  OostaJob
//
//  Created by Armor on 18/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALMoviePlayerController.h"
#import "DWTagList.h"
#import "HPTextViewInternal.h"
#import "HPGrowingTextView.h"
#import "CKCalendarView.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import "OostaJobContractorBillingViewController.h"
@protocol OostaJobCustomerJobDetailsViewControllerDelegate <NSObject>
@optional
- (void)getreloadto:(NSString *)strPage;
@end

@interface OostaJobCustomerJobDetailsViewController : UIViewController<UIScrollViewDelegate,UITextViewDelegate,UITextFieldDelegate,DWTagListDelegate,HPGrowingTextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (weak, nonatomic) IBOutlet UILabel *lblHouseCleaning;
@property (weak, nonatomic) IBOutlet UILabel *lblOneStory;
@property (weak, nonatomic) IBOutlet UILabel *lblSqft;
@property (weak, nonatomic) IBOutlet UILabel *lbl2Bed;
@property (weak, nonatomic) IBOutlet UILabel *lbl2Bath;
@property (weak, nonatomic) IBOutlet UILabel *lblMaterials;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UILabel *lblJobHeading;
@property (weak, nonatomic) IBOutlet UILabel *lblJobTitle;
- (IBAction)backButtonForCalendarSkip:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewForCalendarSkip;

@property (weak, nonatomic) IBOutlet UILabel *lblJobPinCode;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscussion;



@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewContent;
@property (weak, nonatomic) IBOutlet UIButton *btnUp;



@property (weak, nonatomic) IBOutlet UILabel *lblDescriptionl;
@property (strong, nonatomic)NSString *strUserId;
@property (strong, nonatomic)NSString *strJobpostID;
@property (strong, nonatomic)NSString *ContractorID;

@property (strong, nonatomic)NSString *strJobDiscID;
@property (strong, nonatomic)NSString *strQuestionName;
@property (strong, nonatomic)NSString *strGoto;

@property (strong, nonatomic)NSMutableArray *resultAry;
@property (strong, nonatomic)NSMutableArray *resultDiscussionAry;
@property (strong, nonatomic)NSMutableArray *resultJobDiscussionAry;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic)NSMutableArray *arrSelected;
@property (weak, nonatomic) IBOutlet AsyncImageView *imgThumbNail;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlay;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
- (IBAction)btnPlayTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *viewMedia;
@property (nonatomic, strong) ALMoviePlayerController *moviePlayer;
@property (nonatomic) CGRect defaultFrame;

@property (weak, nonatomic) IBOutlet UIView *viewMediaPreview;
@property (weak, nonatomic) IBOutlet UIImageView *imgJobIcon;
@property (weak, nonatomic) IBOutlet DWTagList *tagList;

@property (weak, nonatomic) IBOutlet UIView *viewDetails;
@property (weak, nonatomic) IBOutlet UIView *viewFullContent;
@property (weak, nonatomic) IBOutlet UIScrollView *scrlViewFullContent;
@property (weak, nonatomic) IBOutlet UIView *viewNotification;

@property (weak, nonatomic) IBOutlet UIView *viewDiscussion;


@property (weak, nonatomic) IBOutlet UITableView *tblChatMsgs;
@property (weak, nonatomic) IBOutlet UIButton *btnHomeTapped;
- (IBAction)btnHomeTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgBiddingMember;
@property (weak, nonatomic) IBOutlet UILabel *lblBiddingMemberCount;
@property (weak, nonatomic) IBOutlet UIView *viewBidding;
@property (weak, nonatomic) IBOutlet UIButton *btnBidding;
- (IBAction)btnBiddingTapped:(id)sender;
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, retain) NSArray *list;
@property (strong, nonatomic) UIView *dropDown;
@property (weak, nonatomic) IBOutlet UIView *viewBiddingMessages;
@property (strong, nonatomic) IBOutlet UIView *viewMsgContr;
- (IBAction)btnContractorPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblMesseges;
@property (weak, nonatomic) IBOutlet UILabel *lblContractors;
@property (weak, nonatomic) IBOutlet UIView *viewUnderLine;
@property (weak, nonatomic) IBOutlet UIView *viewMessageContent;
@property (weak, nonatomic) IBOutlet UIScrollView *scrlMsgContractor;

@property (weak, nonatomic) IBOutlet UIButton *btnUpCon;
@property (weak, nonatomic) IBOutlet UITableView *tblContractors;
//Calendar
@property (strong, nonatomic) IBOutlet UIView *viewCalander;
@property (weak, nonatomic) IBOutlet UIView *viewContentCalendar;
@property (weak, nonatomic) IBOutlet CKCalendarView *calendar;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedDate;
@property (weak, nonatomic) IBOutlet UIButton *btnMeet;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UIView *viewOtherContents;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerViewAvailableTimings;
- (IBAction)btnMeetUpPressed:(id)sender;
- (IBAction)btnSkipPressed:(id)sender;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property (weak, nonatomic) IBOutlet UIScrollView *scrlCalendar;
- (IBAction)btnCalendarClosedTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;
@property (strong, nonatomic) IBOutlet UIView *viewSelectedContractor;
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderSelected;
@property (weak, nonatomic) IBOutlet AsyncImageView *imgSelected;
@property (weak, nonatomic) IBOutlet UILabel *lblNameSelected;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressSelected;
@property (weak, nonatomic) IBOutlet UILabel *lblMeetingTimeSelected;
@property (weak, nonatomic) IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;

- (IBAction)btnEmailPressed:(id)sender;
- (IBAction)btnCallPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnRate;
- (IBAction)btnRatePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgCalendar;
@property (weak, nonatomic) IBOutlet UILabel *lblMeetingTime;
@property (weak, nonatomic) IBOutlet UIView *viewStars;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollCalendarView;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgStars;
@property (strong, nonatomic) IBOutlet UIView *viewtermsnconditions;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
- (IBAction)Onclickcheckbox:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewTermsAndConditions;
@property (weak, nonatomic) IBOutlet UIWebView *webViewTermsAndConditions;
- (IBAction)Onclickwrong:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsConditions;
- (IBAction)btnTermsandConditionTapped:(id)sender;

@property (nonatomic, assign)   id<OostaJobCustomerJobDetailsViewControllerDelegate> delegate;
@end
