//
//  OostaJobContractorInvitesTableViewCell.h
//  OostaJob
//
//  Created by Armor on 20/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DWTagList.h"
#import "MXLMediaView.h"
@interface OostaJobContractorTableViewCell : UITableViewCell<MXLMediaViewDelegate>
{
    NSMutableArray *arrCollectionViewData;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTblJobName;
@property (weak, nonatomic) IBOutlet UILabel *lblTblPin;



@property (weak, nonatomic) IBOutlet UIView *viewbids;
@property (weak, nonatomic) IBOutlet UIButton *btnTblBidding;
@property (strong, nonatomic) IBOutlet UIView *viewContractor;
@property (strong, nonatomic) IBOutlet UILabel *lblContratorCount;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (strong, nonatomic) IBOutlet UIView *viewMessage;
@property (strong, nonatomic) IBOutlet UILabel *lblMessageCount;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UIView *viewContent;


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet DWTagList *tagList;
-(void)CollectionData:(NSMutableArray *)arr andtheIndexPath:(NSIndexPath *)indexPath andTheTableView:(UITableView *)tableView;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblJobClosedTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;

@end
