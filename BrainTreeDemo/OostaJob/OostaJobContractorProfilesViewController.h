//
//  OostaJobContractorProfilesViewController.h
//  OostaJob
//
//  Created by Armor on 17/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OostaJobContractorProfilesViewControllerDelegate <NSObject>
@optional
- (void)getUpdate;
@end

@interface OostaJobContractorProfilesViewController : UIViewController<UIScrollViewDelegate>
{
    NSString *animationDirection;
    NSIndexPath *indexPathSel;
}
@property (weak, nonatomic) IBOutlet OostaJobBlurImageView *imgBlur;
@property (weak, nonatomic) IBOutlet AsyncImageView *ProfPic;
@property (weak, nonatomic) IBOutlet UIView*imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblRate;
@property (weak, nonatomic) IBOutlet UILabel *lblAddr;
@property (weak, nonatomic) IBOutlet UILabel *lblAddr1;
@property (weak, nonatomic) IBOutlet UILabel *lblMail;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblEarn;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewContent;
@property (weak, nonatomic) IBOutlet UIView *viewUnderLine;
@property (weak, nonatomic) IBOutlet UIButton *btnUp;
@property (weak, nonatomic) IBOutlet UIButton *btnAbout;
@property (weak, nonatomic) IBOutlet UIButton *btnReview;
@property (weak, nonatomic) IBOutlet UIButton *btnList;
;
- (IBAction)btnMenuTapped:(id)sender;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgStars;
@property (weak, nonatomic) IBOutlet UIView *viewImgStars;
@property (weak, nonatomic) IBOutlet UIView *viewTableContent;
@property (weak, nonatomic) IBOutlet UITableView *tblReview;
@property (strong, nonatomic) IBOutlet UIView *dropDown;
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, retain) NSArray *list;
@property (nonatomic, assign)   id<OostaJobContractorProfilesViewControllerDelegate> delegate;
@end
