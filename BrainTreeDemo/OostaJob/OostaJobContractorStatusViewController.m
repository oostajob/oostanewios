//
//  OostaJobContractorStatusViewController.m
//  OostaJob
//
//  Created by Armor on 25/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobContractorStatusViewController.h"
#import "OostaJobMapViewController.h"
#import "ContractorRearViewController.h"
#import "SWRevealViewController.h"
#import "RearViewController.h"
#import "OostaJobWelcomeViewController.h"
@interface OostaJobContractorStatusViewController ()<SWRevealViewControllerDelegate>
{
 int v;
}
@end

@implementation OostaJobContractorStatusViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSUserDefaults standardUserDefaults]setObject:@"PROFILE" forKey:@"SCREEN"];
    self.navigationController.navigationBarHidden=YES;
    v=0;

    [self setupUI];
    [self.imgBlur assignBlur];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        [self repeatCheck];
    }
    // Do any additional setup after loading the view from its nib.
}
-(void)repeatCheck
{
    [self CheckActivactionProfileandCompletionHandler:^(bool resultProfile)
     {
         if (resultProfile==YES)
         {
             [self performSelector:@selector(repeatCheck) withObject:nil
                        afterDelay:2.0];
         }
         else
         {
             NSLog(@"Go to next");
             
         }
     }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupUI
{
    if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.lblVerifiction.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lbl24hrs.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
    }else
    {
        self.lblVerifiction.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
        self.lbl24hrs.font=[UIFont fontWithName:FONT_BOLD size:15.0f];
    }
}

-(void)Gotobankdetails{
    BankDetailcontroller *emailVerificationObj=[[BankDetailcontroller alloc]initWithNibName:@"BankDetailcontroller" bundle:nil];
    emailVerificationObj.passage = @"fromregister";
    [self.navigationController pushViewController:emailVerificationObj animated:YES];
}
-(void)CheckActivactionProfileandCompletionHandler:(void (^)(bool resultProfile))completionHandler
{
    
    v++;
    NSString *strUserID=[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];
    NSLog(@"USERDETAILS:%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS"]);
    NSLog(@"strUserID:%@",strUserID);
    
    NSString *version=[NSString stringWithFormat:@"%d",v];
    
    
    
    NSString *str = [strUserID stringByAppendingString:@"?v="];
    NSString *strfinal = [str stringByAppendingString:version];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@verificationstatus/%@",BaseURL,strfinal]]];
   
    NSLog(@"verificationstatus:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             completionHandler(true);
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 NSLog(@"Result %@",result);
                 
                 if (![[result valueForKeyPath:@"ProfileStatus"] isEqualToString:@"Not Verified"])
                 {
                     completionHandler(false);
                     //[self gotoContractorJobList];
                     [self Gotobankdetails];
                     [[NSUserDefaults standardUserDefaults]setObject:@"REGISTEREDCONTRACTOR" forKey:@"SCREEN"];
                 }
                 else
                 {
                     completionHandler(true);
                 }
                 
             }
             else
             {
                 NSLog(@"Result %@",result);
                 completionHandler(true);
             }
             
         }
         
     }];
}

-(void)gotoContractorJobList
{
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];
    OostaJobMapViewController *ContractorfrontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        ContractorfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        ContractorfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController" bundle:nil];
    }
    else
    {
        ContractorfrontViewController = [[OostaJobMapViewController alloc] initWithNibName:@"OostaJobMapViewController" bundle:nil];
    }
    
    ContractorRearViewController *ContraRearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController iPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController" bundle:nil];
    }
    else
    {
        ContraRearViewController= [[ContractorRearViewController alloc] initWithNibName:@"ContractorRearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:ContractorfrontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:ContraRearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    self.window.rootViewController = self.viewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}


- (IBAction)backBtnTappedContract:(id)sender
{
    
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];
    
    OostaJobWelcomeViewController *frontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        frontViewController = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        frontViewController = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController" bundle:nil];
    }
    else
    {
        frontViewController = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController Small" bundle:nil];
    }
    
    RearViewController *rearViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        rearViewController= [[RearViewController alloc] initWithNibName:@"RearViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        rearViewController= [[RearViewController alloc] initWithNibName:@"RearViewController" bundle:nil];
    }
    else
    {
        rearViewController= [[RearViewController alloc] initWithNibName:@"RearViewController Small" bundle:nil];
    }
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    self.window.rootViewController = self.viewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];

}
@end
