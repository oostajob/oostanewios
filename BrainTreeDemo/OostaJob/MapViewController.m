//
//  MapViewController.m
//  
//
//  Created by Jian-Ye on 12-10-16.
//  Copyright (c) 2012年 Jian-Ye. All rights reserved.
//

#import "MapViewController.h"
#import "CallOutAnnotationVifew.h"
#import "JingDianMapCell.h"
#import "ImageCache.h"
#import <AVFoundation/AVFoundation.h>
#import "MXLMediaView.h"
#define span 4000

@interface MapViewController ()<MXLMediaViewDelegate>
{
    NSMutableArray *_annotationList;
    
    CalloutMapAnnotation *_calloutAnnotation;
	CalloutMapAnnotation *_previousdAnnotation;
    int currentIndex;
    NSMutableArray *_addedannotationList;
}
-(void)setAnnotionsWithList:(NSArray *)list;

@end

@implementation MapViewController

@synthesize mapView=_mapView;

@synthesize delegate;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    _annotationList = [[NSMutableArray alloc] init];
    [super viewDidLoad];
}

-(void)setAnnotionsWithList:(NSArray *)list
{
    NSLog(@"list:%@",list);
    
        for (int i=0; i<list.count; i++)
        {
            CLLocationDegrees latitude=[[NSString stringWithFormat:@"%@",[list[i] valueForKeyPath:@"lat"]] doubleValue];
            CLLocationDegrees longitude=[[NSString stringWithFormat:@"%@",[list[i] valueForKeyPath:@"lng"]] doubleValue];
            CLLocationCoordinate2D location=CLLocationCoordinate2DMake(latitude, longitude);
            
            MKCoordinateRegion region=MKCoordinateRegionMakeWithDistance(location,span ,span );
            MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:region];
            [_mapView setRegion:adjustedRegion animated:YES];
            
            BasicMapAnnotation *  annotation=[[BasicMapAnnotation alloc] initWithLatitude:latitude andLongitude:longitude andTheTitle:[NSString stringWithFormat:@"%d",i]];
            [_mapView addAnnotation:annotation];
        }
    
}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
	if ([view.annotation isKindOfClass:[BasicMapAnnotation class]]) {
        if (_calloutAnnotation.coordinate.latitude == view.annotation.coordinate.latitude&&
            _calloutAnnotation.coordinate.longitude == view.annotation.coordinate.longitude) {
            return;
        }
        if (_calloutAnnotation) {
            [mapView removeAnnotation:_calloutAnnotation];
            _calloutAnnotation = nil;
        }
        _calloutAnnotation = [[CalloutMapAnnotation alloc]
                               initWithLatitude:view.annotation.coordinate.latitude
                               andLongitude:view.annotation.coordinate.longitude] ;
        
        _addedannotationList=[[NSMutableArray alloc]init];
        if (view.annotation)
        {
            BasicMapAnnotation *myAnnotation  = (BasicMapAnnotation *)view.annotation;
            if((id <MKAnnotation>)myAnnotation !=self.mapView.userLocation)
            {
                @try {
                    NSLog(@"index:%@",myAnnotation.title);
                    currentIndex=[myAnnotation.title intValue];
                    
                    _addedannotationList=[_annotationList objectAtIndex:currentIndex];
                    NSLog(@"currentIndex:%d",currentIndex);
//                    [self mapView:self.mapView viewForAnnotation:myAnnotation];
                }
                @catch (NSException *exception) {
                    NSLog(@"exception:%@",exception);
                }
                @finally {
                    
                }
                
                
                //do rest of the work here with different pin respect to their tag
            }
        }
        
        
        [mapView addAnnotation:_calloutAnnotation];
        
        [mapView setCenterCoordinate:_calloutAnnotation.coordinate animated:YES];
	}
    else{
        if([delegate respondsToSelector:@selector(customMKMapViewDidSelectedWithInfo:)]){
            [delegate customMKMapViewDidSelectedWithInfo:@"Click to after you in this dry Diansha"];
        }
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if (_calloutAnnotation&& ![view isKindOfClass:[CallOutAnnotationVifew class]]) {
        if (_calloutAnnotation.coordinate.latitude == view.annotation.coordinate.latitude&&
            _calloutAnnotation.coordinate.longitude == view.annotation.coordinate.longitude) {
            [mapView removeAnnotation:_calloutAnnotation];
            _calloutAnnotation = nil;
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
	if ([annotation isKindOfClass:[CalloutMapAnnotation class]]) {
        if (_addedannotationList.count!=0)
        {
            
        
        CallOutAnnotationVifew *annotationView = (CallOutAnnotationVifew *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CalloutView"];
        
            annotationView = [[CallOutAnnotationVifew alloc] initWithAnnotation:annotation reuseIdentifier:@"CalloutView"] ;
            JingDianMapCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"JingDianMapCell" owner:self options:nil] objectAtIndex:0];
            
            cell.lblJobtitle.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
            cell.lblZipcode.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
            cell.lblTime.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
            cell.lblMiles.font=[UIFont fontWithName:FONT_BOLD size:14.0f];
            cell.lblDescription.font=[UIFont fontWithName:FONT_THIN size:14.0f];
            [cell.btnBidNow.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:13.0f]];
            cell.viewBidNow.layer.cornerRadius=12.0;
            
            cell.lblJobtitle.text=[NSString stringWithFormat:@"%@",[_addedannotationList valueForKeyPath:@"jobtypeName"]];
            cell.lblZipcode.text=[NSString stringWithFormat:@"%@",[_addedannotationList valueForKeyPath:@"zipcode"]];
            cell.lblDescription.text=[NSString stringWithFormat:@"%@",[_addedannotationList valueForKeyPath:@"description"]];
            NSString *milesRemining = [NSString stringWithFormat:@"%@",[_addedannotationList valueForKeyPath:@"Miles"]];
            if ([milesRemining integerValue]>1)
            {
                cell.lblMiles.text = [NSString stringWithFormat:@"%@ miles from here",milesRemining];
            }
            else
            {
                cell.lblMiles.text = cell.lblTime.text = [NSString stringWithFormat:@"< 1 mile from here"];
            }
            cell.lblTime.text=[NSString stringWithFormat:@"Job closes in %@",[_addedannotationList valueForKeyPath:@"Remaining_time"]];
            cell.imgThumbNail.contentMode = UIViewContentModeScaleAspectFill;
            cell.imgThumbNail.clipsToBounds = YES;
            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgThumbNail];
            
            NSMutableArray *arrMedia = [[NSMutableArray alloc]init];
            if ([[[_addedannotationList valueForKey:@"jobmedialist"] mutableCopy] count]!=0)
            {
                arrMedia = [[[_addedannotationList valueForKey:@"jobmedialist"] mutableCopy] objectAtIndex:0];
                if ([[arrMedia valueForKey:@"mediaType"] isEqualToString:@"1"])
                {
                    NSString *imageCacheKey = [[arrMedia valueForKey:@"mediaContent"] stringByDeletingPathExtension];
                    if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
                    {
                        cell.imgThumbNail.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
                    }
                    else
                    {
                        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                            cell.imgThumbNail.imageURL=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[arrMedia valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (cell.imgThumbNail.image)
                                {
                                    [[ImageCache sharedImageCache] storeImage:cell.imgThumbNail.image withKey:imageCacheKey];
                                }
                            });
                            
                            
                        });
                        
                    }
                    
                    
                    cell.imgPlay.hidden=YES;
                    
                }
                else if ([[arrMedia valueForKey:@"mediaType"] isEqualToString:@"2"])
                {
                    cell.imgPlay.hidden=NO;
                    NSString *imageCacheKey = [[arrMedia valueForKey:@"mediaContent"] stringByDeletingPathExtension];
                    if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
                    {
                        cell.imgThumbNail.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
                    }
                    else
                    {
                        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                            
                            AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[arrMedia valueForKey:@"mediaContent"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] options:nil];
                            NSString *imageCacheKey = [[arrMedia valueForKey:@"mediaContent"] stringByDeletingPathExtension];
                            AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                            generator.appliesPreferredTrackTransform = YES;
                            NSError *error;
                            CGImageRef imageRef = [generator copyCGImageAtTime:CMTimeMake(1, 2) actualTime:NULL error:&error];
                            if (!error)
                            {
                                UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    cell.imgThumbNail.image=image;
                                    if (cell.imgThumbNail.image)
                                    {
                                        [[ImageCache sharedImageCache] storeImage:cell.imgThumbNail.image withKey:imageCacheKey];
                                    }
                                    
                                });
                            }
                        });
                    }
                    
                }

            }
            
            NSString *imageCacheKey1 = [[_addedannotationList valueForKeyPath:@"icon"]  stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey1])
            {
                cell.imgJobIcon.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey1];
                
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[_addedannotationList valueForKeyPath:@"icon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                        UIImage *img=[UIImage imageWithData:data];
                        if (img)
                        {
                            [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey1];
                            cell.imgJobIcon.image=img;
                            
                        }
                        else
                        {
                            NSLog(@"Not Found");
                        }
                    });
                    
                    
                });
                
            }

            
//            [cell.btnPreview addTarget:self action:@selector(preview) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnBidNow addTarget:self action:@selector(gotoDetails) forControlEvents:UIControlEventTouchUpInside];
            cell.imgThumbNail.layer.cornerRadius=3.0;
            cell.imgThumbNail.layer.masksToBounds = YES;
            
            [annotationView.contentView addSubview:cell];
            
       
        return annotationView;
        }
	}
    else if ([annotation isKindOfClass:[BasicMapAnnotation class]])
    {
        
         MKAnnotationView *annotationView =[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomAnnotation"];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                           reuseIdentifier:@"CustomAnnotation"] ;
            annotationView.canShowCallout = NO;
            
            
            
            BasicMapAnnotation *myAnnotation  = (BasicMapAnnotation *)annotation;
            NSLog(@"tag:%@",myAnnotation.title);
            
            
//            annotationView.image = [UIImage imageNamed:@"pin_new.png"];
           
            UIView *anView=[[UIView alloc] init];
            anView.backgroundColor=[UIColor clearColor];
            
            UIImageView *bgImg=[[UIImageView alloc] init];
            bgImg.image=[UIImage imageNamed:@"pin_new.png"];
            bgImg.backgroundColor=[UIColor clearColor];
            
            UIImageView *Img=[[UIImageView alloc] init];
            Img.image=[UIImage imageNamed:@"logo1.png"];
            
            NSString *imageCacheKey1 = [[[_annotationList objectAtIndex:[myAnnotation.title intValue]] valueForKeyPath:@"icon"]  stringByDeletingPathExtension];
            if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey1])
            {
                Img.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey1];
                [Img setNeedsDisplay];
                
            }
            else
            {
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[_annotationList objectAtIndex:[myAnnotation.title intValue]] valueForKeyPath:@"icon"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
                        UIImage *img=[UIImage imageWithData:data];
                        if (img)
                        {
                            [[ImageCache sharedImageCache] storeImage:img withKey:imageCacheKey1];
                            Img.image=img;
                            [Img setNeedsDisplay];
                            
                        }
                        else
                        {
                            NSLog(@"Not Found");
                        }
                    });
                    
                    
                });
                
            }

            [Img setNeedsDisplay];
            Img.backgroundColor=[UIColor clearColor];
            
            
            annotationView.frame=CGRectMake(0, 0, IS_IPAD?66:44, IS_IPAD?66:44);
            anView.frame=CGRectMake(0, 0, IS_IPAD?66:44, IS_IPAD?66:44);
            bgImg.frame=CGRectMake(0, 0, IS_IPAD?66:44, IS_IPAD?66:44);
            Img.frame=CGRectMake(0, 0, IS_IPAD?30:22, IS_IPAD?30:22);
            Img.center= CGPointMake(CGRectGetMidX(anView.bounds),
                                    CGRectGetMidY(anView.bounds));
            [anView addSubview:bgImg];
            [anView addSubview:Img];
            [annotationView addSubview:anView];
          }
		
		return annotationView;
    }
	return nil;
}

/*- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    id myAnnotation = [mapView.annotations objectAtIndex:0];
    [mapView selectAnnotation:myAnnotation animated:YES];
}*/

- (void)resetAnnitations:(NSArray *)data
{
    _annotationList = [[NSMutableArray alloc]init];
    [self removeAllPinsButUserLocation];
    _annotationList = [data mutableCopy];
    [self setAnnotionsWithList:_annotationList];
}

- (void)removeAllPinsButUserLocation
{
    //id userLocation = [self.mapView userLocation];
    [self.mapView removeAnnotations:[self.mapView annotations]];
    
}

-(void)preview
{
    NSMutableArray *arrMedia = [[NSMutableArray alloc]init];
    arrMedia = [[_addedannotationList valueForKey:@"jobmedialist"] mutableCopy];
    if(delegate && [delegate respondsToSelector:@selector(showVideoPreview:)])
    {
        [delegate showVideoPreview:arrMedia];
    }
}

-(void)gotoDetails
{
    if(delegate && [delegate respondsToSelector:@selector(gotoDetailsPage:)])
    {
        [delegate gotoDetailsPage:_addedannotationList];
    }
}
@end
