//
//  OostaJobMapViewController.h
//  OostaJob
//
//  Created by Apple on 21/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapViewController.h"
@interface OostaJobMapViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate,MapViewControllerDidSelectDelegate
>
{
    NSString *animationDirection;
    NSIndexPath *indexPathSel;
    CLLocation *currentLocation;
    MapViewController *_mapViewController;
}
@property (strong, nonatomic) IBOutlet UITextField *txtSearch;
@property (strong, nonatomic) IBOutlet UITextField *txtJobCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalJobsAvailable;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) NSString *strZipcode;
@property (strong, nonatomic) NSString *strExpertise;
//@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)MenubtnTapped:(id)sender;
- (IBAction)RightMenubtnTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *dropDown;
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, retain) NSArray *list;
@property (weak, nonatomic) IBOutlet UIView *viewContentHolder;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tblViewJobList;
@property CLLocationManager *locationManager;
@property CLLocationCoordinate2D chosenLocation;
@property (weak, nonatomic) IBOutlet UIView *viewLblNumberOFJobs;
- (IBAction)btnSearchPressed:(id)sender;
@end
