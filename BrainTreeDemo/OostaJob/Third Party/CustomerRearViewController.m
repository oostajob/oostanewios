

#import "CustomerRearViewController.h"

#import "SWRevealViewController.h"
#import "CustomerRearTableViewCell.h"
#import "OostaJobWelcomeViewController.h"

#import "RearViewController.h"
#import "OostaJobLoginViewController.h"
#import "OostaJobRegisterViewController.h"
#import "OostaJobListOfJobsViewController.h"
#import "OostaJobCustomerJobsViewController.h"
#import "OostaJobCustomerUpdateViewController.h"
#import "OostaJobContactusViewController.h"
#import "OostaJobInboxViewController.h"

@interface CustomerRearViewController()<SWRevealViewControllerDelegate,OostaJobCustomerUpdateViewControllerDelegate>
{
    NSInteger _presentedRow;
    NSArray *imgArray;
    NSMutableArray *arrTitle;
}

@end


@implementation CustomerRearViewController

@synthesize rearTableView = _rearTableView;



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self CreateBlurEffectOnBackgroundImage];
    [self setupUI];
    
    
    
    arrTitle=[[NSMutableArray alloc]init];
    SWRevealViewController *parentRevealController = self.revealViewController;
    SWRevealViewController *grandParentRevealController = parentRevealController.revealViewController;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"logo.png"]
                                                                         style:UIBarButtonItemStyleBordered target:grandParentRevealController action:@selector(revealToggle:)];
    
    
    self.navigationController.navigationBarHidden=YES;
    
    if ( grandParentRevealController )
    {
        NSInteger level=0;
        UIViewController *controller = grandParentRevealController;
        while( nil != (controller = [controller revealViewController]) )
            level++;
        
        NSString *title = [NSString stringWithFormat:@"Detail Level %ld", (long)level];
        
        [self.navigationController.navigationBar addGestureRecognizer:grandParentRevealController.panGestureRecognizer];
        self.navigationItem.leftBarButtonItem = revealButtonItem;
        self.navigationItem.title = title;
    }
    
    imgArray=[[NSArray alloc]initWithObjects:@"customer_request.png",@"customer_Jobs.png",@"inbox.png",@"mail_White.png",@"mail_White.png", nil];
    self.view.backgroundColor=[UIColor colorWithWhite:0.302 alpha:1.000];
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    [_rearTableView selectRowAtIndexPath:indexPath animated:YES  scrollPosition:UITableViewScrollPositionBottom];
    _rearTableView.backgroundColor=[UIColor clearColor];
    self.navigationController.navigationBarHidden=YES;
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"SCREEN"] isEqualToString:@"REGISTEREDCUSTOMER"]||[[[NSUserDefaults standardUserDefaults]objectForKey:@"SCREEN"] isEqualToString:@"CUSTOMERJOBS"])
    {
        [self getUpdate];
    }
    else
    {
        _viewNotRegistered.hidden=NO;
        
    }
    _rearTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.lblName.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblAddress.font=[UIFont fontWithName:FONT_THIN size:26.0f];
        [self.BtnLogout.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        self.imgProfile.layer.cornerRadius=self.imgProfile.frame.size.width/2;
        self.imgProfile.layer.masksToBounds=YES;
        self.viewImgProfile.layer.cornerRadius=self.viewImgProfile.frame.size.width/2;
        self.viewImgProfile.layer.masksToBounds=YES;
        
        [self.btnLogin.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        [self.btnRegister.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        [self.btnGoBack.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        _lblNote.font=[UIFont fontWithName:FONT_BOLD size:26.0f];
    }
    else
    {
        self.lblName.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblAddress.font=[UIFont fontWithName:FONT_THIN size:18.0f];
        [self.BtnLogout.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        self.imgProfile.layer.cornerRadius=self.imgProfile.frame.size.width/2;
        self.imgProfile.layer.masksToBounds=YES;
        self.viewImgProfile.layer.cornerRadius=self.viewImgProfile.frame.size.width/2;
        self.viewImgProfile.layer.masksToBounds=YES;
        
        [self.btnLogin.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        [self.btnRegister.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        [self.btnGoBack.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        _lblNote.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
    }
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    SWRevealViewController *grandParentRevealController = self.revealViewController.revealViewController;
    grandParentRevealController.bounceBackOnOverdraw = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    SWRevealViewController *grandParentRevealController = self.revealViewController.revealViewController;
    grandParentRevealController.bounceBackOnOverdraw = YES;
}


#pragma marl - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    CustomerRearTableViewCell *cell = (CustomerRearTableViewCell*)[_rearTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NSUInteger row=indexPath.row;
    if (cell == nil)
    {
        NSArray *nib;
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            nib= [[NSBundle mainBundle] loadNibNamed:@"CustomerRearTableViewCell IPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
        }
        else if (IS_IPHONE5)
        {
            nib= [[NSBundle mainBundle] loadNibNamed:@"CustomerRearTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else
        {
            nib= [[NSBundle mainBundle] loadNibNamed:@"CustomerRearTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    
    cell.backgroundColor=[UIColor clearColor];
    NSString *text = nil;
    
    if (row == 0)
    {
        text = @"Request a service";
    }
    else if (row == 1)
    {
        text = @"Jobs";
    }
    else if (row == 2)
    {
        text = @"Inbox";
    }
    else if (row == 3)
    {
        text = @"Billing";
    }
    else if (row == 4)
    {
        text = @"Contact us";
    }
    
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    cell.lblMenuTitle.textColor=[UIColor whiteColor];
    cell.lblMenuTitle.text = NSLocalizedString( text, nil );
    cell.imgMenu.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",imgArray[indexPath.row]]];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        cell.lblMenuTitle.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
        
    }
    else if (IS_IPHONE5)
    {
        cell.lblMenuTitle.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
    }
    else
    {
        cell.lblMenuTitle.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
    }
    return cell;
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"test");
    SWRevealViewController *revealController = self.revealViewController;
    NSInteger row = indexPath.row;
    
    UIViewController *newFrontController = nil;
    
    if (row==0)
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            OostaJobListOfJobsViewController *ListObj = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController IPad" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:ListObj];
        }
        else if (IS_IPHONE5)
        {
            OostaJobListOfJobsViewController *ListObj = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:ListObj];
        }
        else
        {
            OostaJobListOfJobsViewController *ListObj = [[OostaJobListOfJobsViewController alloc] initWithNibName:@"OostaJobListOfJobsViewController" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:ListObj];
        }
    }
    else if (row==1)
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController IPad" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
        }
        else if (IS_IPHONE5)
        {
            OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
        }
        else
        {
            OostaJobCustomerJobsViewController *JobObj = [[OostaJobCustomerJobsViewController alloc] initWithNibName:@"OostaJobCustomerJobsViewController" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:JobObj];
        }
    }
    else if (row==2)
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            OostaJobInboxViewController *InboxObj = [[OostaJobInboxViewController alloc] initWithNibName:@"OostaJobInboxViewController IPad" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:InboxObj];
        }
        else if (IS_IPHONE5)
        {
            OostaJobInboxViewController *InboxObj = [[OostaJobInboxViewController alloc] initWithNibName:@"OostaJobInboxViewController" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:InboxObj];
        }
        else
        {
            OostaJobInboxViewController *InboxObj = [[OostaJobInboxViewController alloc] initWithNibName:@"OostaJobInboxViewController" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:InboxObj];
        }
    }
    else if (row == 3)
    {
       
            CustomerHistoryController *ContactusObj = [[CustomerHistoryController alloc] initWithNibName:@"CustomerHistoryController" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:ContactusObj];
        
    }
    else if (row == 4)
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            OostaJobContactusViewController *ContactusObj = [[OostaJobContactusViewController alloc] initWithNibName:@"OostaJobContactusViewController IPad" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:ContactusObj];
        }
        else if (IS_IPHONE5)
        {
            OostaJobContactusViewController *ContactusObj = [[OostaJobContactusViewController alloc] initWithNibName:@"OostaJobContactusViewController" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:ContactusObj];
        }
        else
        {
            OostaJobContactusViewController *ContactusObj = [[OostaJobContactusViewController alloc] initWithNibName:@"OostaJobContactusViewController" bundle:nil];
            newFrontController = [[UINavigationController alloc] initWithRootViewController:ContactusObj];
        }
    }
    
    
    
    [revealController pushFrontViewController:newFrontController animated:YES];
    
    _presentedRow = row;  // <- store the presented row
}
- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)CreateBlurEffectOnBackgroundImage
{
    [self.imgBlur assignBlur];
}
#pragma mark -Logout
-(IBAction)LogoutBtnTouched:(id)sender
{
    [self LogoutandCompletionHandler:^(bool resultLogout) {
        if (resultLogout == YES)
        {
            NSLog(@"Logout");
        }
    }];
}

-(void)LogoutandCompletionHandler:(void (^)(bool resultLogout))completionHandler
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.color=[UIColor whiteColor];
        hud.labelText = @"Logout...";
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        hud.margin = 010.f;
        hud.yOffset = 20.f;
        [hud show:YES];
        
        NSString *strUserID=[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.userID"];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@userlogout/%@/2",BaseURL,strUserID]]];
        NSLog(@"verificationstatus:%@",request.URL);
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest: request
                                           queue: [NSOperationQueue mainQueue]
                               completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error || !data)
             {
                 NSLog(@"Server Error : %@", error);
                 completionHandler(true);
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
             }
             else
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSLog(@"Result %@",result);
                 if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
                 {
                     NSLog(@"Result %@",result);
                     NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
                     [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
                     
                     [hud hide:YES];
                     MBProgressHUD*  HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                     [self.navigationController.view addSubview:HUD];
                     
                     HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                     HUD.mode = MBProgressHUDModeCustomView;
                     HUD.color=[UIColor whiteColor];
                     HUD.labelColor=kMBProgressHUDLabelColor;
                     HUD.backgroundColor=kMBProgressHUDBackgroundColor;
                     HUD.delegate = self;
                     HUD.labelText = @"Done";
                     
                     [HUD show:YES];
                     [HUD hide:YES afterDelay:2];
                     [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"SCREEN"];
                     [self gotoWelcomeScreen];
                     completionHandler(true);
                     
                     
                 }
                 else
                 {
                     NSLog(@"Result %@",result);
                     completionHandler(true);
                     [hud hide:YES];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     
                     hud.mode = MBProgressHUDModeText;
                     hud.color=[UIColor whiteColor];
                     hud.labelColor=kMBProgressHUDLabelColor;
                     hud.backgroundColor=kMBProgressHUDBackgroundColor;
                     
                     hud.labelText = SERVER_ERR;
                     hud.margin = 10.f;
                     hud.yOffset = 20.f;
                     hud.removeFromSuperViewOnHide = YES;
                     [hud hide:YES afterDelay:2];
                 }
                 
             }
             
         }];
    }
}


-(void)gotoWelcomeScreen
{
    
    self.window = [(OostaJobAppDelegate *)[[UIApplication sharedApplication] delegate] window];
    OostaJobWelcomeViewController *frontViewController;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        frontViewController = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        frontViewController = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController" bundle:nil];
    }
    else
    {
        frontViewController = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController Small" bundle:nil];
    }
    
    RearViewController *rearViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        rearViewController= [[RearViewController alloc] initWithNibName:@"RearViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        rearViewController= [[RearViewController alloc] initWithNibName:@"RearViewController" bundle:nil];
    }
    else
    {
        rearViewController= [[RearViewController alloc] initWithNibName:@"RearViewController Small" bundle:nil];
    }
    
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:rearViewController];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    
    mainRevealController.delegate = self;
    
    self.viewController = mainRevealController;
    
    self.window.rootViewController = self.viewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}

- (IBAction)btnProfilePressed:(id)sender
{
    UIViewController *newFrontController = nil;
    SWRevealViewController *revealController = self.revealViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobCustomerUpdateViewController *ProfileObj=[[OostaJobCustomerUpdateViewController alloc]initWithNibName:@"OostaJobCustomerUpdateViewController IPad" bundle:nil];
        ProfileObj.delegate = self;
        newFrontController = [[UINavigationController alloc] initWithRootViewController:ProfileObj];
    }
    else if (IS_IPHONE5)
    {
        OostaJobCustomerUpdateViewController *ProfileObj=[[OostaJobCustomerUpdateViewController alloc]initWithNibName:@"OostaJobCustomerUpdateViewController" bundle:nil];
        ProfileObj.delegate = self;
        newFrontController = [[UINavigationController alloc] initWithRootViewController:ProfileObj];
    }
    else
    {
        OostaJobCustomerUpdateViewController *ProfileObj=[[OostaJobCustomerUpdateViewController alloc]initWithNibName:@"OostaJobCustomerUpdateViewController Small" bundle:nil];
        ProfileObj.delegate = self;
        newFrontController = [[UINavigationController alloc] initWithRootViewController:ProfileObj];
    }
    [revealController pushFrontViewController:newFrontController animated:YES];
}
- (IBAction)btnLoginPressed:(id)sender
{
    UIViewController *newFrontController = nil;
    SWRevealViewController *revealController = self.revealViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobLoginViewController *LoginObj=[[OostaJobLoginViewController alloc]initWithNibName:@"OostaJobLoginViewController IPad" bundle:nil];
        LoginObj.strPage=@"CUSTOMER";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:LoginObj];
    }
    else if (IS_IPHONE5)
    {
        OostaJobLoginViewController *LoginObj=[[OostaJobLoginViewController alloc]initWithNibName:@"OostaJobLoginViewController" bundle:nil];
        LoginObj.strPage=@"CUSTOMER";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:LoginObj];
    }
    else
    {
        OostaJobLoginViewController *LoginObj=[[OostaJobLoginViewController alloc]initWithNibName:@"OostaJobLoginViewController Small" bundle:nil];
        LoginObj.strPage=@"CUSTOMER";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:LoginObj];
    }
    [revealController pushFrontViewController:newFrontController animated:YES];
}

- (IBAction)btnRegisterPressed:(id)sender
{
    UIViewController *newFrontController = nil;
    SWRevealViewController *revealController = self.revealViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobRegisterViewController *RegisterObj=[[OostaJobRegisterViewController alloc]initWithNibName:@"OostaJobRegisterViewController IPad" bundle:nil];
        RegisterObj.strPage=@"CUSTOMER";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:RegisterObj];
    }
    else if (IS_IPHONE5)
    {
        OostaJobRegisterViewController *RegisterObj=[[OostaJobRegisterViewController alloc]initWithNibName:@"OostaJobRegisterViewController" bundle:nil];
        RegisterObj.strPage=@"CUSTOMER";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:RegisterObj];
    }
    else
    {
        OostaJobRegisterViewController *RegisterObj=[[OostaJobRegisterViewController alloc]initWithNibName:@"OostaJobRegisterViewController Small" bundle:nil];
        RegisterObj.strPage=@"CUSTOMER";
        newFrontController = [[UINavigationController alloc] initWithRootViewController:RegisterObj];
    }
    [revealController pushFrontViewController:newFrontController animated:YES];
}
- (IBAction)btnProfileTapped:(id)sender {
}
-(void)getUpdate
{
    _viewNotRegistered.hidden=YES;
    NSLog(@"UserDetails:%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"USERDETAILS"]);
    
    _imgProfile.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.profilePhoto"]]];
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.profilePhoto"] isEqual: @""])
    {
        _imgProfile.image = nil;
    }
    
    NSArray *address=[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Address"]] componentsSeparatedByString:@","];
    if (address.count>3)
    {
        _lblAddress.text=[NSString stringWithFormat:@"%@ ,%@",address[address.count-3],address[address.count-2]];
    }
    else
    {
        _lblAddress.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Address"]];
    }
    
    _lblName.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"USERDETAILS.Name"]];
    
}
- (IBAction)btnGoBack:(id)sender
{
    [self gotoWelcomeScreen];
}
@end
