//
//  JingDianMapCell.h
//  IYLM
//
//  Created by Jian-Ye on 12-11-8.
//  Copyright (c) 2012年 Jian-Ye. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JingDianMapCell : UIView

@property (strong, nonatomic) IBOutlet AsyncImageView *imgThumbNail;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlay;
@property (strong, nonatomic) IBOutlet UIImageView *imgJobIcon;
@property (strong, nonatomic) IBOutlet UILabel *lblJobtitle;
@property (strong, nonatomic) IBOutlet UIView *viewBidNow;
@property (strong, nonatomic) IBOutlet UIButton *btnBidNow;
@property (strong, nonatomic) IBOutlet UILabel *lblZipcode;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet UILabel *lblMiles;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UIButton *btnPreview;

@end
