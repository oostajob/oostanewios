//
//  MVPopView.m
//  
//
//  Created by ximiao on 15/10/10.
//
//

#import "MVPopView.h"

@implementation MVPopView
-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self createHorizontalListWithImage];

    UIGestureRecognizer *rec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stopTap)];
    [self addGestureRecognizer:rec];
    return self;
}

-(void)showInView:(UIView*)view {
    [view addSubview:self];
    CGRect rect = self.frame;
    rect.origin.y = view.frame.size.height;
    self.frame = rect;
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
        CGRect rect = self.frame;
        rect.origin.y = view.frame.size.height - rect.size.height;
        self.frame = rect;
    } completion:nil];
    
    
//    CGRect rect = self.frame;
//    rect.origin.y = view.frame.size.height - rect.size.height;
//    self.frame = rect;
//    
//    CABasicAnimation *animation = [[CABasicAnimation alloc] init];
//    animation.duration = 0.3f;
//    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
//    animation.keyPath = @"transform.translation.y";
//    animation.fromValue = [NSNumber numberWithFloat:self.frame.size.height];
//    animation.toValue = [NSNumber numberWithFloat:0.0f];
//    animation.byValue = [NSNumber numberWithFloat:self.frame.size.height * -1.0f];
//    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    animation.removedOnCompletion = YES;
//    [self.layer addAnimation:animation forKey:nil];

}
- (void)createHorizontalListWithImage {
    TNImageRadioButtonData *reason1 = [TNImageRadioButtonData new];
    reason1.labelText = @"Contractor didn’t show up.";
    reason1.identifier = @"Contractor didn’t show up.";
    reason1.selected = YES;
    reason1.unselectedImage = [UIImage imageNamed:@"unchecked"];
    reason1.selectedImage = [UIImage imageNamed:@"checked"];
    
    TNImageRadioButtonData *reason2 = [TNImageRadioButtonData new];
    reason2.labelText = @"Contractor didn’t complete the job.";
    reason2.identifier = @"Contractor didn’t complete the job.";
    reason2.selected = NO;
    reason2.unselectedImage = [UIImage imageNamed:@"unchecked"];
    reason2.selectedImage = [UIImage imageNamed:@"checked"];
    
    TNImageRadioButtonData *reason3 = [TNImageRadioButtonData new];
    reason3.labelText = @"Contractor didn’t honor the Not to Exceed price.";
    reason3.identifier = @"Contractor didn’t honor the Not to Exceed price.";
    reason3.selected = NO;
    reason3.unselectedImage = [UIImage imageNamed:@"unchecked"];
    reason3.selectedImage = [UIImage imageNamed:@"checked"];
    
    TNImageRadioButtonData *reason4 = [TNImageRadioButtonData new];
    reason4.labelText = @"The scope of job changed after the on-site visit.";
    reason4.identifier = @"The scope of job changed after the on-site visit.";
    reason4.selected = NO;
    reason4.unselectedImage = [UIImage imageNamed:@"unchecked"];
    reason4.selectedImage = [UIImage imageNamed:@"checked"];
    
    TNImageRadioButtonData *reason5 = [TNImageRadioButtonData new];
    reason5.labelText = @"Contractor can not meet my deadline.";
    reason5.identifier = @"Contractor can not meet my deadline.";
    reason5.selected = NO;
    reason5.unselectedImage = [UIImage imageNamed:@"unchecked"];
    reason5.selectedImage = [UIImage imageNamed:@"checked"];
    self.temperatureGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[reason1, reason2, reason3, reason4, reason5] layout:TNRadioButtonGroupLayoutVertical];
    self.temperatureGroup.identifier = @"Temperature group";
    [self.temperatureGroup create];
    //NSLog(@"2 height :%f",self.view.bounds.size.height/4);
    
    self.temperatureGroup.position = CGPointMake(5, self.frame.size.height/4);
    
    //  [self addSubview:self.temperatureGroup];
    [self addSubview:self.temperatureGroup];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(temperatureGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:self.temperatureGroup];
}
- (void)temperatureGroupUpdated:(NSNotification *)notification {
    NSLog(@"[MainView] Temperature group updated to %@", self.temperatureGroup.selectedRadioButton.data.identifier);
}
- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SELECTED_RADIO_BUTTON_CHANGED object:self.temperatureGroup];
}
-(void)dismiss {

    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        CGRect rect = self.frame;
        rect.origin.y += rect.size.height;
        self.frame = rect;
    } completion:nil];
}
- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag {\
    [self removeFromSuperview];
}
-(void)stopTap {
    
}
@end
