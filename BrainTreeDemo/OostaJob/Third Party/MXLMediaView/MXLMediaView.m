//
//  MXLMediaView.m
//
//  Created by Kiran Panesar on 08/02/2014.
//  Copyright (c) 2014 MobileX Labs. All rights reserved.
//

#import "MXLMediaView.h"
#import "ImageCache.h"
// Frameworks
@import MediaPlayer;

// Categories
#import "UIImage+ImageEffects.h"

const CGFloat kMXLMediaViewBackgroundViewOriginalScale = 1.2f;

#define IS_DEVICE_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@interface MXLMediaView () <UIDynamicAnimatorDelegate>

// Background image view (used for blurring the background)
@property (strong, nonatomic, readwrite) UIImageView       *backgroundImageView;

// UIKit Dynamics manager
@property (strong, nonatomic, readwrite) UIDynamicAnimator *dynamicAnimator;

// Gesture recognizers
@property (strong, nonatomic, readwrite) UITapGestureRecognizer       *tapGestureRecognizer;
@property (strong, nonatomic, readwrite) UILongPressGestureRecognizer *longPressGestureRecognizer;

@property (assign, nonatomic, readwrite) MXLMediaViewType mediaType;

-(void)dropDownView:(UIView *)view withGravityVelocity:(float)velocity;

-(void)showMediaImageView;
-(void)showMoviePlayerView;

-(void)dismiss:(id)sender;

-(void)pushLongPress:(id)sender;

@end

@implementation MXLMediaView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)addingMediaInScrollViewinParentView:(UIView *)parentView completion:(void(^)(void))completion
{
    NSLog(@"arrMedia:%@",_arrMedia);
    NSLog(@"SelectedIndex:%ld",(long)_selectedIndex);
    // Store the parent view and video URL
    _parentView = parentView;
    
    // Set up self
    [self setFrame:CGRectMake(0.0f, 0.0f, parentView.frame.size.width, parentView.frame.size.height)];
    [self setUserInteractionEnabled:YES];
    
    // Set up background imageview
    // This is used to replace the parentView in the backgroud, allowing us to efficiently blur
    // We're setting this to be 1.2x the size of the parentView so the parent view will be smaller than the bounds
    // of _backgroundImageView, so when we blur the view it will blur around the bounds, rather than clipping.
    _backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, parentView.frame.size.width * kMXLMediaViewBackgroundViewOriginalScale,
                                                                         parentView.frame.size.height * kMXLMediaViewBackgroundViewOriginalScale)];
    [_backgroundImageView setContentMode:UIViewContentModeCenter];
    [_backgroundImageView setImage:[self captureView:parentView]];
    [_backgroundImageView setImage:[self blurredImageFromView:_backgroundImageView]]; // Blur
    [_backgroundImageView setAlpha:0.0f]; // Make invisible
    
    // Update the center of the image view to match the parent
    [_backgroundImageView setCenter:parentView.center];
    
    [self addSubview:_backgroundImageView];
    
    
    // Add self to view stack
    [parentView addSubview:self];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _scrollView.pagingEnabled = YES;
    _scrollView.delegate = self;
    [self addSubview:_scrollView];
    if ([_strBidding isEqualToString:@"YES"])
    {
        _scrollView.contentSize =CGSizeMake(self.frame.size.width*_dictMedia.count, self.frame.size.height);
    }
    else
    {
        _scrollView.contentSize =CGSizeMake(self.frame.size.width*_arrMedia.count, self.frame.size.height);
    }
    
    
    
    CGPoint scrollPoint = CGPointMake(self.frame.size.width * _selectedIndex, 0);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    
    
    arrKeys = [[NSMutableArray alloc]init];
    arrKeys = [[_dictMedia allKeys] mutableCopy];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:arrKeys];
    [sortedArray sortUsingSelector:@selector(localizedStandardCompare:)];
    arrKeys = [[NSMutableArray alloc]init];
    arrKeys = [sortedArray mutableCopy];
    
    if ([_strBidding isEqualToString:@"YES"])
    {
        for (int i=0; i<arrKeys.count; i++)
        {
            _currentIndex = i;
            if ([[arrKeys objectAtIndex:_currentIndex] rangeOfString:@"Image"].length!=0)
            {
                _mediaType = MXLMediaViewTypeImage;
                [self showMediaFile:[_dictMedia valueForKeyPath:[NSString stringWithFormat:@"%@",[arrKeys objectAtIndex:_currentIndex]]] ofType:_mediaType inParentView:parentView completion:completion];
            }
            else if ([[arrKeys objectAtIndex:_currentIndex] rangeOfString:@"Video"].length!=0)
            {
                NSURL *videoURL = [_dictMedia valueForKeyPath:[NSString stringWithFormat:@"%@",[arrKeys objectAtIndex:_currentIndex]]];
                _mediaType = MXLMediaViewTypeVideo;
                [self showMediaFile:videoURL ofType:_mediaType inParentView:parentView completion:completion];
            }
            
        }
    }
    else
    {
        for (int i=0; i<_arrMedia.count; i++)
        {
            _currentIndex = i;
            if ([[[_arrMedia objectAtIndex:_currentIndex] valueForKey:@"mediaType"] isEqualToString:@"1"])
            {
                NSString *imageCacheKey = [[[_arrMedia objectAtIndex:_currentIndex] valueForKey:@"mediaContent"] stringByDeletingPathExtension];
                if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
                {
                    //
                    _mediaType = MXLMediaViewTypeImage;
                    [self showMediaFile:[[ImageCache sharedImageCache]imageForKey:imageCacheKey] ofType:_mediaType inParentView:parentView completion:completion];
                }
                else
                {
                    NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[_arrMedia objectAtIndex:_currentIndex] valueForKey:@"mediaContent"]]]];
                    UIImage *img=[UIImage imageWithData:data];
                    _mediaType = MXLMediaViewTypeImage;
                    [self showMediaFile:img ofType:_mediaType inParentView:parentView completion:completion];
                    
                }
                
                
            }
            else if ([[[_arrMedia objectAtIndex:_currentIndex] valueForKey:@"mediaType"] isEqualToString:@"2"])
            {
                NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[_arrMedia objectAtIndex:_currentIndex] valueForKey:@"mediaContent"]]];
                _mediaType = MXLMediaViewTypeVideo;
                [self showMediaFile:videoURL ofType:_mediaType inParentView:parentView completion:completion];
            }
            
            
            
        }
    }
    
    _currentIndex = _selectedIndex;
}
// Method to show an image
-(void)showImage:(UIImage *)image inParentView:(UIView *)parentView completion:(void(^)(void))completion {
    _mediaType = MXLMediaViewTypeImage;
    [self showMediaFile:image ofType:_mediaType inParentView:parentView completion:completion];
}

// Method to show a video
-(void)showVideoWithURL:(NSURL *)videoURL inParentView:(UIView *)parentView completion:(void(^)(void))completion {
    _mediaType = MXLMediaViewTypeVideo;
    [self showMediaFile:videoURL ofType:_mediaType inParentView:parentView completion:completion];
}

// Main method to show a media file
-(void)showMediaFile:(id)mediaFile ofType:(MXLMediaViewType)mediaType inParentView:(UIView *)parentView completion:(void(^)(void))completion {
    // Set up the completion block
    
    _completionBlock = completion;
    
    // Check the file type, show the appropriate view
    if (mediaType == MXLMediaViewTypeImage) {
        _mediaImage = (UIImage *)mediaFile;
        [self showMediaImageView];
    } else if (mediaType == MXLMediaViewTypeVideo) {
        
        if (_currentIndex==_selectedIndex)
        {
            _videoURL   = (NSURL *)mediaFile;
            [self showMoviePlayerView];
        }
    }
    
    if (_currentIndex==_selectedIndex)
    {
        // Animate the background image view opacity
        // This gives the illusion that the background is being blurred
        [UIView animateWithDuration:0.2 animations:^{
            [_backgroundImageView setAlpha:1.0f];
        } completion:^(BOOL finished) {
            
            // Once that's complete, hide all the parent view's subviews, except for self
            for (UIView *v in parentView.subviews) {
                if (v != self) {
                    [v setHidden:YES];
                }
            }
            
            // Animate the scaling of the background image
            // Giving the illusion that the background view is shrinking a bit
            [UIView animateWithDuration:0.2 animations:^{
                // CATransform stuff
                CGAffineTransform transform = _backgroundImageView.transform;
                [_backgroundImageView setTransform:CGAffineTransformScale(transform, 1/kMXLMediaViewBackgroundViewOriginalScale, 1/kMXLMediaViewBackgroundViewOriginalScale)];
                [_backgroundImageView setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0f, [UIScreen mainScreen].bounds.size.height/2.0f)];
                [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
            }];
        }];
        
        UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(IS_IPAD?700:280, IS_IPAD?20:5, IS_IPAD?30:30, IS_IPAD?30:30)];
        [btn setBackgroundImage:[UIImage imageNamed:@"close-new.png"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(dismiss:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        [btn bringSubviewToFront:_mediaImageView];
    }
    
}

// Method to show the actual image provided
-(void)showMediaImageView {
    // Initialise the imageview
    CGFloat originX = self.frame.size.width * _currentIndex;
    _mediaImageView = [[UIImageView alloc] initWithImage:_mediaImage];
    [_mediaImageView setFrame:CGRectMake(originX, 0.0f, self.frame.size.width, self.frame.size.height)]; // Set it to be off frame
    [_mediaImageView setContentMode:UIViewContentModeScaleAspectFit];                                                               // Set the content mode
    
    [self.scrollView addSubview:_mediaImageView]; // Add to view stack
    
    //    [self dropDownView:_mediaImageView withGravityVelocity:(IS_DEVICE_IPAD ? 9.0f : 2.5f)];
}

// Method to show the video player
-(void)showMoviePlayerView {
    
    CGFloat originX = self.frame.size.width * _currentIndex;
    
    
    
    _moviePlayer = [[ALMoviePlayerController alloc] initWithFrame:CGRectMake(originX,((self.frame.size.height/2)-((IS_IPAD?400.0f:180.0f)/2)), self.frame.size.width, IS_IPAD?400.0f:180.0f)];
    
    //    moviePlayer.view.center = CGPointMake(_scrollView.frame.size.width  / 2,
    //                                     _scrollView.frame.size.height / 2);
    
    _moviePlayer.view.alpha = 0.f;
    _moviePlayer.delegate = self;
    ALMoviePlayerControls *movieControls = [[ALMoviePlayerControls alloc] initWithMoviePlayer:_moviePlayer style:ALMoviePlayerControlsStyleDefault];
    movieControls.barHeight=IS_IPAD?80.0f:50.0f;
    //    [movieControls setAdjustsFullscreenImage:YES];
    [movieControls setBarColor:[UIColor colorWithWhite:0.000 alpha:0.600]];
    [movieControls setTimeRemainingDecrements:YES];
    //[movieControls setFadeDelay:2.0];
    //[movieControls setBarHeight:100.f];
    //[movieControls setSeekRate:2.f];
    //assign controls
    movieControls.Closeshowing = YES;
    [_moviePlayer setControls:movieControls];
    [self.scrollView addSubview:_moviePlayer.view];
    //    [self dropDownView:self.moviePlayer.view withGravityVelocity:(IS_DEVICE_IPAD ? 10.0f : 3.5f)];
    
    NSURL *videoURL = _videoURL;
    [_moviePlayer setContentURL:videoURL];
    //delay initial load so statusBarOrientation returns correct value
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //        [self configureViewForOrientation:[UIApplication sharedApplication].statusBarOrientation];
        [UIView animateWithDuration:0.3 delay:0.0 options:0 animations:^{
            _moviePlayer.view.alpha = 1.f;
        } completion:^(BOOL finished) {
            
        }];
    });
    
    
    
    
}

// Method to drop down an arbitrary UIView into the centre of the screen with a given velocity
-(void)dropDownView:(UIView *)view withGravityVelocity:(float)velocity {
    // Set up UIKit Dynamic animator instance
    _dynamicAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.scrollView];
    [_dynamicAnimator setDelegate:self];
    
    CGFloat originX = self.frame.size.width * _currentIndex;
    // Create collision point at the bottom boundary of self
    UICollisionBehavior *collisionBehaviour = [[UICollisionBehavior alloc] initWithItems:@[view]];
    [collisionBehaviour addBoundaryWithIdentifier:@"barrier"
                                        fromPoint:CGPointMake(originX, (self.frame.size.height + view.frame.size.height)/2.0f)
                                          toPoint:CGPointMake(self.frame.size.width*(_currentIndex+1), (self.frame.size.height + view.frame.size.height)/2.0f)];
    
    // Add gravity effect with 2.5 vertical velocity
    UIGravityBehavior *gravityBehaviour   = [[UIGravityBehavior alloc] initWithItems:@[view]];
    [gravityBehaviour setGravityDirection:CGVectorMake(0.0f, velocity)];
    
    // Add the collision and gravity behaviours to the main animator
    [_dynamicAnimator addBehavior:collisionBehaviour];
    [_dynamicAnimator addBehavior:gravityBehaviour];
}

// Used to hide the image/video being displayed
-(void)hideMediaView {
    
    // Pause the video, this keeps the thumbnail visible while the view is dismissing.
    // if you call -stop on the player, the background of the player cuts to black. Not very smooth.
    [self.moviePlayer pause];
    
    // Animation to shrink it to nothing
    [UIView animateWithDuration:0.2 animations:^{
        CGAffineTransform imageViewTransform = _mediaImageView.transform;
        [_mediaImageView setTransform:CGAffineTransformScale(imageViewTransform, 0.001f, 0.001f)];
        [_mediaImageView setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0f, [UIScreen mainScreen].bounds.size.height/2.0f)];
        
        CGAffineTransform movieViewTransform = self.moviePlayer.view.transform;
        [self.moviePlayer.view setTransform:CGAffineTransformScale(movieViewTransform, 0.001f, 0.001f)];
        [self.moviePlayer.view setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0f, [UIScreen mainScreen].bounds.size.height/2.0f)];
    } completion:^(BOOL finished) {
        [self.moviePlayer stop]; // Now it's hidden, stop playback completely
        [self.moviePlayer.view removeFromSuperview];
        [_mediaImageView removeFromSuperview];
    }];
}

// Method to dismiss self
-(void)dismiss:(id)sender {
    // Remove gesture recognizers, prevents users from 'double exiting' by tapping twice
    [self removeGestureRecognizer:_tapGestureRecognizer];
    [self removeGestureRecognizer:_longPressGestureRecognizer];
    
    // Trigger mediaViewWillDismiss: delegate method
    if ([_delegate respondsToSelector:@selector(mediaViewWillDismiss:)]) {
        [_delegate mediaViewWillDismiss:self];
    }
    
    // Dismiss the image/video being displayed
    [self hideMediaView];
    
    // Scale background back to fullscreen
    [UIView animateWithDuration:0.2 animations:^{
        // Scale background image
        CGAffineTransform transform = _backgroundImageView.transform;
        [_backgroundImageView setTransform:CGAffineTransformScale(transform, kMXLMediaViewBackgroundViewOriginalScale, kMXLMediaViewBackgroundViewOriginalScale)];
        [_backgroundImageView setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0f, [UIScreen mainScreen].bounds.size.height/2.0f)];
        
        // Show status bar
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    } completion:^(BOOL finished) {
        // Show all parentview subviews again
        for (UIView *v in _parentView.subviews) {
            [v setHidden:NO];
        }
        
        // Animate background image opacity to 0
        // Giving the illusion that the background image is un-blurring
        [UIView animateWithDuration:0.2 animations:^{
            [_backgroundImageView setAlpha:0.0f];
        } completion:^(BOOL finished) {
            
            // Trigger delegate method
            if ([_delegate respondsToSelector:@selector(mediaViewDidDismiss:)]) {
                [_delegate mediaViewDidDismiss:self];
            }
            
            [self removeFromSuperview];
        }];
    }];
    
    //    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

-(void)pushLongPress:(id)sender {
    if ([(UIGestureRecognizer *)sender state] == UIGestureRecognizerStateBegan) {
        if ([_delegate respondsToSelector:@selector(mediaView:didReceiveLongPressGesture:)]) {
            [_delegate mediaView:self didReceiveLongPressGesture:sender];
        }
    }
}

#pragma mark Blur methods

-(UIImage *)blurredImageFromView:(UIView *)backgroundView
{
    UIImage *backgroundImage = [self captureView:backgroundView];
    backgroundImage = [backgroundImage applyBlurWithRadius:6.0 tintColor:[UIColor colorWithWhite:0.0f alpha:0.6] saturationDeltaFactor:1.0f maskImage:nil];
    return backgroundImage;
}

- (UIImage *)captureView:(UIView *)view {
    CGRect captureRect = view.bounds;
    UIGraphicsBeginImageContext(captureRect.size);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [[UIColor blackColor] set];
    CGContextFillRect(ctx, captureRect);
    
    [view.layer renderInContext:ctx];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(void)dynamicAnimatorDidPause:(UIDynamicAnimator *)animator {
    // Set up and add gesture recognizers
    // We set this in the UIDynamicAnimator completion delegete so the
    // user can't dismiss the view while the media image is dropping down
    //    _tapGestureRecognizer       = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
    _longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(pushLongPress:)];
    
    [self.moviePlayer prepareToPlay];
    [self.moviePlayer play];
    
    //    [self addGestureRecognizer:_tapGestureRecognizer];
    [self addGestureRecognizer:_longPressGestureRecognizer];
    
    if (_completionBlock) {
        _completionBlock();
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    NSLog(@"Dragging - You are now on page %i",page);
    if ([_strBidding isEqualToString:@"YES"])
    {
        if ([[arrKeys objectAtIndex:page] rangeOfString:@"Video"].length!=0)
        {
            if (_currentIndex!=page)
            {
                [self.moviePlayer stop]; // Now it's hidden, stop playback completely
                [self.moviePlayer.view removeFromSuperview];
                _currentIndex = page;
                NSURL *videoURL = [_dictMedia valueForKeyPath:[NSString stringWithFormat:@"%@",[arrKeys objectAtIndex:_currentIndex]]];
                _videoURL   = videoURL;
                [self showMoviePlayerView];
            }
            
        }
        else
        {
            _currentIndex = page;
            [self.moviePlayer stop]; // Now it's hidden, stop playback completely
            [self.moviePlayer.view removeFromSuperview];
        }
    }
    else
    {
        if ([[[_arrMedia objectAtIndex:page] valueForKey:@"mediaType"] isEqualToString:@"2"])
        {
            if (_currentIndex!=page)
            {
                [self.moviePlayer stop]; // Now it's hidden, stop playback completely
                [self.moviePlayer.view removeFromSuperview];
                _currentIndex = page;
                NSURL *videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[_arrMedia objectAtIndex:page] valueForKey:@"mediaContent"]]];
                _videoURL   = videoURL;
                [self showMoviePlayerView];
            }
        }
        else
        {
            _currentIndex = page;
            [self.moviePlayer stop]; // Now it's hidden, stop playback completely
            [self.moviePlayer.view removeFromSuperview];
        }
    }
}
@end
