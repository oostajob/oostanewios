

#import "RearViewController.h"

#import "SWRevealViewController.h"
#import "RearTableViewCell.h"
#import "OostaJobWelcomeViewController.h"
#import "OostaJobAboutUsViewController.h"
#import "OostaJobCustomerStoriesViewController.h"
#import "OostaJobFAQViewController.h"
#import "OostaJobContactusViewController.h"
@interface RearViewController()
{
    NSInteger _presentedRow;
    NSArray *imgArray;
    NSMutableArray *arrTitle;
}

@end


@implementation RearViewController

@synthesize rearTableView = _rearTableView;



- (void)viewDidLoad
{
	[super viewDidLoad];
	
    
    arrTitle=[[NSMutableArray alloc]init];
    SWRevealViewController *parentRevealController = self.revealViewController;
    SWRevealViewController *grandParentRevealController = parentRevealController.revealViewController;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"logo.png"]
                                                                         style:UIBarButtonItemStyleBordered target:grandParentRevealController action:@selector(revealToggle:)];
    _rearTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.navigationController.navigationBarHidden=YES;
    
    if ( grandParentRevealController )
    {
        NSInteger level=0;
        UIViewController *controller = grandParentRevealController;
        while( nil != (controller = [controller revealViewController]) )
            level++;
        
        NSString *title = [NSString stringWithFormat:@"Detail Level %ld", (long)level];
        
        [self.navigationController.navigationBar addGestureRecognizer:grandParentRevealController.panGestureRecognizer];
        self.navigationItem.leftBarButtonItem = revealButtonItem;
        self.navigationItem.title = title;
    }
    
    
    else
    {
        
        
    }
    
    imgArray=[[NSArray alloc]initWithObjects:@"about_White.png",@"mail_White.png",@"stories_White.png",@"faq_White.png", nil];
    self.view.backgroundColor=[UIColor colorWithWhite:0.302 alpha:1.000];
    _rearTableView.backgroundColor=[UIColor colorWithWhite:0.302 alpha:1.000];
    self.navigationController.navigationBarHidden=YES;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
       [self.btnGetStarted.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
    }
    else
    {
        [self.btnGetStarted.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
    }
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    SWRevealViewController *grandParentRevealController = self.revealViewController.revealViewController;
    grandParentRevealController.bounceBackOnOverdraw = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    SWRevealViewController *grandParentRevealController = self.revealViewController.revealViewController;
    grandParentRevealController.bounceBackOnOverdraw = YES;
}


#pragma marl - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
      return 4;
    }
    else
    {
       return 2;
    }
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    RearTableViewCell *cell = (RearTableViewCell*)[_rearTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NSUInteger row=indexPath.row;
    
    if (cell == nil)
    {
        NSArray *nib;
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            nib= [[NSBundle mainBundle] loadNibNamed:@"RearTableViewCell IPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
        }
        else if (IS_IPHONE5)
        {
            nib= [[NSBundle mainBundle] loadNibNamed:@"RearTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        else
        {
            nib= [[NSBundle mainBundle] loadNibNamed:@"RearTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    }
    
    cell.backgroundColor=[UIColor colorWithWhite:0.302 alpha:1.000];
    NSString *text = nil;
   
    
    if (indexPath.section==0)
    {
        if (row == 0)
        {
            text = @"About us";
        }
        else if (row == 1)
        {
            text = @"Contact us";
        }
        else if (row == 2)
        {
            text = @"Customer Stories";
            
        }
        else if (row == 3)
        {
            text = @"FAQ";
        }
        cell.lblMenuTitle.textColor=[UIColor whiteColor];
        cell.lblMenuTitle.text = NSLocalizedString( text, nil );
        cell.imgMenu.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",imgArray[indexPath.row]]];
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            cell.lblMenuTitle.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        }
        else if (IS_IPHONE5)
        {
            cell.lblMenuTitle.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        }
        else
        {
            cell.lblMenuTitle.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        }
 
    }
    else
    {
        if (row == 0)
        {
            text = @"Customer";
            cell.imgMenu.image=[UIImage imageNamed:@"customer_White.png"];
        }
        else if (row == 1)
        {
            text = @"Contractor";
            cell.imgMenu.image=[UIImage imageNamed:@"contractor_White.png"];
        }
        if (indexPath.row == 1) {
            cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
        }
        cell.lblMenuTitle.textColor=[UIColor whiteColor];
        cell.lblMenuTitle.text = NSLocalizedString( text, nil );
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            cell.lblMenuTitle.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        }
        else if (IS_IPHONE5)
        {
            cell.lblMenuTitle.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        }
        else
        {
            cell.lblMenuTitle.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        }

    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    /*UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor clearColor]];
    [cell setSelectedBackgroundView:bgColorView];*/
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        return 88;
    }
    else
    {
        return 44;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    SWRevealViewController *revealController = self.revealViewController;
    NSInteger row = indexPath.row;
    
    UIViewController *newFrontController = nil;
    
    if (indexPath.section==0)
    {
        if (row == 0)
        {
            
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                OostaJobAboutUsViewController *AboutUsObj = [[OostaJobAboutUsViewController alloc] initWithNibName:@"OostaJobAboutUsViewController IPad" bundle:nil];
                newFrontController = [[UINavigationController alloc] initWithRootViewController:AboutUsObj];
            }
            else if (IS_IPHONE5)
            {
                OostaJobAboutUsViewController *AboutUsObj = [[OostaJobAboutUsViewController alloc] initWithNibName:@"OostaJobAboutUsViewController" bundle:nil];
                newFrontController = [[UINavigationController alloc] initWithRootViewController:AboutUsObj];
            }
            else
            {
                OostaJobAboutUsViewController *AboutUsObj = [[OostaJobAboutUsViewController alloc] initWithNibName:@"OostaJobAboutUsViewController" bundle:nil];
                newFrontController = [[UINavigationController alloc] initWithRootViewController:AboutUsObj];
            }
            
        }
        else if (row == 1)
        {
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                OostaJobContactusViewController *ContactusObj = [[OostaJobContactusViewController alloc] initWithNibName:@"OostaJobContactusViewController IPad" bundle:nil];
                newFrontController = [[UINavigationController alloc] initWithRootViewController:ContactusObj];
            }
            else if (IS_IPHONE5)
            {
                OostaJobContactusViewController *ContactusObj = [[OostaJobContactusViewController alloc] initWithNibName:@"OostaJobContactusViewController" bundle:nil];
                newFrontController = [[UINavigationController alloc] initWithRootViewController:ContactusObj];
            }
            else
            {
                OostaJobContactusViewController *ContactusObj = [[OostaJobContactusViewController alloc] initWithNibName:@"OostaJobContactusViewController" bundle:nil];
                newFrontController = [[UINavigationController alloc] initWithRootViewController:ContactusObj];
            }
            
        }
        else if (row == 2)
        {
            
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                OostaJobCustomerStoriesViewController *CustomerStoriesObj = [[OostaJobCustomerStoriesViewController alloc] initWithNibName:@"OostaJobCustomerStoriesViewController IPad" bundle:nil];
                newFrontController = [[UINavigationController alloc] initWithRootViewController:CustomerStoriesObj];
            }
            else if (IS_IPHONE5)
            {
                OostaJobCustomerStoriesViewController *CustomerStoriesObj = [[OostaJobCustomerStoriesViewController alloc] initWithNibName:@"OostaJobCustomerStoriesViewController" bundle:nil];
                newFrontController = [[UINavigationController alloc] initWithRootViewController:CustomerStoriesObj];
            }
            else
            {
                OostaJobCustomerStoriesViewController *CustomerStoriesObj = [[OostaJobCustomerStoriesViewController alloc] initWithNibName:@"OostaJobCustomerStoriesViewController" bundle:nil];
                newFrontController = [[UINavigationController alloc] initWithRootViewController:CustomerStoriesObj];
            }
            
        }
        else if ( row == 3 )
        {
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                OostaJobFAQViewController *FAQObj = [[OostaJobFAQViewController alloc] initWithNibName:@"OostaJobFAQViewController IPad" bundle:nil];
                newFrontController = [[UINavigationController alloc] initWithRootViewController:FAQObj];
            }
            else if (IS_IPHONE5)
            {
                OostaJobFAQViewController *FAQObj = [[OostaJobFAQViewController alloc] initWithNibName:@"OostaJobFAQViewController" bundle:nil];
                newFrontController = [[UINavigationController alloc] initWithRootViewController:FAQObj];
            }
            else
            {
                OostaJobFAQViewController *FAQObj = [[OostaJobFAQViewController alloc] initWithNibName:@"OostaJobFAQViewController" bundle:nil];
                newFrontController = [[UINavigationController alloc] initWithRootViewController:FAQObj];
            }
        }
    }
    else
    {
        if (row == 0)
        {
            
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                OostaJobWelcomeViewController *welcomeObj = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController IPad" bundle:nil];
                welcomeObj.strPage=@"Customer";
                newFrontController = [[UINavigationController alloc] initWithRootViewController:welcomeObj];
            }
            else if (IS_IPHONE5)
            {
                OostaJobWelcomeViewController *welcomeObj = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController" bundle:nil];
                welcomeObj.strPage=@"Customer";
                newFrontController = [[UINavigationController alloc] initWithRootViewController:welcomeObj];
            }
            else
            {
                OostaJobWelcomeViewController *welcomeObj = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController Small" bundle:nil];
                welcomeObj.strPage=@"Customer";
                newFrontController = [[UINavigationController alloc] initWithRootViewController:welcomeObj];
            }
            
        }
        else if (row == 1)
        {
            
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                OostaJobWelcomeViewController *welcomeObj = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController IPad" bundle:nil];
                welcomeObj.strPage=@"Contractor";
                newFrontController = [[UINavigationController alloc] initWithRootViewController:welcomeObj];
            }
            else if (IS_IPHONE5)
            {
                OostaJobWelcomeViewController *welcomeObj = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController" bundle:nil];
                welcomeObj.strPage=@"Contractor";
                newFrontController = [[UINavigationController alloc] initWithRootViewController:welcomeObj];
            }
            else
            {
                OostaJobWelcomeViewController *welcomeObj = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController Small" bundle:nil];
                welcomeObj.strPage=@"Contractor";
                newFrontController = [[UINavigationController alloc] initWithRootViewController:welcomeObj];
            }
            
        }
        
    }
    
    
    [revealController pushFrontViewController:newFrontController animated:YES];
    
    _presentedRow = row;  // <- store the presented row
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0)
    {
        return 0;
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
           return 88;
        }
        else
        {
           return 44;
        }
        
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==0)
    {
        return nil;
    }
    else
    {
        UIView *view;
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            view= [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 87)];
        }
        else
        {
            view= [[UIView alloc] initWithFrame:CGRectMake(0, 1, tableView.frame.size.width, 43)];
        }
        UILabel *label;
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width-10, 87)];
            [label setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        }
        else
        {
            label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width-10, 43)];
            [label setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        }
        [label setTextColor:[UIColor whiteColor]];
        [label setText:@"How it works"];
        [view addSubview:label];
        [view setBackgroundColor:[UIColor colorWithWhite:0.243 alpha:1.000]];
        return view;
    }
}



- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnGetStarted:(id)sender
{
    UIViewController *newFrontController = nil;
    SWRevealViewController *revealController = self.revealViewController;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobWelcomeViewController *welcomeObj = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController IPad" bundle:nil];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:welcomeObj];
    }
    else if (IS_IPHONE5)
    {
        OostaJobWelcomeViewController *welcomeObj = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController" bundle:nil];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:welcomeObj];
    }
    else
    {
        OostaJobWelcomeViewController *welcomeObj = [[OostaJobWelcomeViewController alloc] initWithNibName:@"OostaJobWelcomeViewController Small" bundle:nil];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:welcomeObj];
    }
    [revealController pushFrontViewController:newFrontController animated:YES];
}
@end
