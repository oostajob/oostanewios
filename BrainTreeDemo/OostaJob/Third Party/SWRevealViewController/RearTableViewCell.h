//
//  RearTableViewCell.h
//  Family Fitness Center
//
//  Created by Apple on 30/03/15.
//  Copyright (c) 2015 armor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RearTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgMenu;
@property (weak, nonatomic) IBOutlet UILabel *lblMenuTitle;


@end
