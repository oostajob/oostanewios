//
//  OostaJobReviewTableViewCell.h
//  OostaJob
//
//  Created by Apple on 13/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface OostaJobReviewTableViewCell : UITableViewCell
@property(nonatomic,strong)IBOutlet AsyncImageView *imgProfile;
@property (strong, nonatomic) IBOutlet UILabel *lblCellName;
@property (strong, nonatomic) IBOutlet UILabel *lblCellDate;
@property (strong, nonatomic) IBOutlet UILabel *lblCellAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgStars;
@property (weak, nonatomic) IBOutlet UIView *viewStars;
@property (weak, nonatomic) IBOutlet UILabel *lblJobTypes;

@end
