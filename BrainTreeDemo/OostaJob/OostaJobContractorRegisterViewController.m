//
//  OostaJobCustomerRegisterationViewController.m
//  OostaJob
//
//  Created by Apple on 20/10/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobContractorRegisterViewController.h"
#import "OostaJobEmailVerificationViewController.h"
#import "UIImage+ImageCompress.h"
#import "OostaJobZipCodeViewController.h"
@interface OostaJobContractorRegisterViewController ()
{
    UIImage *choosenImage;
    UIImage *licenseImage;
    NSString *strLatitude;
    NSString *strLongitude;
    NSString *strZipcode;
    BOOL isProfilePhoto;
    int val;
    CAKeyframeAnimation * anim;
    
    KLCPopup *popupTermsAndConditions;
    MBProgressHUD *HUD;
    
    
    KLCPopup *popupExpertiseAt;
    NSMutableArray *arrSelected;
    NSMutableArray *arrJobDetails;
    BOOL isCalled;
    NSString *strTimeZone;
    NSString *strExpertiseAtId;
    UIImagePickerController *pick;
    NSArray *cityArr;
}
@end

@implementation OostaJobContractorRegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.imgBlur assignBlur];
    [self setupUI];
    [self AddingToolBarSSN];
    [self AddingToolBarPhone];
    [self AddingToolBarHourlyRate];
    [self registerForKeyboardNotifications];
    self.scrollview.scrollEnabled=YES;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.scrollview.contentSize=CGSizeMake(768, 1150);
    }
    else
    {
        self.scrollview.contentSize=CGSizeMake(320, 700);
    }
    
    arrSelected=[[NSMutableArray alloc]init];
    [self getCityDetails];
    [self removePlusIcon];
    [self removeallCaches];
    // Do any additional setup after loading the view from its nib.
}
-(void)removeallCaches
{
    // Delete any cached URLrequests!
    NSURLCache *sharedCache = [NSURLCache sharedURLCache];
    [sharedCache removeAllCachedResponses];
    
    // Also delete all stored cookies!
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookies = [cookieStorage cookies];
    id cookie;
    for (cookie in cookies) {
        [cookieStorage deleteCookie:cookie];
    }
}
-(void)removePlusIcon
{
    _imgPlusName.image=[UIImage imageNamed:@""];
    _imgPlusEmail.image=[UIImage imageNamed:@""];
    _imgPlusPassword.image=[UIImage imageNamed:@""];
    _imgPlusAddress.image=[UIImage imageNamed:@""];
    _imgPlusPhone.image=[UIImage imageNamed:@""];
    _imgPlusSSN.image=[UIImage imageNamed:@""];
    _imgPlusExpertise.image=[UIImage imageNamed:@""];
    _imgPlusHourlyRate.image=[UIImage imageNamed:@""];
    _imgPlusLicense.image=[UIImage imageNamed:@""];
    
}
-(void)AddingToolBarSSN
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneWithNumberPadSSN)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:FONT_BOLD size:18.0], NSFontAttributeName,
                                        [UIColor whiteColor], NSForegroundColorAttributeName,
                                        nil]
                              forState:UIControlStateNormal];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           doneButton,
                           nil];
    [numberToolbar sizeToFit];
    
    _txtSSNNumber.inputAccessoryView = numberToolbar;
    
}
-(void)AddingToolBarPhone
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneWithNumberPadPhone)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:FONT_BOLD size:18.0], NSFontAttributeName,
                                        [UIColor whiteColor], NSForegroundColorAttributeName,
                                        nil]
                              forState:UIControlStateNormal];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           doneButton,
                           nil];
    [numberToolbar sizeToFit];
    
    _txtPhone.inputAccessoryView = numberToolbar;
    
}
-(void)AddingToolBarHourlyRate
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneWithNumberPadHourlyRate)];
    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:FONT_BOLD size:18.0], NSFontAttributeName,
                                        [UIColor whiteColor], NSForegroundColorAttributeName,
                                        nil]
                              forState:UIControlStateNormal];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           doneButton,
                           nil];
    [numberToolbar sizeToFit];
    
    _txtHourlyRate.inputAccessoryView = numberToolbar;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
#pragma mark - Setup UI
-(void)setupUI
{
    self.viewName.layer.cornerRadius=5.0;
    self.viewEmail.layer.cornerRadius=5.0;
    self.viewPassword.layer.cornerRadius=5.0;
    self.viewAddress.layer.cornerRadius=5.0;
    self.viewPhone.layer.cornerRadius=5.0;
    self.viewSSN.layer.cornerRadius=5.0;
    self.viewExpertise.layer.cornerRadius=5.0;
    self.viewHourlyRate.layer.cornerRadius=5.0;
    self.viewLicense.layer.cornerRadius=5.0;
    
    self.viewName.layer.masksToBounds=YES;
    self.viewEmail.layer.masksToBounds=YES;
    self.viewPassword.layer.masksToBounds=YES;
    self.viewAddress.layer.masksToBounds=YES;
    self.viewPhone.layer.masksToBounds=YES;
    self.viewSSN.layer.masksToBounds=YES;
    self.viewExpertise.layer.masksToBounds=YES;
    self.viewHourlyRate.layer.masksToBounds=YES;
    self.viewLicense.layer.masksToBounds=YES;
    
    self.btnRegister.layer.cornerRadius=5.0;
    self.btnRegister.layer.masksToBounds=YES;
    if  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        [self.btnRegister.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:30.0f]];
        [self.btnTermsConditions.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:23.0f]];
        
        self.txtName.font=[UIFont fontWithName:FONT_THIN size:30.0f];
        self.txtEmail.font=[UIFont fontWithName:FONT_THIN size:30.0f];
        self.txtPassword.font=[UIFont fontWithName:FONT_THIN size:30.0f];
        self.txtAddress.font=[UIFont fontWithName:FONT_THIN size:30.0f];
        self.txtPhone.font=[UIFont fontWithName:FONT_THIN size:30.0f];
        self.txtExpertise.font=[UIFont fontWithName:FONT_THIN size:30.0f];
        self.txtSSNNumber.font=[UIFont fontWithName:FONT_THIN size:30.0f];
        self.txtHourlyRate.font=[UIFont fontWithName:FONT_THIN size:30.0f];
        self.txtLicense.font=[UIFont fontWithName:FONT_THIN size:30.0f];
        
        self.lblIAgree.font=[UIFont fontWithName:FONT_BOLD size:23.0f];
        
        self.lblTermsAndConditions.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        self.lblExpertiseAt.font=[UIFont fontWithName:FONT_BOLD size:23.0f];
        [_btnExpertiseAtDone.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:23.0f]];
        
        self.lblUploadYourPicture.font=[UIFont fontWithName:FONT_BOLD size:23.0f];
        
    }
    else
    {
        [self.btnRegister.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
        [self.btnTermsConditions.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
        
        self.txtName.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtEmail.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtPassword.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtAddress.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtPhone.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtExpertise.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtSSNNumber.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtHourlyRate.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        self.txtLicense.font=[UIFont fontWithName:FONT_THIN size:20.0f];
        
        self.lblIAgree.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
        self.lblTermsAndConditions.font=[UIFont fontWithName:FONT_BOLD size:20.0f];
        self.lblExpertiseAt.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
        [_btnExpertiseAtDone.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:16.0f]];
        
        self.lblUploadYourPicture.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
    }
}

#pragma mark - Textfield Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_txtName)
    {
        
        NSLog(@"Open Map");
        [self btnAddressTapped:self];
        [self.txtName resignFirstResponder];
        CGPoint point = CGPointMake(0, 0);
        [self.scrollview setContentOffset:point animated:YES];
    }
    else if (textField==_txtSSNNumber)
    {
        NSLog(@"Open Expertise");
        [self.txtSSNNumber resignFirstResponder];
        [self btnExpertise:self];
        
        CGPoint point = CGPointMake(0, 0);
        [self.scrollview setContentOffset:point animated:YES];
        
    }
    else if (textField==_txtEmail)
    {
        [_txtPassword becomeFirstResponder];
    }
    else if (textField==_txtPassword)
    {
        //[_txtHourlyRate becomeFirstResponder];
        [self btnLicensePressed:self];
    }
    //else if (textField==_txtHourlyRate)
    //{
    //    [_txtHourlyRate resignFirstResponder];
    //    [self btnLicensePressed:self];
    //}
    else
    {
        [self.view endEditing:YES];
        CGPoint point = CGPointMake(0, 0);
        [self.scrollview setContentOffset:point animated:YES];
    }
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField==_txtName)
    {
        if (text.length>0)
        {
            _imgPlusName.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            _imgPlusName.image=[UIImage imageNamed:@""];
        }
    }
    
    if (textField==_txtEmail)
    {
        if ([self IsValidEmail:text])
        {
            _imgPlusEmail.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            _imgPlusEmail.image=[UIImage imageNamed:@""];
        }
    }
    
    if (textField==_txtPassword)
    {
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (text.length>0&&(![string isEqualToString:@""]) && newLength >=10)
        {
            _imgPlusPassword.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            _imgPlusPassword.image=[UIImage imageNamed:@""];
        }
    }
    
    if (textField==_txtAddress)
    {
        if (text.length>0)
        {
            _imgPlusAddress.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            _imgPlusAddress.image=[UIImage imageNamed:@""];
        }
    }
    
    if (textField==_txtPhone)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength>=10)
        {
            _imgPlusPhone.image=[UIImage imageNamed:@"tick"];
        }
        else
        {
            _imgPlusPhone.image=[UIImage imageNamed:@""];
        }
        
        return newLength <= 10;
    }
    
    if (textField==_txtSSNNumber)
    {
        
        NSString *text = textField.text;
        text = [text stringByReplacingCharactersInRange:range withString:string];
        
        if ([text length] > 11 )
        {
            return NO;
        }
        
        text = [text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        
        // Do your length checking here
        
        NSMutableString *mutableText = [text mutableCopy];
        
        
        
        
        for (NSUInteger i = 0; i < mutableText.length; i++)
        {
            if (i==3)
            {
                [mutableText insertString:@"-" atIndex:i];
            }
            else if (i==6)
            {
                [mutableText insertString:@"-" atIndex:i];
            }
            
        }
        
        textField.text = mutableText;
        
        return NO;
        
        
        
        
    }
    
    /* if (textField==_txtHourlyRate)
     {
     if (text.length>0)
     {
     _imgPlusHourlyRate.image=[UIImage imageNamed:@"tick"];
     }
     else
     {
     _imgPlusHourlyRate.image=[UIImage imageNamed:@""];
     }
     }*/
    
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    if (textField == _txtSSNNumber)
    { // SSN is an outlet
        
        NSString *regEx = @"[0-9]{3}-[0-9]{2}-[0-9]{4}";
        
        NSRange r = [textField.text rangeOfString:regEx options:NSRegularExpressionSearch];
        
        if (r.location != NSNotFound)
        {
            _imgPlusSSN.image=[UIImage imageNamed:@"tick"];
            
        }
        else
        {
            _imgPlusSSN.image=[UIImage imageNamed:@""];
            
        }
        
    }
    
    return YES;
    
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+14, 0.0);
    self.scrollview.contentInset = contentInsets;
    self.scrollview.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, _txtExpertise.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, _txtExpertise.frame.origin.y-kbSize.height);
        [self.scrollview setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollview.contentInset = contentInsets;
    self.scrollview.scrollIndicatorInsets = contentInsets;
}
-(void)doneWithNumberPadSSN
{
    
    NSLog(@"Open Expertise");
    [self.txtSSNNumber resignFirstResponder];
    [self btnExpertise:self];
    
    CGPoint point = CGPointMake(0, 0);
    [self.scrollview setContentOffset:point animated:YES];
    
    
    
}
-(void)doneWithNumberPadPhone
{
    [_txtEmail becomeFirstResponder];
    
}
-(void)doneWithNumberPadHourlyRate
{
    NSLog(@"Open License");
    [self btnLicensePressed:self];
    [self.txtHourlyRate resignFirstResponder];
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
    CGPoint point = CGPointMake(0, 0);
    [self.scrollview setContentOffset:point animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions
- (IBAction)btnBackTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)IsValidSSN:(NSString *)checkString
{
    NSString *regEx = @"[0-9]{3}-[0-9]{2}-[0-9]{4}";
    
    NSRange r = [checkString rangeOfString:regEx options:NSRegularExpressionSearch];
    
    if (r.location != NSNotFound)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
-(BOOL)IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(void)scrolltoParticularTextField:(UITextField *)textField completion:(void (^)(BOOL success))completionBlock
{
    
    CGPoint point = CGPointMake(0, textField.superview.frame.origin.y);
    [self.scrollview setContentOffset:point animated:NO];
    if (completionBlock!=nil) {
        completionBlock(true);
    }
}

- (IBAction)btnRegisterTapped:(id)sender
{
    anim = [ CAKeyframeAnimation animationWithKeyPath:@"transform" ] ;
    anim.values = [ NSArray arrayWithObjects:
                   [ NSValue valueWithCATransform3D:CATransform3DMakeTranslation(-5.0f, 0.0f, 0.0f) ],
                   [ NSValue valueWithCATransform3D:CATransform3DMakeTranslation(5.0f, 0.0f, 0.0f) ],
                   nil ] ;
    anim.autoreverses = YES ;
    anim.repeatCount = 2.0f ;
    anim.duration = 0.07f ;
    
    if (_txtName.text.length==0&&_txtEmail.text.length==0&&_txtAddress.text.length==0&&_txtPassword.text.length==0&&_txtPhone.text.length==0&&_txtExpertise.text.length==0&&licenseImage==nil) //&&_txtHourlyRate.text.length==0
    {
        
        [self scrolltoParticularTextField:_txtName completion:^(BOOL success)
         {
             if (success)
             {
                 [ _viewName.layer addAnimation:anim forKey:nil ] ;
                 [ _viewAddress.layer addAnimation:anim forKey:nil ] ;
                 [ _viewPhone.layer addAnimation:anim forKey:nil ] ;
                 [ _viewEmail.layer addAnimation:anim forKey:nil ] ;
                 [ _viewPassword.layer addAnimation:anim forKey:nil ] ;
                 [ _viewSSN.layer addAnimation:anim forKey:nil ] ;
                 [ _viewExpertise.layer addAnimation:anim forKey:nil ] ;
                 //[ _viewHourlyRate.layer addAnimation:anim forKey:nil ] ;
                 [ _viewLicense.layer addAnimation:anim forKey:nil ] ;
             }
         }];
        
    }
    else if(_txtName.text.length==0)
    {
        [self scrolltoParticularTextField:_txtName completion:^(BOOL success)
         {
             if (success)
             {
                 [ _viewName.layer addAnimation:anim forKey:nil ] ;
             }
         }];
        
        
        
    }
    else if(_txtAddress.text.length==0)
    {
        [self scrolltoParticularTextField:_txtAddress completion:^(BOOL success)
         {
             if (success)
             {
                 [ _viewAddress.layer addAnimation:anim forKey:nil ] ;
             }
         }];
        
        
    }
    //    else if(![self IsValidSSN:_txtSSNNumber.text])
    //    {
    //        [self scrolltoParticularTextField:_txtSSNNumber completion:^(BOOL success)
    //         {
    //             if (success)
    //             {
    //                 [ _viewSSN.layer addAnimation:anim forKey:nil ] ;
    //             }
    //         }];
    //
    //
    //    }
    else if(_txtExpertise.text.length==0)
    {
        [self scrolltoParticularTextField:_txtExpertise completion:^(BOOL success)
         {
             if (success)
             {
                 [ _viewExpertise.layer addAnimation:anim forKey:nil ] ;
             }
         }];
        
        
    }
    else if(_txtPhone.text.length<10)
    {
        [self scrolltoParticularTextField:_txtPhone completion:^(BOOL success)
         {
             if (success)
             {
                 [ _viewPhone.layer addAnimation:anim forKey:nil ] ;
             }
         }];
        
    }
    else if(![self IsValidEmail:_txtEmail.text])
    {
        [self scrolltoParticularTextField:_txtEmail completion:^(BOOL success)
         {
             if (success)
             {
                 [ _viewEmail.layer addAnimation:anim forKey:nil ] ;
             }
         }];
        
        
    }
    else if(_txtPassword.text.length==0 || _txtPassword.text.length<10)
    {
        [self scrolltoParticularTextField:_txtPassword completion:^(BOOL success)
         {
             if (success)
             {
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 hud.labelFont = [UIFont boldSystemFontOfSize:12];
                 hud.labelText = @"Password must contain at least 10 characters";
                 hud.margin = 10.f;
                 hud.yOffset = 10.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 [ _viewPassword.layer addAnimation:anim forKey:nil ];
             }
         }];
        
    }
    /*else if(_txtHourlyRate.text.length==0)
     {
     [self scrolltoParticularTextField:_txtHourlyRate completion:^(BOOL success)
     {
     if (success)
     {
     [ _viewHourlyRate.layer addAnimation:anim forKey:nil ] ;
     }
     }];
     
     }*/
    else if(licenseImage==nil)
    {
        [self scrolltoParticularTextField:_txtLicense completion:^(BOOL success)
         {
             if (success)
             {
                 [ _viewLicense.layer addAnimation:anim forKey:nil ] ;
             }
         }];
        
    }
    else if (val !=1)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
        hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
        
        hud.labelText = @"Agree to Terms & Conditions ";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else if(choosenImage==nil)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
        hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
        
        hud.labelText = @"Upload Profile photo";
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    
    else
    {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        if (internetStatus == NotReachable)
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            
            hud.mode = MBProgressHUDModeText;
            hud.color=[UIColor whiteColor];
            hud.labelColor=kMBProgressHUDLabelColor;
            hud.backgroundColor=kMBProgressHUDBackgroundColor;
            
            hud.labelText = INTERNET_ERR;
            hud.margin = 10.f;
            hud.yOffset = 20.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
        else
        {
            NSLog(@"Register");
            CGFloat scaleSize = 0.2f;
            UIImage *smallImage = [UIImage imageWithCGImage:choosenImage.CGImage
                                                      scale:scaleSize
                                                orientation:choosenImage.imageOrientation];
            choosenImage=smallImage;
            smallImage = [UIImage imageWithCGImage:licenseImage.CGImage
                                             scale:scaleSize
                                       orientation:licenseImage.imageOrientation];
            licenseImage = smallImage;
            
            
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"dd-MM-YYYY"];
            int random = arc4random() % 9999999;
            NSString * randomName;
            randomName=[NSString stringWithFormat:@"%@-%d-Photo.png",[dateFormatter stringFromDate:[NSDate date]],random];
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"( \" )"];
            randomName = [[randomName componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
            NSString * randomLicense;
            randomLicense=[NSString stringWithFormat:@"%@-%d-License.png",[dateFormatter stringFromDate:[NSDate date]],random];
            randomLicense = [[randomLicense componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
            NSLog(@" Name %@",randomName);
            NSLog(@" License %@",randomLicense);
            NSString *URLString =[NSString stringWithFormat:@"%@license",BaseURL];
            //        NSDictionary *parameters = @{@"profile": randomName,@"license": randomLicense};
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
            [parameters setObject:randomName forKey:@"profile"];
            if (licenseImage!=nil)
            {
                [parameters setObject:randomLicense forKey:@"license"];
            }
            
            NSLog(@"parameters:%@",parameters);
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.color=[UIColor whiteColor];
            hud.labelText = @"Registering...";
            hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
            hud.activityIndicatorColor=[UIColor colorWithRed:0.971 green:0.000 blue:0.452 alpha:1.000];
            hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
            hud.margin = 010.f;
            hud.yOffset = 20.f;
            [hud show:YES];
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.securityPolicy.allowInvalidCertificates = YES;
            manager.requestSerializer = [AFJSONRequestSerializer serializer];    [manager POST:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
            {
                                                                                      
            for(int i=0;i<parameters.count;i++)
            {
            if (i==0)
            {
            [formData appendPartWithFileData:UIImagePNGRepresentation([UIImage compressImage:choosenImage compressRatio:0.8f]) name:@"profile" fileName:randomName mimeType:@"image/png"];                                }
                else
                {
             [formData appendPartWithFileData:UIImagePNGRepresentation([UIImage compressImage:licenseImage compressRatio:0.8f]) name:@"license" fileName:randomLicense mimeType:@"image/png"];                                }
                }
             NSLog(@"formData:%@",formData);
                }
             success:^(AFHTTPRequestOperation *operation, id responseObject)
                {
               NSLog(@"Success %@", responseObject);
              NSString * key1 =@"Name";
              NSString * obj1 =_txtName.text;
              NSString * key2 =@"Address";
              NSString * obj2 =_txtAddress.text;
              NSString * key3 =@"Phone";
              NSString * obj3 =_txtPhone.text;
              NSString * key4 =@"Zipcode";
              NSString * obj4 =strZipcode;
              NSString * key5 =@"Lat";
              NSString * obj5 =strLatitude;
              NSString * key6 =@"Lng";
              NSString * obj6 =strLongitude;
              NSString * key7 =@"profilePhoto";
              NSString * obj7 =randomName;
              NSUserDefaults *userValue=[NSUserDefaults standardUserDefaults];
              NSString *strToken=[userValue objectForKey:@"DEVICE_TOKEN"];
              if (strToken.length==0)
              {
               strToken=@"28d1e4a814db45b0cdabf929106d5f9dd48d966900f1c591dcff70ecdd6b5e6d";
              }
               NSString * key8 =@"notificationID";
               NSString * obj8 =strToken;
               NSString * key9 =@"deviceType";
               NSString * obj9 =@"2";
               NSString * key10 =@"Email";
               NSString * obj10 =_txtEmail.text;
               NSString * key11 =@"Password";
               NSString * obj11 =_txtPassword.text;
               NSString * key12 =@"SSN";
               NSString * obj12 =_txtSSNNumber.text;
               NSString * key13 =@"expertiseAT";
               NSString * obj13 =strExpertiseAtId;
               NSString * key14 =@"hourlyRate";
               NSString * obj14 =_txtHourlyRate.text;
               NSString * key15 =@"Licence";
               NSString * obj15 = licenseImage?randomLicense:@"";
               NSString * key16 =@"timezone";
               NSString * obj16 =strTimeZone;
               NSDictionary *jsonDictionary =[[NSDictionary alloc]initWithObjects:@[obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10,obj11,obj12,obj13,obj14,obj15,obj16]forKeys:@[key1,key2,key3,key4,key5,key6,key7,key8,key9,key10,key11,key12,key13,key14,key15,key16]];
               NSData * JsonData =[NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
               NSString * jsonString= [[NSString alloc] initWithData:JsonData encoding:NSUTF8StringEncoding];
               NSLog(@"DATA %@",jsonString);
               NSMutableData *body = [NSMutableData data];
               [body appendData:[[NSString stringWithFormat:@"%@",jsonString] dataUsingEncoding:NSUTF8StringEncoding]];
               NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@contractorregister",BaseURL]]];
               [request setHTTPBody:body];
               [request setHTTPMethod:@"POST"];
               [NSURLConnection sendAsynchronousRequest: request queue: [NSOperationQueue mainQueue] completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
                {
                if (error || !data)
                {
                 NSLog(@"Server Error : %@", error);
                 [hud hide:YES];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.color=[UIColor whiteColor];
                hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                hud.labelText = SERVER_ERR;
                hud.margin = 10.f;
                hud.yOffset = 20.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hide:YES afterDelay:2];
                }
                else
                {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                NSLog(@"Result %@",result);
                if ([[result valueForKeyPath:@"Result"]isEqualToString:@"Success"])
                {
                 NSString *contractorid = [result valueForKeyPath:@"ContractorDetails.contractorID"];
                    NSLog(@"contractorid%@:",contractorid);
                    ///****saving userid for bank details///***
                    NSUserDefaults *saveid = [NSUserDefaults standardUserDefaults];
                    [saveid setObject:contractorid forKey:@"Userid"];
                    [saveid synchronize];
                [hud hide:YES];
                MBProgressHUD*  hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                [self.navigationController.view addSubview:hud];
                hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"oostaright.png"]];
                hud.mode = MBProgressHUDModeCustomView;
                hud.color=[UIColor whiteColor];
                hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                hud.delegate = self;
                hud.labelText = @"Done";
                [hud show:YES];
                [hud hide:YES afterDelay:2];
                [[NSUserDefaults standardUserDefaults]setObject:[[result valueForKeyPath:@"ContractorDetails"] objectAtIndex:0] forKey:@"USERDETAILS"];
                 //   [self Gotobankdetails];
                [self gotoEmailVerification];
                }
                else if([[result valueForKeyPath:@"ContractorDetails"]isEqualToString:@"Email already exists"])
                {
                [hud hide:YES];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.color=[UIColor whiteColor];
                hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                hud.labelText = @"Email already exists";
                hud.margin = 10.f;
                hud.yOffset = 20.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hide:YES afterDelay:2];
                }
                else
                {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                 hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 }
                 }
                 }];
                 }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error)
                 {
                  NSLog(@"Failure %@, %@", error, operation.responseString);
                  [hud hide:YES];
                  MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                  hud.mode = MBProgressHUDModeText;
                  hud.color=[UIColor whiteColor];
                  hud.labelColor=[UIColor colorWithRed:0.075 green:0.433 blue:0.993 alpha:1.000];
                  hud.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
                  hud.labelText = SERVER_ERR;
                  hud.margin = 10.f;
                  hud.yOffset = 20.f;
                  hud.removeFromSuperViewOnHide = YES;
                  [hud hide:YES afterDelay:2];
                }];
        }
    }
}
-(void)Gotobankdetails{
    BankDetailcontroller *emailVerificationObj=[[BankDetailcontroller alloc]initWithNibName:@"BankDetailcontroller" bundle:nil];
    emailVerificationObj.passage = @"fromregister";
    [self.navigationController pushViewController:emailVerificationObj animated:YES];
}
-(void)gotoEmailVerification
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController IPad" bundle:nil];
        [self.navigationController pushViewController:emailVerificationObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController" bundle:nil];
        [self.navigationController pushViewController:emailVerificationObj animated:YES];
    }
    else
    {
        OostaJobEmailVerificationViewController *emailVerificationObj=[[OostaJobEmailVerificationViewController alloc]initWithNibName:@"OostaJobEmailVerificationViewController" bundle:nil];
        [self.navigationController pushViewController:emailVerificationObj animated:YES];
    }
    
    
}
#pragma mark - Camera Crop
- (IBAction)btnCameraTapped:(id)sender
{
    isProfilePhoto=YES;
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles: nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Take Photo", nil)];
    }
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Photo Library", nil)];
    if (choosenImage!=nil)
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Remove Photo", nil)];
    }
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    [actionSheet showFromToolbar:self.navigationController.toolbar];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Photo Library", nil)])
    {
        [self openPhotoAlbum];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Take Photo", nil)])
    {
        [self showCamera];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Remove Photo", nil)])
    {
        [self removePhoto];
    }
}
- (void)showCamera
{
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:controller animated:YES completion:NULL];
    }];
    
}

- (void)openPhotoAlbum
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:controller animated:YES completion:NULL];
    }];
    
    
}

-(void)removePhoto
{
    if (isProfilePhoto)
    {
        choosenImage=nil;
        [self.btnPhotoCapture setBackgroundImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
    }
    else
    {
        licenseImage=nil;
        _txtLicense.hidden=NO;
        _txtLicense.alpha = 0;
        _imgPlusLicense.image=[UIImage imageNamed:@""];
        [UIView animateWithDuration:0.3 animations:^{
            _imgLicense.alpha = 0;
        } completion: ^(BOOL finished) {
            _imgLicense.hidden = finished;
            _txtLicense.alpha = 1;
        }];
    }
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    pick = picker;
    if (isProfilePhoto)
    {
        choosenImage = info[UIImagePickerControllerOriginalImage];
    }
    else
    licenseImage = info[UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        if (isProfilePhoto)
        controller.image = choosenImage;
        else
        controller.image = licenseImage;
        
        UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:controller];
        [self presentViewController:navigationController animated:YES completion:NULL];
        self.view.clipsToBounds = YES;
        [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
    }];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    NSLog(@"cropped%@",croppedImage);
    if (isProfilePhoto)
    {
        choosenImage=croppedImage;
        [self.btnPhotoCapture setBackgroundImage:croppedImage forState:UIControlStateNormal];
        choosenImage=croppedImage;
    }
    else
    {
        licenseImage=croppedImage;
        _txtLicense.hidden=YES;
        _imgLicense.image=licenseImage;
        _imgLicense.alpha = 0;
        _imgLicense.hidden = NO;
        _imgPlusLicense.image=[UIImage imageNamed:@"tick"];
        
        [UIView animateWithDuration:0.5 animations:^{
            _imgLicense.alpha = 1;
        }];
    }
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:^{
        [self presentViewController:pick animated:YES completion:NULL];
    }];
    
}

#pragma mark - Terms and Conditions Checkbox
- (IBAction)btnCheckBoxTapped:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    val = 0;
    if ([UIImagePNGRepresentation(btn.currentBackgroundImage) isEqualToData:UIImagePNGRepresentation([UIImage imageNamed:@"check-40.png"])]){
        val = 0;
        [btn setBackgroundImage:[UIImage imageNamed:@"uncheck-40.png"] forState:UIControlStateNormal];
    }
    else
    {
        [btn setBackgroundImage:[UIImage imageNamed:@"check-40.png"] forState:UIControlStateNormal];
        val = 1;
    }
}

- (IBAction)btnTermsandConditionTapped:(id)sender
{
    [self.scrollview endEditing:YES];
    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutCenter);
    
    popupTermsAndConditions = [KLCPopup popupWithContentView:_viewTermsAndConditions
                                                    showType:KLCPopupShowTypeFadeIn
                                                 dismissType:KLCPopupDismissTypeFadeOut
                                                    maskType:KLCPopupMaskTypeDimmed
                                    dismissOnBackgroundTouch:NO
                                       dismissOnContentTouch:NO];
    [popupTermsAndConditions showWithLayout:layout];
    
    //HUD
    HUD = [[MBProgressHUD alloc] initWithView:self.viewTermsAndConditions];
    [self.viewTermsAndConditions addSubview:HUD];
    HUD.delegate = self;
    HUD.labelText = @"Loading...";
    
    //LOAD URL
    NSString* url = [[NSUserDefaults standardUserDefaults] objectForKey:@"ContractorTerms"];
    NSLog(@"url:%@",url);
    NSURL* nsUrl = [NSURL URLWithString:url];
    NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];

    [_webViewTermsAndConditions loadRequest:request];
}
- (IBAction)btnTermsAndConditionsClosePressed:(id)sender
{
    [popupTermsAndConditions dismissPresentingPopup];
}
#pragma mark - Webview Delegate
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    //SHOW HUD
    [HUD show:YES];
    
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //KILL HUD
    
    [HUD hide:YES];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    if(!webView.loading)
    {
        //KILL HUD
        [HUD hide:YES];
        
    }
}

#pragma mark - Address
- (IBAction)btnAddressTapped:(id)sender
{
    [self.scrollview endEditing:YES];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        LocationPickerViewController *locObj=[[LocationPickerViewController alloc]initWithNibName:@"LocationPickerViewController IPad" bundle:nil];
        locObj.delegate=self;
        locObj.strLat = strLatitude;
        locObj.strLong = strLongitude;
        [self.navigationController pushViewController:locObj animated:YES];
    }
    else
    {
        LocationPickerViewController *locObj=[[LocationPickerViewController alloc]initWithNibName:@"LocationPickerViewController" bundle:nil];
        locObj.delegate=self;
        locObj.strLat = strLatitude;
        locObj.strLong = strLongitude;
        [self.navigationController pushViewController:locObj animated:YES];
    }
}
- (void)getAddress:(NSString*)address andtheLat:(NSString *)latitude andtheLong:(NSString *)longitude andtheZipcode:(NSString *)zipcode
{
    NSLog(@"address:%@",address);
    strZipcode=zipcode;
    if ([[cityArr valueForKeyPath:@"zipcode"] containsObject:zipcode])
    {
        
        _imgPlusAddress.image=[UIImage imageNamed:@"tick"];
        _txtAddress.text=address;
        strLongitude=longitude;
        strLatitude=latitude;
        strZipcode=zipcode;
        [self getTheTimeZoneFromGivenLattitude:strLatitude andLongitude:strLongitude];
    }
    else
    {
        _imgPlusAddress.image=[UIImage imageNamed:@""];
        [self GotoAddressNotify];
    }
}

-(void)getTheTimeZoneFromGivenLattitude:(NSString *)Lattitude andLongitude:(NSString *)Longitude
{
    NSTimeInterval seconds = [[NSDate date] timeIntervalSince1970];
    NSString* urlStr1 =[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/timezone/json?location=%@,%@&timestamp=%f&language=es&key=%@",Lattitude,Longitude,seconds,kGoogleAPIKey];
    NSLog(@"GetTheTimezone Url %@",urlStr1);
    NSURLRequest *request1 = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr1]];
    [NSURLConnection sendAsynchronousRequest:request1
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSArray *statusArr =[result valueForKeyPath:@"status"];
             NSLog(@"Status %@",statusArr);
             NSString *resultStr =[NSString stringWithFormat:@"%@",statusArr];
             if ([resultStr isEqualToString:@"OK"])
             {
                 NSLog(@"result:%@",result);
                 strTimeZone=[result valueForKeyPath:@"timeZoneId"];
             }
             else
             {
                 NSLog(@"result:%@",result);
                 
             }
         }
         else
         {
             NSLog(@"error %@",connectionError);
         }
     }];
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrJobDetails.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
#define IMAGE_VIEW_TAG 99
    if (cell == nil)
    {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //add AsyncImageView to cell
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            AsyncImageView *imageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(10.0f, 10.0f, 42.0f, 42.0f)];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            imageView.tag = IMAGE_VIEW_TAG;
            [cell addSubview:imageView];
            
            //common settings
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.indentationWidth = 42.0f;
            cell.indentationLevel = 1;
        }
        else
        {
            AsyncImageView *imageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(2.0f, 2.0f, 42.0f, 42.0f)];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            imageView.tag = IMAGE_VIEW_TAG;
            [cell addSubview:imageView];
            
            //common settings
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.indentationWidth = 42.0f;
            cell.indentationLevel = 1;
        }
        
        
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[[arrJobDetails objectAtIndex:indexPath.row] valueForKeyPath:@"job_name"]];
    
    //get image view
    AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:IMAGE_VIEW_TAG];
    
    //cancel loading previous image for cell
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imageView];
    
    //load the image
    imageView.imageURL = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[arrJobDetails objectAtIndex:indexPath.row] valueForKeyPath:@"job_icon"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    
    if ([arrSelected containsObject:indexPath])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        cell.textLabel.font = [UIFont fontWithName:FONT_THIN size:23.0f];
    }
    else
    {
        cell.textLabel.font = [UIFont fontWithName:FONT_THIN size:16.0f];
    }
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([arrSelected containsObject:indexPath])
    {
        [arrSelected removeObject:indexPath];
    }
    else
    {
        [arrSelected addObject:indexPath];
    }
    [tableView reloadData];
}
#pragma mark - ExpertiseAt
- (IBAction)btnExpertise:(id)sender
{
    [self.scrollview endEditing:YES];
    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutCenter);
    
    popupExpertiseAt = [KLCPopup popupWithContentView:_viewExpertiseAt
                                             showType:KLCPopupShowTypeBounceInFromTop
                                          dismissType:KLCPopupDismissTypeBounceOutToBottom
                                             maskType:KLCPopupMaskTypeDimmed
                             dismissOnBackgroundTouch:NO
                                dismissOnContentTouch:NO];
    [popupExpertiseAt showWithLayout:layout];
    if (isCalled!=YES)
    {
        [self GetTheExpertiseList];
    }
}
-(void)GetTheExpertiseList
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        arrJobDetails=[[NSMutableArray alloc]init];
        [MBProgressHUD showHUDAddedTo:self.viewExpertiseAt animated:YES];
        
        NSString* urlStr1 =[NSString stringWithFormat:@"%@getjobtype",BaseURL];
        NSLog(@"GetTheJoblist Url %@",urlStr1);
        NSURLRequest *request1 = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr1]];
        [NSURLConnection sendAsynchronousRequest:request1
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response,
                                                   NSData *data, NSError *connectionError)
         {
             if (data.length > 0 && connectionError == nil)
             {
                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                        options:0
                                                                          error:NULL];
                 NSArray *statusArr =[result valueForKeyPath:@"Result"];
                 NSLog(@"Status %@",statusArr);
                 NSString *resultStr =[NSString stringWithFormat:@"%@",statusArr];
                 if ([resultStr isEqualToString:@"Success"])
                 {
                     NSLog(@"result:%@",result);
                     isCalled=YES;
                     arrJobDetails=[result valueForKeyPath:@"JobtypeDetails"];
                     NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"job_name" ascending:YES comparator:^NSComparisonResult(id obj1, id obj2) {
                         return [(NSString *)obj1 compare:(NSString *)obj2 options:NSNumericSearch];
                     }];
                     NSArray *sortedArray = [arrJobDetails sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
                     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (job_name CONTAINS 'Others')"];
                     arrJobDetails = [[NSMutableArray alloc]initWithArray:[sortedArray filteredArrayUsingPredicate:predicate]];
                     [self.tblExpertise reloadData];
                     [MBProgressHUD hideAllHUDsForView:self.viewExpertiseAt animated:YES];
                     
                 }
                 else
                 {
                     NSLog(@"result:%@",result);
                     [MBProgressHUD hideAllHUDsForView:self.viewExpertiseAt animated:YES];
                     
                 }
             }
             else
             {
                 NSLog(@"error %@",connectionError);
                 [MBProgressHUD hideAllHUDsForView:self.viewExpertiseAt animated:YES];
                 
             }
         }];
    }
}

- (IBAction)btnExpertiseAtDonePressed:(id)sender
{
    [popupExpertiseAt dismissPresentingPopup];
    if (arrSelected.count!=0)
    {
        [arrSelected sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSInteger r1 = [obj1 row];
            NSInteger r2 = [obj2 row];
            if (r1 > r2) {
                return (NSComparisonResult)NSOrderedDescending;
            }
            if (r1 < r2) {
                return (NSComparisonResult)NSOrderedAscending;
            }
            return (NSComparisonResult)NSOrderedSame;
        }];
        
        NSMutableArray *arrExpertise=[[NSMutableArray alloc]init];
        NSMutableArray *arrExpertiseJobId=[[NSMutableArray alloc]init];
        
        for (NSIndexPath *indexPath in arrSelected)
        {
            
            NSString *str=[[arrJobDetails  objectAtIndex:indexPath.row] valueForKeyPath:@"jobtype_id"];
            NSString *trimmedString = [str stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceCharacterSet]];
            NSString *strAt=[[arrJobDetails  objectAtIndex:indexPath.row] valueForKeyPath:@"job_name"];
            NSString *trimmedStringAt = [strAt stringByTrimmingCharactersInSet:
                                         [NSCharacterSet whitespaceCharacterSet]];
            
            [arrExpertiseJobId addObject:trimmedString];
            [arrExpertise addObject:trimmedStringAt];
        }
        NSLog(@"arrExpertise:%@",arrExpertise);
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"(\"\n)"];
        NSString *strExpertiseAt =[NSString stringWithFormat:@"%@",arrExpertise];
        strExpertiseAt = [[strExpertiseAt componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""];
        NSString *trimmedStringAt = [strExpertiseAt stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceCharacterSet]];
        trimmedStringAt = [trimmedStringAt stringByReplacingOccurrencesOfString:@",    " withString:@","];
        NSString *strExpertiseid =[NSString stringWithFormat:@"%@",arrExpertiseJobId];
        
        strExpertiseAtId=[[[[strExpertiseid componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@""]stringByTrimmingCharactersInSet:
                           [NSCharacterSet whitespaceCharacterSet]] stringByReplacingOccurrencesOfString:@",    " withString:@","];
        NSLog(@"strExpertiseAtId:%@",strExpertiseAtId);
        _txtExpertise.text=trimmedStringAt;
        
        _imgPlusExpertise.image=[UIImage imageNamed:@"tick"];
        
        
        
    }
    else
    {
        _txtExpertise.text=nil;
        _imgPlusExpertise.image=[UIImage imageNamed:@""];
    }
}

- (IBAction)btnLicensePressed:(id)sender
{
    isProfilePhoto=NO;
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles: nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Take Photo", nil)];
    }
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Photo Library", nil)];
    if (licenseImage!=nil)
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Remove Photo", nil)];
    }
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    [actionSheet showFromToolbar:self.navigationController.toolbar];
    
}
-(void)getCityDetails
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@city",BaseURL]]];
    NSLog(@"city:%@",request.URL);
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 cityArr=[[NSArray alloc]init];
                 cityArr=[result valueForKeyPath:@"CityDetails"];
                 
             }
             else
             {
                 
             }
         }
     }];
    
}
-(void)GotoAddressNotify
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        OostaJobZipCodeViewController *ZipCodeObj=[[OostaJobZipCodeViewController alloc]initWithNibName:@"OostaJobZipCodeViewController IPad" bundle:nil];
        ZipCodeObj.strZipcode = strZipcode;
        [self.navigationController pushViewController:ZipCodeObj animated:YES];
    }
    else if (IS_IPHONE5)
    {
        OostaJobZipCodeViewController *ZipCodeObj=[[OostaJobZipCodeViewController alloc]initWithNibName:@"OostaJobZipCodeViewController" bundle:nil];
        ZipCodeObj.strZipcode = strZipcode;
        [self.navigationController pushViewController:ZipCodeObj animated:YES];
    }
    else
    {
        OostaJobZipCodeViewController *ZipCodeObj=[[OostaJobZipCodeViewController alloc]initWithNibName:@"OostaJobZipCodeViewController Small" bundle:nil];
        ZipCodeObj.strZipcode = strZipcode;
        [self.navigationController pushViewController:ZipCodeObj animated:YES];
    }
    
}


@end
