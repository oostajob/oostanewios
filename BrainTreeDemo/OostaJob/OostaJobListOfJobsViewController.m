//
//  OostaJobListOfJobsViewController.m
//  OostaJob
//
//  Created by Apple on 06/11/15.
//  Copyright (c) 2015 Armor. All rights reserved.
//

#import "OostaJobListOfJobsViewController.h"
#import "OostaJobListOfJobsTableViewCell.h"
#import "SWRevealViewController.h"
#import "ImageCache.h"
#import <QuartzCore/QuartzCore.h>
#import "OostaJobBuildJobViewController.h"
#import <iAd/iAd.h>

@interface OostaJobListOfJobsViewController ()<UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate,ADBannerViewDelegate>
{
    NSString *strOthers;
}
@property (retain, nonatomic) ADBannerView *adBanner;
@end

@implementation OostaJobListOfJobsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    strOthers = @"";
    [self setupUI];
    [self CallingGetJobType];
    self.navigationController.navigationBarHidden=YES;
    SWRevealViewController *revealController = [self revealViewController];
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    _tblSelectservice.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self settingAdBanner];
    [self getTheServiceURL];
    
}
-(void)CallingGetJobType
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        hud.mode = MBProgressHUDModeText;
        hud.color=[UIColor whiteColor];
        hud.labelColor=kMBProgressHUDLabelColor;
        hud.backgroundColor=kMBProgressHUDBackgroundColor;
        
        hud.labelText = INTERNET_ERR;
        hud.margin = 10.f;
        hud.yOffset = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    else
    {
        [self getJobType];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self setKeyBoardNotificationCenters];
}
-(void)setupUI
{
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        self.lblSelectservice.font=[UIFont fontWithName:FONT_BOLD size:30.0f];
        
    }
    else if (IS_IPHONE5)
    {
        self.lblSelectservice.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
    }
    else
    {
        self.lblSelectservice.font=[UIFont fontWithName:FONT_BOLD size:16.0f];
    }
    
    
    
}

#pragma mark -Table View Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (JobTypeArr.count==0)
    {
        _tblSelectservice.backgroundColor = [UIColor clearColor];
        return 1;
        
    }
    else
    {
        _tblSelectservice.backgroundColor = [UIColor whiteColor];
        return JobTypeArr.count;
        
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (JobTypeArr.count==0)
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        
        cell.textLabel.text = @"No Services Found";
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont fontWithName:FONT_BOLD size:IS_IPAD?22:18];
        return cell;
    }
    else
    {
        static NSString *identifier =@"OostaJobListOfJobsTableViewCell";
        
        OostaJobListOfJobsTableViewCell *cell=(OostaJobListOfJobsTableViewCell *)[self.tblSelectservice dequeueReusableCellWithIdentifier:identifier];
        
        if (cell == nil)
        {
            NSArray *nib;
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                nib=[[NSBundle mainBundle]loadNibNamed:@"OostaJobListOfJobsTableViewCell iPad" owner:self options:nil];
                cell=[nib objectAtIndex:0];
                cell.lblList.font=[UIFont fontWithName:FONT_BOLD size:35.0f];
                cell.lblList.textAlignment=NSTextAlignmentCenter;
                
            }
            else if (IS_IPHONE5)
            {
                nib=[[NSBundle mainBundle]loadNibNamed:@"OostaJobListOfJobsTableViewCell" owner:self options:nil];
                cell=[nib objectAtIndex:0];
                cell.lblList.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
                cell.lblList.textAlignment=NSTextAlignmentCenter;
                
            }
            else
            {
                nib=[[NSBundle mainBundle]loadNibNamed:@"OostaJobListOfJobsTableViewCell" owner:self options:nil];
                cell=[nib objectAtIndex:0];
                cell.lblList.font=[UIFont fontWithName:FONT_BOLD size:25.0f];
                cell.lblList.textAlignment=NSTextAlignmentCenter;
                
            }
            cell.txtOthers.tag=100;
            
        }
        cell.lblList.text=[[JobTypeArr objectAtIndex:indexPath.row] valueForKey:@"job_name"];
        if ([cell.lblList.text isEqualToString:@"Others"])
        {
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                cell.lblList.frame=CGRectMake(0, 0, 768, 240);
                cell.txtOthers.hidden=NO;
                cell.btnNext.hidden=NO;
                cell.viewLine.hidden=NO;
                cell.txtOthers.delegate=self;
                cell.txtOthers.font=[UIFont fontWithName:FONT_THIN size:26.0f];
                cell.btnNext.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:26.0f];
                
                
            }
            else
            {
                cell.lblList.frame=CGRectMake(0, 0, 320, 210);
                cell.txtOthers.hidden=NO;
                cell.btnNext.hidden=NO;
                cell.viewLine.hidden=NO;
                cell.txtOthers.delegate=self;
                cell.txtOthers.font=[UIFont fontWithName:FONT_THIN size:18.0f];
                cell.btnNext.titleLabel.font=[UIFont fontWithName:FONT_BOLD size:18.0f];
                
            }
            
        }
        else
        {
            cell.selectionStyle=UITableViewCellSelectionStyleDefault;
            if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
            {
                cell.lblList.frame=CGRectMake(0, 0, 768, 320);
                cell.txtOthers.hidden=YES;
                cell.btnNext.hidden=YES;
                cell.viewLine.hidden=YES;
            }
            else
            {
                cell.lblList.frame=CGRectMake(0, 0, 320, 240);
                cell.txtOthers.hidden=YES;
                cell.btnNext.hidden=YES;
                cell.viewLine.hidden=YES;
            }
            
        }
        
        NSString *imageCacheKey = [[[JobTypeArr objectAtIndex:indexPath.row] valueForKeyPath:@"job_image"] stringByDeletingPathExtension];
        //    [cell.imgServices setContentMode:UIViewContentModeScaleAspectFill];
        [cell.imgServices setClipsToBounds:YES];
        if ([[ImageCache sharedImageCache]hasImageWithKey:imageCacheKey])
        {
            cell.imgServices.image=[[ImageCache sharedImageCache]imageForKey:imageCacheKey];
        }
        else
        {
            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imgServices];
            cell.imgServices.imageURL=[NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",ImageBaseURL,[[JobTypeArr objectAtIndex:indexPath.row] valueForKeyPath:@"job_image"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            if (cell.imgServices.image)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[ImageCache sharedImageCache] storeImage:cell.imgServices.image withKey:imageCacheKey];
                    
                });
            }
            
        }
        if (IS_IPAD)
        {
            cell.imgServices.image = [self CreateAResizeImage:cell.imgServices.image ThumbSize:cell.imgServices.frame.size];
        }
        cell.btnNext.tag=indexPath.row;
        [cell.btnNext addTarget:self action:@selector(MoveToNext:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;
        
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (JobTypeArr.count==0)
    {
        return IS_IPAD?60:40;
    }
    else
    {
        return IS_IPAD?400:240;
    }
}

-(void)MoveToNext:(UIButton *)sender
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:sender.tag inSection:0];
    UITableViewCell *cell = [_tblSelectservice cellForRowAtIndexPath:indexPath];
    UITextField *txtField = (UITextField*)[cell viewWithTag:100];
    strOthers=txtField.text;
    
    [self GoToBuildYourJob:[JobTypeArr objectAtIndex:sender.tag]];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (JobTypeArr.count!=0)
    {
        if ([[[JobTypeArr objectAtIndex:indexPath.row] valueForKey:@"job_name"] isEqualToString:@"Others"])
        {
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            UITextField *txtField = (UITextField*)[cell viewWithTag:100];
            [txtField becomeFirstResponder];
        }
        else
        {
            [self GoToBuildYourJob:[JobTypeArr objectAtIndex:indexPath.row]];
        }
    }
    
}
- (IBAction)btnMenu:(id)sender
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController revealToggle:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)getJobType
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.color=[UIColor whiteColor];
    hud.labelText = @"Loading Services...";
    hud.labelColor=kMBProgressHUDLabelColor;
    hud.activityIndicatorColor=kMBProgressHUDActivityIndicatorColor;
    hud.backgroundColor=kMBProgressHUDBackgroundColor;
    hud.margin = 010.f;
    hud.yOffset = 20.f;
    [hud show:YES];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@getjobtype",BaseURL]]];
    [request setHTTPMethod:@"GET"];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSLog(@"getjobtype URl:%@",request.URL);
    
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error || !data)
         {
             NSLog(@"Server Error : %@", error);
             
             [hud hide:YES];
             
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             
             hud.mode = MBProgressHUDModeText;
             hud.color=[UIColor whiteColor];
             hud.labelColor=kMBProgressHUDLabelColor;
             hud.backgroundColor=kMBProgressHUDBackgroundColor;
             
             hud.labelText = SERVER_ERR;
             hud.margin = 10.f;
             hud.yOffset = 20.f;
             hud.removeFromSuperViewOnHide = YES;
             [hud hide:YES afterDelay:2];
             [self.tblSelectservice reloadData];
         }
         else
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSLog(@"Result %@",result);
             if ([[result valueForKeyPath:@"Result"] isEqualToString:@"Success"])
             {
                 [hud hide:YES];
                 JobTypeArr=[[NSMutableArray alloc]init];
                 JobTypeArr=[result valueForKeyPath:@"JobtypeDetails"];
                 NSLog(@"Job types%@",JobTypeArr);
                 [self.tblSelectservice reloadData];
                 
             }
             else
             {
                 [hud hide:YES];
                 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                 
                 hud.mode = MBProgressHUDModeText;
                 hud.color=[UIColor whiteColor];
                 hud.labelColor=kMBProgressHUDLabelColor;
                 hud.backgroundColor=kMBProgressHUDBackgroundColor;
                 
                 hud.labelText = SERVER_ERR;
                 hud.margin = 10.f;
                 hud.yOffset = 20.f;
                 hud.removeFromSuperViewOnHide = YES;
                 [hud hide:YES afterDelay:2];
                 [self.tblSelectservice reloadData];
             }
         }
     }];
    
}


#pragma mark KeyBoard adjust Tableview

-(void) setKeyBoardNotificationCenters
{
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillShow:)
                                                 name: UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardDidShow:)
                                                 name: UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillDisappear:)
                                                 name: UIKeyboardWillHideNotification object:nil];
}
- (void) keyboardWillShow: (NSNotification*) aNotification
{
    [UIView animateWithDuration: [self keyboardAnimationDurationForNotification: aNotification] animations:^{
        
    } completion:^(BOOL finished) {
    }];
}

- (void) keyboardDidShow: (NSNotification*) aNotification
{
    [UIView animateWithDuration: [self keyboardAnimationDurationForNotification: aNotification] animations:^{
        CGSize kbSize = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        //change the frame of your talbleiview via kbsize.height
        
        CGRect frame=_tblSelectservice.frame;
        frame.size.height=frame.size.height-kbSize.height;
        _tblSelectservice.frame=frame;
        
        
        
        
        
    } completion:^(BOOL finished) {
        CGSize r = self.tblSelectservice.contentSize;
        [self.tblSelectservice scrollRectToVisible:CGRectMake(0, r.height-10, r.width, 10) animated:YES];
    }];
}

- (void) keyboardWillDisappear: (NSNotification*) aNotification
{
    [UIView animateWithDuration: [self keyboardAnimationDurationForNotification: aNotification] animations:^{
        //restore your tableview
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            _tblSelectservice.frame=CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height-100);
        }
        else
        {
            _tblSelectservice.frame=CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64);
        }
        
    } completion:^(BOOL finished) {
    }];
}

- (NSTimeInterval) keyboardAnimationDurationForNotification:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    NSValue* value = [info objectForKey: UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval duration = 0;
    [value getValue: &duration];
    
    return duration;
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)GoToBuildYourJob:(NSMutableArray *)arr
{
    OostaJobBuildJobViewController *BuildObj;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        BuildObj = [[OostaJobBuildJobViewController alloc]initWithNibName:@"OostaJobBuildJobViewController IPad" bundle:nil];
    }
    else if (IS_IPHONE5)
    {
        BuildObj = [[OostaJobBuildJobViewController alloc]initWithNibName:@"OostaJobBuildJobViewController" bundle:nil];
    }
    else
    {
        BuildObj = [[OostaJobBuildJobViewController alloc]initWithNibName:@"OostaJobBuildJobViewController" bundle:nil];
    }
    
    BuildObj.arrSelectedJob = arr;
    BuildObj.strOther = [NSString stringWithFormat:@"%@",strOthers];
    [self.navigationController pushViewController:BuildObj animated:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

-(UIImage *)CreateAResizeImage:(UIImage *)Img ThumbSize:(CGSize)ThumbSize
{
    float actualHeight = Img.size.height;
    float actualWidth = Img.size.width;
    
    if(actualWidth==actualHeight)
    {
        actualWidth = ThumbSize.width;
        actualHeight = ThumbSize.height;
    }
    
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = ThumbSize.width/ThumbSize.height; //320.0/480.0;
    
    
    if(imgRatio!=maxRatio)
    {
        if(imgRatio < maxRatio)
        {
            imgRatio = ThumbSize.height / actualHeight; //480.0 / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = ThumbSize.height; //480.0;
        }
        else
        {
            imgRatio = ThumbSize.width / actualWidth; //320.0 / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = ThumbSize.width; //320.0;
        }
    }
    else
    {
        actualWidth = ThumbSize.width;
        actualHeight = ThumbSize.height;
    }
    
    
    CGRect rect = CGRectMake(0, 0, (int)actualWidth, (int)actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [Img drawInRect:rect];
    UIImage *NewImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return NewImg;
}

#pragma mark Adding Ad Banner

-(void)settingAdBanner
{
    self.adBanner = [[ADBannerView alloc]initWithFrame:CGRectMake(0,IS_IPAD?100:64, [UIScreen mainScreen].bounds.size.width, IS_IPAD?66:50)];
    self.adBanner.delegate=self;
    [self.adBanner setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.adBanner];
}

-(void)bannerViewWillLoadAd:(ADBannerView *)banner {
    NSLog(@"bannerViewWillLoadAd");
}

-(void)bannerViewDidLoadAd:(ADBannerView *)banner {
    // Show the ad banner.
    NSLog(@"bannerViewDidLoadAd");
    
    [UIView animateWithDuration:0.5 animations:^{
        self.adBanner.alpha = 1.0;
        self.tblSelectservice.frame = CGRectMake(0, banner.frame.origin.y+banner.frame.size.height, self.tblSelectservice.frame.size.width, self.view.frame.size.height-(banner.frame.origin.y+banner.frame.size.height));
    }];
}

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    NSLog(@"didFailToReceiveAdWithError");
    
    // Hide the ad banner.
    [UIView animateWithDuration:0.5 animations:^{
        self.adBanner.alpha = 0.0;
        self.tblSelectservice.frame = CGRectMake(0, IS_IPAD?100:64, self.tblSelectservice.frame.size.width, self.view.frame.size.height+(banner.frame.origin.y+banner.frame.size.height));
    }];
}

-(void)bannerViewActionDidFinish:(ADBannerView *)banner {
    NSLog(@"Ad did finish");
}


-(void)getTheServiceURL
{
    NSString* urlStr1 =[NSString stringWithFormat:@"%@urllinks",BaseURL];
    NSLog(@"GetTheServiceURL Url %@",urlStr1);
    NSURLRequest *request1 = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr1]];
    [NSURLConnection sendAsynchronousRequest:request1
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             NSArray *statusArr =[result valueForKeyPath:@"Result"];
             NSLog(@"Status %@",statusArr);
             NSString *resultStr =[NSString stringWithFormat:@"%@",statusArr];
             if ([resultStr isEqualToString:@"Success"])
             {
                 NSLog(@"result:%@",result);
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"Aboutus"] forKey:@"Aboutus"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"FAQ"] forKey:@"FAQ"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"ContractorTerms"] forKey:@"ContractorTerms"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"CustomerTerms"] forKey:@"CustomerTerms"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"CustomerStories"] forKey:@"CustomerStories"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"Contractorpayments"] forKey:@"Contractorpayments"];
                 [[NSUserDefaults standardUserDefaults]setObject:[result valueForKeyPath:@"Customerpaymentsterms"] forKey:@"Customerpaymentsterms"];
             }
             else
             {
                 NSLog(@"result:%@",result);
             }
         }
         else
         {
             NSLog(@"error %@",connectionError);
         }
     }];
}
@end
